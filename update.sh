#!/bin/bash

(

	flock -x -w 10 200 || exit 1

	source "${BASH_SOURCE%/*}/common.sh"

	R="rsync://10.8.13.150/share"

	if [[ "$VERSION_ID" == "14.04" && "$(uname -m)" == "i686" ]]; then
		dpkg --log=/dev/null --force-confold --configure -a

		if rsync --checksum --partial -hhhiiv --stats --recursive --compress "$R/root/*" "/"; then
			dpkg --log=/dev/null -E -G -R --force-confold -i packages
		fi

		# if debsums librtpkcs11ecp; then
		# 	rsync --checksum -q --perms "/opt/aktivco/rutokenecp/i386/librtpkcs11ecp.so" "/opt/utm/lib/librtpkcs11ecp.so"
		# 	rsync --checksum -q --perms "/opt/aktivco/rutokenecp/i386/librtpkcs11ecp.so" "/opt/utm/lib/librtpkcs11ecp-replica.so"
		# fi
	elif [[ "$VERSION_ID" == "18.04" && "$(uname -m)" == "x86_64" ]]; then
		rsync --checksum --partial -hhhiiv --stats --recursive --compress "$R/root/root/"{"*.sh","ttnload"} "/root/"
	fi

	#rm $(file -F' ' -N /var/lib/apt/lists/* | grep -ahF 'HTML document' | cut -d' ' -f1)

) 200>/var/lock/.update.exclusivelock
