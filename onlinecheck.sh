#!/bin/bash

source "${BASH_SOURCE%/*}/common_input_funcs.sh"

function display_help_message() {
    echo -ne '\e[36m'
    echo "Использование: ./onlinecheck.sh [опции] <идентификатор_чека>"
    echo "Опции:"
    echo "  --mail <email>         Электронная почта клиента"
    echo "  --close-check          Закрыть чек без ввода электронной почты"
    echo "  --help                 Показать это сообщение"
    echo -e '\e[0m'
}

if ! TEMP=$(getopt --name 'onlinecheck.sh' --longoptions 'mail:,close-check,help' --options 'h' -- "$@"); then
    display_help_message
    exit 1
fi
eval set -- "$TEMP"
unset TEMP

while true; do
    case "$1" in
    '--mail')
        customer_email="$2"
        shift 2
        ;;
    '--close-check')
        close_check=1
        shift
        ;;
    '-h' | '--help')
        display_help_message
        exit 0
        ;;
    '--')
        shift
        break
        ;;
    *)
        red_print "Неизвестная опция '$1' проигнорирована"
        shift
        ;;
    esac
done

for arg; do
    if [[ "$arg" && "$arg" != "--" ]]; then
        check_id="$arg"
        break
    fi
done

if [[ -z "$check_id" ]]; then
    red_print "Ошибка: Идентификатор чека не указан"
    display_help_message
    exit 1
fi

echo "Начало обработки онлайн-чека: $check_id"

looping_timeout=30

for ((input_attempt_counter = 0; ; input_attempt_counter++)); do
    if ((input_attempt_counter == 5)); then
        red_print "Превышено максимальное количество попыток ввода онлайн-чека"
        exit 1
    fi
    escape
    sleep "$default_delay"
    echo -n "$check_id" | xclip -selection "clipboard"
    xdotool key --delay 1 ctrl+v KP_Enter
    update_log_snapshot
    wait_until_pattern_found 100 "Введен идентификатор онлайн-чека" "echo"
    loop_beginning_mark_seconds="$(date "+%s")"
    while true; do
        if pattern_exists_in_log_snapshot 3 "Невозможно восстановить онлайн-чек" "red_print"; then
            red_print "Через 5 секунд будет повторная попытка ввода онлайн-чека"
            sleep 5s
            continue 2
        elif pattern_exists_in_log_snapshot 100 "Получение чека с сервера по id" "echo"; then
            break
        fi
        if (("$(date "+%s")" - loop_beginning_mark_seconds > 30)); then
            red_print "Превышено максимальное время работы цикла"
            exit 1
        fi
        update_log_snapshot
    done
    wait_until_pattern_found 100 "Запрос выполнен успешно" "green_print"
    loop_beginning_mark_seconds="$(date "+%s")"
    while true; do
        if pattern_exists_in_log_snapshot 3 "Невозможно восстановить онлайн-чек" "red_print"; then
            red_print "Через 5 секунд будет повторная попытка ввода онлайн-чека"
            sleep 5s
            continue 2
        elif pattern_exists_in_log_snapshot 100 "Создание документа" "green_print"; then
            break 2
        fi
        if (("$(date "+%s")" - loop_beginning_mark_seconds > 60)); then
            red_print "Превышено максимальное время работы цикла"
            exit 1
        fi
        update_log_snapshot
    done
done
loop_beginning_mark_seconds="$(date "+%s")"
while true; do
    if handle_common_input_errors "red_print" "red_print"; then
        exit 1
    elif pattern_exists_in_log_snapshot 8 "Переход в подытог" "echo"; then
        break 2
    fi
    if (("$(date "+%s")" - loop_beginning_mark_seconds > 30)); then
        red_print "Превышено максимальное время работы цикла"
        exit 1
    fi
    update_log_snapshot
done
wait_until_pattern_found 5 "Разбор штрих-кода завершен" "green_print"

if ((close_check)) || [[ "$customer_email" ]]; then
    for ((input_attempt_counter = 0; ; input_attempt_counter++)); do
        if ((input_attempt_counter == 10)); then
            red_print "Превышено максимальное количество попыток закрытия чека"
            exit 1
        fi
        sleep "$default_delay"
        xdotool key --delay 1 F12
        loop_beginning_mark_seconds="$(date "+%s")"
        while true; do
            update_log_snapshot
            if pattern_exists_in_log_snapshot 5 "Диалог ввода адреса покупателя" "echo"; then
                wait_until_pattern_found 3 "Активация контекста диалога"
                if [[ "$customer_email" ]]; then
                    echo -n "$customer_email" | xclip -selection "clipboard"
                    sleep "$default_delay"
                    xdotool key --delay 1 ctrl+v KP_Enter
                    update_log_snapshot
                    wait_until_pattern_found 5 "Печатать чек?"
                    wait_until_pattern_found 3 "Активация контекста диалога"
                fi
                sleep "$default_delay"
                xdotool key --delay 1 Escape
                break 2
            elif pattern_exists_in_log_snapshot 3 "Активация контекста сдачи"; then
                continue 2
            elif pattern_exists_in_log_snapshot 20 "Закрытие документа" "green_print"; then
                break 2
            elif handle_common_input_errors "red_print" "red_print"; then
                exit 1
            fi
            if (("$(date "+%s")" - loop_beginning_mark_seconds > 30)); then
                continue 2
            fi
        done
    done
    wait_until_pattern_found 8 "Чек закрыт успешно" "green_print"
    wait_until_pattern_found 5 "Активация контекста закрытого документа" "green_print"
fi

green_print "Обработка онлайн-чека завершена"

exit 0
