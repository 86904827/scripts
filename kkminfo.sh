source "${BASH_SOURCE%/*}/common.sh"

data="$(mktemp)"
kkm_data="$(mktemp)"

/linuxcash/cash/bin/InfoClient >"$data"

function print_kkm_info() {
    local kkm_number="$1"
    grep -ahF "kkm=$kkm_number" "$data" | tr '&' '\n' >"$kkm_data"
    if [[ -s "$kkm_data" ]]; then
        echo -ne '\e[94m'
        echo -n "ККМ$kkm_number "
        if grep -ahiF 'DUMMY' "$kkm_data" &>/dev/null; then
            echo "Заглушка"
        else
            producer="$(grep -ahoP '^producer=\K\d+' "$kkm_data")"
            model="$(grep -ahoP '^model=\K\d+' "$kkm_data")"
            number="$(grep -ahoP '^number=\K\d+' "$kkm_data")"
            fn_number="$(grep -ahoP '^fn_number=\K\d+' "$kkm_data")"
            fn_time_end="$(grep -ahoP '^fn_time_end=\K[\d\.]+' "$kkm_data")"
            firmware="$(grep -ahoP '^firmware=\K.+' "$kkm_data")"
            ffd_version="$(grep -ahoP '^ffd_version=\K[\d\.]+' "$kkm_data")"
            fn_not_send_doc_count="$(grep -ahoP '^fn_not_send_doc_count=\K\d+' "$kkm_data")"
            fn_earliest_not_send_doc_date="$(grep -ahoP '^fn_earliest_not_send_doc_date=\K.+' "$kkm_data")"
            if [[ "$producer" == "1" ]]; then
                if [[ "$model" == "25" ]]; then
                    echo -n "ШТРИХ-ON-LINE"
                elif [[ "$model" == "252" ]]; then
                    echo -n "ШТРИХ-ЛАЙТ-02Ф"
                else
                    echo -n "Штрих-М"
                fi
            elif [[ "$producer" == "4" ]]; then
                echo -n "Атол"
                if [[ "$model" == "61" ]]; then
                    echo -n " 30 Ф"
                elif [[ "$model" == "62" ]]; then
                    echo -n " 55Ф"
                elif [[ "$model" == "67" ]]; then
                    echo -n " 11Ф"
                fi
            elif [[ "$producer" == "5" ]]; then
                echo -n "Вики Принт"
                if [[ "$model" == "4" ]]; then
                    echo -n " 57 Ф"
                fi
            fi

            echo ":"
            echo -ne '\e[36m'
            echo "  Прошивка $firmware ФФД $ffd_version"
            echo "  ЗН $number"
            echo -n "  ФН $fn_number до "
            if ((kkm_number == 1 && (INN_NCASH_INI == 1644096180 || \
                INN_NCASH_INI == 1657253779 || \
                INN_NCASH_INI == 1660340005 || \
                INN_NCASH_INI == 1660343863 || \
                INN_NCASH_INI == 1660344472 || \
                INN_NCASH_INI == 1660346991 || \
                INN_NCASH_INI == 1660347201 || \
                INN_NCASH_INI == 1660349488 || \
                INN_NCASH_INI == 1660349657))); then
                echo -ne '\e[96m'
                echo "->Эридан<-"
            else
                echo "$fn_time_end"
            fi
            if [[ "$fn_not_send_doc_count" == "0" ]]; then
                echo -ne '\e[32m'
            else
                echo -ne '\e[91m'
            fi
            echo -n "  ОФД : Непередано $fn_not_send_doc_count "
            if [[ "$fn_earliest_not_send_doc_date" ]]; then
                echo -n "с $fn_earliest_not_send_doc_date"
            fi
            echo
        fi
        echo -ne '\e[0m'
    fi
}

print_kkm_info 1
print_kkm_info 2
echo
echo -ne '\e[94m'
dpkg-query -W libfptr10 artix-comproxy
echo -ne '\e[0m'
