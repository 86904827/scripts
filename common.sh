#!/bin/bash

renice -n -20 $$ &>/dev/null

cd /root || exit 1

set -o pipefail
shopt -s nocasematch

source /etc/os-release

export LANG="ru_RU.UTF-8"
export LC_COLLATE="C"
export LC_NUMERIC="C"

export DISPLAY=":255"
export XAUTHORITY="/home/autologon/.Xauthority"

export PUPPET_CONF="/etc/puppet/puppet.conf"

export CHECK_IMG="/linuxcash/cash/data/tmp/check.img"

export NCASH_INI="/linuxcash/cash/conf/ncash.ini"
export EGAISTTN_INI="/linuxcash/cash/conf/ncash.ini.d/egaisttn.ini"
export EGAIS_XML="/linuxcash/cash/conf/plugins/egais.xml"

export INN_NCASH_INI="$(grep -ahoP '^inn.*?\K\d+' "$NCASH_INI")"
export KPP_NCASH_INI="$(grep -ahoP '^kpp.*?\K\d+' "$NCASH_INI")"
export NAME_NCASH_INI="$(grep -ahoP '^name.*?"\K.*(?="$)' "$NCASH_INI")"

export FSRAR_ID_1_EGAISTTN_INI="$(grep -ahF -A8 '[EgaisTTN]' "$EGAISTTN_INI" | grep -ahoP '^fsrarId.*?\K\d{12}')"
export FSRAR_ID_2_EGAISTTN_INI="$(grep -ahF -A8 '[EgaisTTN.nonexcise]' "$EGAISTTN_INI" | grep -ahoP '^fsrarId.*?\K\d{12}')"

export TERMINAL_LOG="/linuxcash/logs/current/terminal.log"
export CASHLOGS_DIR="/linuxcash/logs/cashlogs"
export ARCHIVE_LOGS_DIR="/linuxcash/logs/archive/logs"

export EGAISDOC1_DIR="/linuxcash/logs/current/egaisdocexcise"
export EGAISDOC2_DIR="/linuxcash/logs/current/egaisdocnonexcise"

export EXCHANGE="/linuxcash/net/server/server/exchange"

export TTNLOAD1_DIR="/root/ttnload"
export UTM1_URL="http://localhost:8082"
export TTN1_DIR="$TTNLOAD1_DIR/TTN"

export TTNLOAD2_DIR="/root/ttnload2"
export UTM2_URL="http://localhost:18082"
export TTN2_DIR="$TTNLOAD2_DIR/TTN"

export TTNLOAD_EXT_DIR="/root/ttnload_ext"
export UTM_EXT_URL="http://$(xmlstarlet sel -t -v '//*[@name = "address"]/value' "$EGAIS_XML" | grep -ahoP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d+' | grep -ahFv '127.0.0.1')"
export TTN_EXT_DIR="$TTNLOAD_EXT_DIR/TTN"

check_file_modification() {
	local file="$1"
	local interval=${2:-15} # Интервал в минутах, по умолчанию 15

	if [[ ! -f "$file" ]]; then
		return 1
	fi

	local current_time mod_time time_diff
	current_time=$(date +%s)
	mod_time=$(stat -c %Y "$file")
	time_diff=$((current_time - mod_time))

	if ((time_diff > interval * 60)); then
		return 0
	else
		return 1
	fi
}

function roundp() {
	if [[ "$1" ]]; then
		local num
		if printf -v num "%${2:+.$2}f" "$1"; then
			jq -n "$num + 0"
		fi
	fi
}

unset TMPDIR
export TMPDIR="$(mktemp -d)"
trap 'rm -rf -- "$TMPDIR"' EXIT

function prepare_document() {
	xmlstarlet pyx "$1" | grep -ahPv '^A' | sed -E 's/^([()]).*?:/\1/g' | xmlstarlet depyx | sed -E "s/'/'\"'\"'/g" >"$2"
}

function parse_waybill() {
	prepare_document "$1" "$2"
	source <(xmlstarlet sel -t -m '/*/*/WayBill_v4' \
		-o "WAYBILL_IDENTITY='" -v 'Identity' -o "'" -n \
		-o "WAYBILL_INTERNAL_NUMBER='" -v 'Header/NUMBER' -o "'" -n \
		-o "WAYBILL_DATE='" -v 'Header/Date' -o "'" -n \
		-o "WAYBILL_SHIPPING_DATE='" -v 'Header/ShippingDate' -o "'" -n \
		-o "WAYBILL_SHIPPER_SHORT_NAME='" -v 'Header/Shipper/*/ShortName' -o "'" -n \
		-o "WAYBILL_SHIPPER_FULL_NAME='" -v 'Header/Shipper/*/FullName' -o "'" -n \
		-o "WAYBILL_SHIPPER_ADDRESS='" -v 'Header/Shipper/*/address/description' -o "'" -n \
		-o "WAYBILL_SHIPPER_INN='" -v 'Header/Shipper/*/INN' -o "'" -n \
		-o "WAYBILL_SHIPPER_KPP='" -v 'Header/Shipper/*/KPP' -o "'" -n \
		-o "WAYBILL_SHIPPER_FSRAR_ID='" -v 'Header/Shipper/*/ClientRegId' -o "'" -n \
		-o "WAYBILL_CONSIGNEE_SHORT_NAME='" -v 'Header/Consignee/*/ShortName' -o "'" -n \
		-o "WAYBILL_CONSIGNEE_FULL_NAME='" -v 'Header/Consignee/*/FullName' -o "'" -n \
		-o "WAYBILL_CONSIGNEE_ADDRESS='" -v 'Header/Consignee/*/address/description' -o "'" -n \
		-o "WAYBILL_CONSIGNEE_INN='" -v 'Header/Consignee/*/INN' -o "'" -n \
		-o "WAYBILL_CONSIGNEE_KPP='" -v 'Header/Consignee/*/KPP' -o "'" -n \
		-o "WAYBILL_CONSIGNEE_FSRAR_ID='" -v 'Header/Consignee/*/ClientRegId' -o "'" -n \
		-o "WAYBILL_NUM_POSITIONS='" -v 'count(Content/Position)' -o "'" -n \
		-o "WAYBILL_NUM_BOXES='" -v 'count(Content/Position/InformF2/MarkInfo/boxpos)' -o "'" -n \
		-o "WAYBILL_NUM_EXCISE_MARKS='" -v 'count(Content/Position/InformF2/MarkInfo/boxpos/amclist/amc)' -o "'" -n \
		-o "WAYBILL_SUM=\"\$(printf '%.2f' \"\$(echo '" -v 'Content/Position/*[local-name() = "Quantity" or local-name() = "Price"]' -o "' | tr '\n' ' ' | sed -E 's/([0-9.]+)\s+([0-9.]+)\s*/\1*\2+/g;s/\+$//;\$a\' | bc -l)\")\"" "$2")
	WAYBILL_SHIPPER_NAME="${WAYBILL_SHIPPER_SHORT_NAME:-$WAYBILL_SHIPPER_FULL_NAME}"
	WAYBILL_CONSIGNEE_NAME="${WAYBILL_CONSIGNEE_SHORT_NAME:-$WAYBILL_CONSIGNEE_FULL_NAME}"
}

function parse_waybill_positions() {
	unset WAYBILL_POSITION_IDENTITY WAYBILL_POSITION_PRODUCT_FULLNAME WAYBILL_POSITION_PRODUCT_ALCCODE WAYBILL_POSITION_PRODUCT_CAPACITY WAYBILL_POSITION_PRODUCT_ALCVOLUME
	unset WAYBILL_POSITION_PRODUCT_PRODUCTVCODE WAYBILL_POSITION_NUM_BOXES WAYBILL_POSITION_NUM_EXCISE_MARKS WAYBILL_POSITION_PRICE WAYBILL_POSITION_EAN13 WAYBILL_POSITION_QUANTITY
	source <(xmlstarlet sel -t -m '/*/*/WayBill_v4/Content/Position' \
		-o "WAYBILL_POSITION_IDENTITY[" -v 'position()' -o "]='" -v 'Identity' -o "'" -n \
		-o "WAYBILL_POSITION_PRODUCT_FULLNAME[" -v 'position()' -o "]='" -v 'Product/FullName' -o "'" -n \
		-o "WAYBILL_POSITION_PRODUCT_ALCCODE[" -v 'position()' -o "]='" -v 'Product/AlcCode' -o "'" -n \
		-o "WAYBILL_POSITION_PRODUCT_CAPACITY[" -v 'position()' -o "]='" -v 'Product/Capacity' -o "'" -n \
		-o "WAYBILL_POSITION_PRODUCT_ALCVOLUME[" -v 'position()' -o "]='" -v 'Product/AlcVolume' -o "'" -n \
		-o "WAYBILL_POSITION_PRODUCT_PRODUCTVCODE[" -v 'position()' -o "]='" -v 'Product/ProductVCode' -o "'" -n \
		-o "WAYBILL_POSITION_NUM_BOXES[" -v 'position()' -o "]='" -v 'count(InformF2/MarkInfo/boxpos)' -o "'" -n \
		-o "WAYBILL_POSITION_NUM_EXCISE_MARKS[" -v 'position()' -o "]='" -v 'count(InformF2/MarkInfo/boxpos/amclist/amc)' -o "'" -n \
		-o "WAYBILL_POSITION_PRICE[" -v 'position()' -o "]='" -v 'Price' -o "'" -n \
		-o "WAYBILL_POSITION_EAN13[" -v 'position()' -o "]='" -v 'EAN13' -o "'" -n \
		-o "WAYBILL_POSITION_QUANTITY[" -v 'position()' -o "]='" -v 'Quantity' -o "'" -n "$1")
}

function parse_form2reginfo() {
	prepare_document "$1" "$2"
	source <(xmlstarlet sel -t -m '/*/*/TTNInformF2Reg' \
		-o "FORM2REGINFO_WBREGID='" -v 'Header/WBRegId' -o "'" -n \
		-o "FORM2REGINFO_INTERNAL_NUMBER='" -v 'Header/WBNUMBER' -o "'" -n \
		-o "FORM2REGINFO_DATE='" -v 'Header/WBDate' -o "'" -n \
		-o "FORM2REGINFO_SHIPPER_SHORT_NAME='" -v 'Header/Shipper/*/ShortName' -o "'" -n \
		-o "FORM2REGINFO_SHIPPER_FULL_NAME='" -v 'Header/Shipper/*/FullName' -o "'" -n \
		-o "FORM2REGINFO_SHIPPER_ADDRESS='" -v 'Header/Shipper/*/address/description' -o "'" -n \
		-o "FORM2REGINFO_SHIPPER_INN='" -v 'Header/Shipper/*/INN' -o "'" -n \
		-o "FORM2REGINFO_SHIPPER_KPP='" -v 'Header/Shipper/*/KPP' -o "'" -n \
		-o "FORM2REGINFO_SHIPPER_FSRAR_ID='" -v 'Header/Shipper/*/ClientRegId' -o "'" -n \
		-o "FORM2REGINFO_CONSIGNEE_SHORT_NAME='" -v 'Header/Consignee/*/ShortName' -o "'" -n \
		-o "FORM2REGINFO_CONSIGNEE_FULL_NAME='" -v 'Header/Consignee/*/FullName' -o "'" -n \
		-o "FORM2REGINFO_CONSIGNEE_ADDRESS='" -v 'Header/Consignee/*/address/description' -o "'" -n \
		-o "FORM2REGINFO_CONSIGNEE_INN='" -v 'Header/Consignee/*/INN' -o "'" -n \
		-o "FORM2REGINFO_CONSIGNEE_KPP='" -v 'Header/Consignee/*/KPP' -o "'" -n \
		-o "FORM2REGINFO_CONSIGNEE_FSRAR_ID='" -v 'Header/Consignee/*/ClientRegId' -o "'" -n \
		-o "FORM2REGINFO_NUM_POSITIONS='" -v 'count(Content/Position)' -o "'" -n "$2")
	FORM2REGINFO_SHIPPER_NAME="${FORM2REGINFO_SHIPPER_SHORT_NAME:-$FORM2REGINFO_SHIPPER_FULL_NAME}"
	FORM2REGINFO_CONSIGNEE_NAME="${FORM2REGINFO_CONSIGNEE_SHORT_NAME:-$FORM2REGINFO_CONSIGNEE_FULL_NAME}"
}

function parse_form2reginfo_positions() {
	unset FORM2REGINFO_POSITION_IDENTITY FORM2REGINFO_POSITION_INFORMF2REGID FORM2REGINFO_POSITION_BOTTLINGDATE
	source <(xmlstarlet sel -t -m '/*/*/TTNInformF2Reg/Content/Position' \
		-o "FORM2REGINFO_POSITION_IDENTITY[" -v 'position()' -o "]='" -v 'Identity' -o "'" -n \
		-o "FORM2REGINFO_POSITION_INFORMF2REGID[" -v 'Identity' -o "]='" -v 'InformF2RegId' -o "'" -n \
		-o "FORM2REGINFO_POSITION_BOTTLINGDATE[" -v 'position()' -o "]='" -v 'BottlingDate' -o "'" -n "$1")
}

function parse_replynattn() {
	prepare_document "$1" "$2"
	unset REPLYNATTN_NOANSWER_WBREGID REPLYNATTN_NOANSWER_TTNNUMBER REPLYNATTN_NOANSWER_TTNDATE REPLYNATTN_NOANSWER_SHIPPER
	source <(xmlstarlet sel -t -m '/*/*/ReplyNoAnswerTTN' \
		-o "REPLYNATTN_CONSIGNEE='" -v 'Consignee' -o "'" -n \
		-o "REPLYNATTN_NUM_NOANSWERS='" -v 'count(ttnlist/NoAnswer)' -o "'" -n \
		-m 'ttnlist/NoAnswer' \
		-o "REPLYNATTN_NOANSWER_WBREGID[" -v 'position()' -o "]='" -v 'WbRegID' -o "'" -n \
		-o "REPLYNATTN_NOANSWER_TTNNUMBER[" -v 'position()' -o "]='" -v 'ttnNumber' -o "'" -n \
		-o "REPLYNATTN_NOANSWER_TTNDATE[" -v 'position()' -o "]='" -v 'ttnDate' -o "'" -n \
		-o "REPLYNATTN_NOANSWER_SHIPPER[" -v 'position()' -o "]='" -v 'Shipper' -o "'" -n "$2")
}

function parse_ticket() {
	prepare_document "$1" "$2"
	source <(xmlstarlet sel -t -m '/*/*/Ticket' \
		-o "TICKET_TICKETDATE='" -v 'TicketDate' -o "'" -n \
		-o "TICKET_IDENTITY='" -v 'Identity' -o "'" -n \
		-o "TICKET_DOCID='" -v 'DocId' -o "'" -n \
		-o "TICKET_TRANSPORTID='" -v 'TransportId' -o "'" -n \
		-o "TICKET_REGID='" -v 'RegID' -o "'" -n \
		-o "TICKET_DOCHASH='" -v 'DocHash' -o "'" -n \
		-o "TICKET_DOCTYPE='" -v 'DocType' -o "'" -n \
		-o "TICKET_REGID='" -v 'RegID' -o "'" -n \
		-o "TICKET_OPERATIONRESULT_OPERATIONNAME='" -v 'OperationResult/OperationName' -o "'" -n \
		-o "TICKET_OPERATIONRESULT_OPERATIONRESULT='" -v 'OperationResult/OperationResult' -o "'" -n \
		-o "TICKET_OPERATIONRESULT_OPERATIONDATE='" -v 'OperationResult/OperationDate' -o "'" -n \
		-o "TICKET_OPERATIONRESULT_OPERATIONCOMMENT='" -v 'OperationResult/OperationComment' -o "'" -n \
		-o "TICKET_RESULT_CONCLUSION='" -v 'Result/Conclusion' -o "'" -n \
		-o "TICKET_RESULT_CONCLUSIONDATE='" -v 'Result/ConclusionDate' -o "'" -n \
		-o "TICKET_RESULT_COMMENTS='" -v 'Result/Comments' -o "'" -n "$2")
	if [[ "$TICKET_OPERATIONRESULT_OPERATIONDATE" ]]; then
		TICKET_OPERATIONRESULT_OPERATIONDATE="$(date --date="$TICKET_OPERATIONRESULT_OPERATIONDATE" "+%F %R")"
	fi
	if [[ "$TICKET_RESULT_CONCLUSIONDATE" ]]; then
		TICKET_RESULT_CONCLUSIONDATE="$(date --date="$TICKET_RESULT_CONCLUSIONDATE" "+%F %R")"
	fi
}

NC='\e[0m'

ATTR_RESET='\e[0m'
ATTR_BOLD='\e[1m'
ATTR_DIM='\e[2m'
ATTR_UNDERLINE='\e[4m'
ATTR_BLINK='\e[5m'
ATTR_REVERSE='\e[7m'
ATTR_HIDDEN='\e[8m'

FG_BLACK='\e[30m'
FG_RED='\e[31m'
FG_GREEN='\e[32m'
FG_YELLOW='\e[33m'
FG_BLUE='\e[34m'
FG_MAGENTA='\e[35m'
FG_CYAN='\e[36m'
FG_WHITE='\e[37m'

FG_BRIGHT_BLACK='\e[90m'
FG_BRIGHT_RED='\e[91m'
FG_BRIGHT_GREEN='\e[92m'
FG_BRIGHT_YELLOW='\e[93m'
FG_BRIGHT_BLUE='\e[94m'
FG_BRIGHT_MAGENTA='\e[95m'
FG_BRIGHT_CYAN='\e[96m'
FG_BRIGHT_WHITE='\e[97m'

BG_BLACK='\e[40m'
BG_RED='\e[41m'
BG_GREEN='\e[42m'
BG_YELLOW='\e[43m'
BG_BLUE='\e[44m'
BG_MAGENTA='\e[45m'
BG_CYAN='\e[46m'
BG_WHITE='\e[47m'

BG_BRIGHT_BLACK='\e[100m'
BG_BRIGHT_RED='\e[101m'
BG_BRIGHT_GREEN='\e[102m'
BG_BRIGHT_YELLOW='\e[103m'
BG_BRIGHT_BLUE='\e[104m'
BG_BRIGHT_MAGENTA='\e[105m'
BG_BRIGHT_CYAN='\e[106m'
BG_BRIGHT_WHITE='\e[107m'

function red_print() {
	if [[ "$1" ]]; then
		echo -ne "${FG_BRIGHT_RED}"
		echo -n "$1"
		echo -e "${NC}"
	fi
}

function green_print() {
	if [[ "$1" ]]; then
		echo -ne "${FG_GREEN}"
		echo -n "$1"
		echo -e "${NC}"
	fi
}
