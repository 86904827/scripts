#!/bin/bash

(

	flock -x -w 10 200 || exit 1

	source "${BASH_SOURCE%/*}/../common.sh"

	temp_file="$(mktemp)"
	temp_file2="$(mktemp)"

	waybill_optimized="$(mktemp)"
	form2reginfo_optimized="$(mktemp)"
	replynattn_optimized="$(mktemp)"
	ticket_optimized="$(mktemp)"

	function print_error() {
		echo "$(date -Iseconds):"$'\t'"$1" >>"$TTNLOAD1_DIR/log"
		tail -n10000 "$TTNLOAD1_DIR/log" | sponge "$TTNLOAD1_DIR/log"
		red_print "$1"
	}

	function search_utm() {
		local TTN_DIR="$1"
		local UTM_URL="$2"
		local EGAISDOC_DIR="$3"

		mkdir -p "$TTN_DIR"
		rm -r $(find -O3 "$TTN_DIR" -regextype posix-egrep -mindepth 1 -maxdepth 1 -type d -regex ".*/TTN-[[:digit:]]{10}" | sort -g | head -n -1000) &>/dev/null

		mapfile -t TTNHISTORYF2REG_url_array < <(curl -s "$UTM_URL/opt/out/TTNHISTORYF2REG" | grep -ahoPi '<url.*?>\K.*?(?=</url>)' | sort -rg)
		for ((i = 0; i < ${#TTNHISTORYF2REG_url_array[*]}; i++)); do
			if curl -s "${TTNHISTORYF2REG_url_array[i]}" 2>/dev/null | tr -d '\r' >"$temp_file" && xmllint --encode "UTF-8" --format --output "$temp_file" "$temp_file"; then
				TTNHISTORYF2REG_WBREGID="$(grep -ahoPiz '(?s)<wbr:Header.*?>.*?<wbr:WBRegId>\KTTN-\d{10}(?=</wbr:WBRegId>.*?</wbr:Header>)' "$temp_file")"
				mkdir -p "$TTN_DIR/$TTNHISTORYF2REG_WBREGID"
				rsync --quiet --perms --chmod=755 --checksum "$temp_file" "$TTN_DIR/$TTNHISTORYF2REG_WBREGID/TTNHISTORYF2REG.xml"
			fi
		done

		mapfile -t ReplyNATTN_url_array < <(curl -s "$UTM_URL/opt/out/ReplyNATTN" | grep -ahoPi '<url.*?>\K.*?(?=</url>)' | sort -rg)
		for ((i = 0; i < ${#ReplyNATTN_url_array[*]}; i++)); do
			if curl -s "${ReplyNATTN_url_array[i]}" 2>/dev/null | tr -d '\r' >"$temp_file" && xmllint --encode "UTF-8" --format --output "$temp_file" "$temp_file"; then
				parse_replynattn "$temp_file" "$replynattn_optimized"
				for ((j = 1; j <= REPLYNATTN_NUM_NOANSWERS; j++)); do
					echo -ne '\e[94m'
					echo -n " ${REPLYNATTN_NOANSWER_WBREGID[j]}"
					echo -ne '\t\e[36m'
					echo -n "${REPLYNATTN_NOANSWER_TTNNUMBER[j]}"
					echo -ne '\t\e[94m'
					echo -n "${REPLYNATTN_NOANSWER_TTNDATE[j]}"
					echo -ne '\t\e[0m'
					echo "ReplyNATTN/$(basename "${ReplyNATTN_url_array[i]}")"
					mkdir -p "$TTN_DIR/${REPLYNATTN_NOANSWER_WBREGID[j]}"
				done
			fi
		done

		mapfile -t FORM2REGINFO_url_array < <(curl -s "$UTM_URL/opt/out/FORM2REGINFO" | grep -ahoPi '<url.*?>\K.*?(?=</url>)' | sort -rg)
		for ((i = 0; i < ${#FORM2REGINFO_url_array[*]}; i++)); do
			if curl -s "${FORM2REGINFO_url_array[i]}" 2>/dev/null | tr -d '\r' >"$temp_file" && xmllint --encode "UTF-8" --format --output "$temp_file" "$temp_file"; then
				parse_form2reginfo "$temp_file" "$form2reginfo_optimized"
				if [[ -z "$FORM2REGINFO_WBREGID" ]]; then
					print_error "Не удалось получить WBRegId из FORM2REGINFO"
					continue
				fi
				echo -ne '\e[94m'
				echo -n " $FORM2REGINFO_WBREGID"
				echo -ne '\t\e[36m'
				echo -n "$FORM2REGINFO_INTERNAL_NUMBER"
				echo -ne '\t\e[94m'
				echo -n "$FORM2REGINFO_DATE"
				echo -ne '\t\e[0m'
				echo "FORM2REGINFO/$(basename "${FORM2REGINFO_url_array[i]}")"
				mkdir -p "$TTN_DIR/$FORM2REGINFO_WBREGID"
				rsync --quiet --perms --chmod=755 --checksum "$temp_file" "$TTN_DIR/$FORM2REGINFO_WBREGID/FORM2REGINFO.xml"
			fi
		done

		mapfile -t WayBill_v4_url_array < <(curl -s "$UTM_URL/opt/out/WayBill_v4" | grep -ahoPi '<url.*?>\K.*?(?=</url>)' | sort -rg)
		for ((i = 0; i < ${#WayBill_v4_url_array[*]}; i++)); do
			if curl -s "${WayBill_v4_url_array[i]}" 2>/dev/null | tr -d '\r' >"$temp_file" && xmllint --encode "UTF-8" --format --output "$temp_file" "$temp_file"; then
				parse_waybill "$temp_file" "$waybill_optimized"
				if [[ -z "$WAYBILL_INTERNAL_NUMBER" ]]; then
					print_error "Не удалось получить внутр. номер накладной из WayBill_v4"
					continue
				fi
				grep -aFlis "<wbr:WBNUMBER>$WAYBILL_INTERNAL_NUMBER</wbr:WBNUMBER>" $(find -O3 "$TTN_DIR" -mindepth 2 -maxdepth 2 -type f -mtime -30 -name "FORM2REGINFO.xml") /dev/null >"$temp_file2"
				form2reginfo_num="$(wc -l "$temp_file2" | cut -d' ' -f 1)"
				if ((form2reginfo_num == 0)); then
					print_error "Соответсвующий FORM2REGINFO не найден"
					continue
				fi

				if [[ "$WAYBILL_IDENTITY" ]]; then
					grep -aFlis "<wbr:Identity>$WAYBILL_IDENTITY</wbr:Identity>" $(<"$temp_file2") >"$temp_file2"
					form2reginfo_num="$(wc -l "$temp_file2" | cut -d' ' -f 1)"
				fi

				if ((form2reginfo_num == 0)); then
					print_error "Соответсвующий FORM2REGINFO не найден"
				elif ((form2reginfo_num == 1)); then
					WBREGID="$(grep -ahoPi '<wbr:WBRegId>\KTTN-\d{10}(?=</wbr:WBRegId>)' "$(<"$temp_file2")")"
					if [[ -z "$WBREGID" ]]; then
						print_error "Не удалось получить WBRegId из FORM2REGINFO"
						continue
					fi
					echo -ne '\e[94m'
					echo -n " $WBREGID"
					echo -ne '\t\e[36m'
					echo -n "$WAYBILL_INTERNAL_NUMBER"
					echo -ne '\t\e[94m'
					echo -n "$WAYBILL_DATE"
					echo -ne '\t\e[0m'
					echo "WayBill_v4/$(basename "${WayBill_v4_url_array[i]}")"
					mkdir -p "$TTN_DIR/$WBREGID"
					rsync --quiet --perms --chmod=755 --checksum "$temp_file" "$TTN_DIR/$WBREGID/WayBill_v4.xml"
				else
					print_error "Соответствующих FORM2REGINFO больше одного"
				fi
			fi
		done

		mapfile -t Ticket_url_array < <(curl -s "$UTM_URL/opt/out/Ticket" | grep -ahoPi '<url.*?>\K.*?(?=</url>)' | sort -rg)
		for ((i = 0; i < ${#Ticket_url_array[*]}; i++)); do
			if curl -s "${Ticket_url_array[i]}" 2>/dev/null | tr -d '\r' >"$temp_file" && xmllint --encode "UTF-8" --format --output "$temp_file" "$temp_file"; then
				parse_ticket "$temp_file" "$ticket_optimized"
				if [[ -z "$TICKET_REGID" ]]; then
					continue
				fi
				echo -ne '\e[94m'
				echo -n " $TICKET_REGID"
				echo -ne '\t\t\t\e[0m'
				echo "Ticket/$(basename "${Ticket_url_array[i]}")"
				mkdir -p "$TTN_DIR/$TICKET_REGID"
				rsync --quiet --perms --chmod=755 --checksum "$temp_file" "$TTN_DIR/$TICKET_REGID/Ticket_$(md5sum "$temp_file" | cut -f1 -d' ').xml"
				if xmllint --nowarning --noout "$TTN_DIR/$TICKET_REGID/Ticket.xml" &>/dev/null; then
					if [[ -z "$TICKET_TICKETDATE" ]]; then
						print_error "Отсутствует дата в Ticket"
						continue
					fi
					TICKET2_DATE="$(grep -ahoPi '<tc:TicketDate>\K.*?(?=</tc:TicketDate>)' "$TTN_DIR/$TICKET_REGID/Ticket.xml")"
					if [[ "$TICKET2_DATE" ]] && (("$(date --date "$TICKET_TICKETDATE" "+%s%N")" < "$(date --date "$TICKET2_DATE" "+%s%N")")); then
						continue
					fi
				fi
				rsync --quiet --perms --chmod=755 --checksum "$temp_file" "$TTN_DIR/$TICKET_REGID/Ticket.xml"
			fi
		done

		mapfile -t WayBillTicket_url_array < <(curl -s "$UTM_URL/opt/out/WayBillTicket" | grep -ahoPi '<url.*?>\K.*?(?=</url>)' | sort -rg)
		for ((i = 0; i < ${#WayBillTicket_url_array[*]}; i++)); do
			if curl -s "${WayBillTicket_url_array[i]}" 2>/dev/null | tr -d '\r' >"$temp_file" && xmllint --encode "UTF-8" --format --output "$temp_file" "$temp_file"; then
				WAYBILLTICKET_WBREGID="$(grep -ahoPi '<wt:WBRegId>\KTTN-\d{10}(?=</wt:WBRegId>)' "$temp_file")"
				if [[ -z "$WAYBILLTICKET_WBREGID" ]]; then
					print_error "Не удалось получить WBRegId из WayBillTicket"
					continue
				fi
				echo " $TTN_DIR/$WAYBILLTICKET_WBREGID/WayBillTicket.xml"
				mkdir -p "$TTN_DIR/$WAYBILLTICKET_WBREGID"
				rsync --quiet --perms --chmod=755 --checksum "$temp_file" "$TTN_DIR/$WAYBILLTICKET_WBREGID/WayBillTicket.xml"
			fi
		done

		for WayBillAct_Differences in $(grep -ahlFirs '<wa:IsAccept>Differences</wa:IsAccept>' "$EGAISDOC_DIR" 2>/dev/null); do
			WAYBILLACT_WBREGID="$(grep -ahoPi '<wa:WBRegId>\KTTN-\d{10}(?=</wa:WBRegId>)' "$WayBillAct_Differences")"
			if [[ -z "$WAYBILLACT_WBREGID" ]]; then
				print_error "Не удалось получить WBRegId из WayBillAct"
				continue
			fi
			echo " $TTN_DIR/$WAYBILLACT_WBREGID/WayBillAct_v3.xml"
			mkdir -p "$TTN_DIR/$WAYBILLACT_WBREGID"
			rsync --quiet --perms --chmod=755 --checksum "$WayBillAct_Differences" "$TTN_DIR/$WAYBILLACT_WBREGID/WayBillAct_v3.xml"
		done

		for TTN_REGID in $(find -O3 "$TTN_DIR" -regextype posix-egrep -mindepth 1 -maxdepth 1 -type d -mtime -30 -regex ".*/TTN-[[:digit:]]{10}" -printf '%f\n' | sort -g | tail -n 10); do

			WAYBILL_PATH="$TTN_DIR/$TTN_REGID/WayBill_v4.xml"
			FORM2REGINFO_PATH="$TTN_DIR/$TTN_REGID/FORM2REGINFO.xml"

			! xmllint --nowarning --noout "$WAYBILL_PATH" &>/dev/null
			WAYBILL_OK="$?"
			! xmllint --nowarning --noout "$FORM2REGINFO_PATH" &>/dev/null
			FORM2REGINFO_OK="$?"

			if ((!WAYBILL_OK && !FORM2REGINFO_OK)); then
				continue
			fi

			if ((WAYBILL_OK)); then
				parse_waybill "$WAYBILL_PATH" "$waybill_optimized"
				if ((($(date "+%s") - $(date --date="$WAYBILL_DATE" "+%s")) > 2592000)); then
					continue
				fi
				if [[ "${WAYBILL_CONSIGNEE_NAME::2}" == "ИП" ]]; then
					continue
				fi
				INN="$WAYBILL_CONSIGNEE_INN"
				KPP="$WAYBILL_CONSIGNEE_KPP"
			fi

			if ((FORM2REGINFO_OK)); then
				parse_form2reginfo "$FORM2REGINFO_PATH" "$form2reginfo_optimized"
				if [[ -z "$FORM2REGINFO_WBREGID" ]]; then
					print_error "Не удалось получить WBRegId из FORM2REGINFO"
					continue
				fi
				if [[ "$FORM2REGINFO_WBREGID" != "$TTN_REGID" ]]; then
					print_error "FORM2REGINFO $FORM2REGINFO_WBREGID != $TTN_REGID"
					continue
				fi
				if ((($(date "+%s") - $(date --date="$FORM2REGINFO_DATE" "+%s")) > 2592000)); then
					continue
				fi
				if [[ "${FORM2REGINFO_CONSIGNEE_NAME::2}" == "ИП" ]]; then
					continue
				fi
				if ((WAYBILL_OK)); then
					if [[ "$WAYBILL_CONSIGNEE_FSRAR_ID" != "$FORM2REGINFO_CONSIGNEE_FSRAR_ID" || "$WAYBILL_CONSIGNEE_INN" != "$FORM2REGINFO_CONSIGNEE_INN" || "$WAYBILL_CONSIGNEE_KPP" != "$FORM2REGINFO_CONSIGNEE_KPP" ]]; then
						print_error "WayBill_v4 $WAYBILL_CONSIGNEE_FSRAR_ID/$WAYBILL_CONSIGNEE_INN/$WAYBILL_CONSIGNEE_KPP != FORM2REGINFO $FORM2REGINFO_CONSIGNEE_FSRAR_ID/$FORM2REGINFO_CONSIGNEE_INN/$FORM2REGINFO_CONSIGNEE_KPP"
						continue
					fi
				fi
				INN="$FORM2REGINFO_CONSIGNEE_INN"
				KPP="$FORM2REGINFO_CONSIGNEE_KPP"
			fi

			mkdir "$EXCHANGE/$INN" &>/dev/null
			mkdir "$EXCHANGE/$INN/$KPP" &>/dev/null
			mkdir "$EXCHANGE/$INN/$KPP/$TTN_REGID" &>/dev/null
			rsync --quiet --size-only --partial --inplace --perms --chmod=755 --ignore-missing-args --times "$TTN_DIR/$TTN_REGID/WayBill_v4.xml" "$TTN_DIR/$TTN_REGID/FORM2REGINFO.xml" "$TTN_DIR/$TTN_REGID/Ticket.xml" "$TTN_DIR/$TTN_REGID/WayBillAct_v3.xml" "$TTN_DIR/$TTN_REGID/WayBillTicket.xml" "$EXCHANGE/$INN/$KPP/$TTN_REGID/"
		done
	}

	FSRAR_ID_UTM1="$(curl -s "$UTM1_URL/api/rsa/orginfo" | jq -r ".cn")"
	FSRAR_ID_UTM2="$(curl -s "$UTM2_URL/api/rsa/orginfo" | jq -r ".cn")"
	FSRAR_ID_UTM_EXT="$(curl -s "$UTM_EXT_URL/api/rsa/orginfo" | jq -r ".cn")"

	if [[ -z "$FSRAR_ID_UTM1" && -z "$FSRAR_ID_UTM2" && -z "$FSRAR_ID_UTM_EXT" ]]; then
		print_error "Не удалось получить ни 1, ни 2 FSRAR_ID"
		exit 1
	fi

	tabs 1,+5,+25,+28

	if [[ "$FSRAR_ID_UTM1" ]]; then
		if [[ "$FSRAR_ID_UTM1" != "$FSRAR_ID_1_EGAISTTN_INI" ]]; then
			print_error "УТМ1 $FSRAR_ID_UTM1 != egaisttn.ini FSRAR_ID_1 $FSRAR_ID_1_EGAISTTN_INI"
		fi
		org_info_utm1="$(mktemp)"
		curl -s "$UTM1_URL/api/rsa" | jq '.rows | arrays | map(select(.Owner_ID == "'"$FSRAR_ID_UTM1"'"))[0]' >"$org_info_utm1"
		INN_UTM1="$(jq -r ".INN" "$org_info_utm1")"
		KPP_UTM1="$(jq -r ".KPP" "$org_info_utm1")"
		NAME_UTM1="$(jq -r ".Full_Name" "$org_info_utm1")"
		if [[ -z "$INN_UTM1" || -z "$NAME_UTM1" ]]; then
			print_error "Не удалось получить данные организации по FSRAR_ID $FSRAR_ID_UTM1 из УТМ1"
		else
			if [[ "$INN_NCASH_INI" != "$INN_UTM1" || "$KPP_NCASH_INI" != "$KPP_UTM1" ]]; then
				print_error "УТМ1 $INN_UTM1/$KPP_UTM1 != ncash.ini $INN_NCASH_INI/$KPP_NCASH_INI"
			fi
		fi
		echo -ne '\e[96m'
		echo -n "УТМ1"
		echo -ne '\t\e[0m'
		echo -n "$INN_UTM1\\$KPP_UTM1"
		echo -ne '\t'
		echo -n "$INN_UTM1\\$FSRAR_ID_UTM1"
		echo -ne '\t\e[94m'
		echo "$NAME_UTM1"
	fi

	if [[ "$FSRAR_ID_UTM2" ]]; then
		if [[ "$FSRAR_ID_UTM2" != "$FSRAR_ID_2_EGAISTTN_INI" ]]; then
			print_error "УТМ2 $FSRAR_ID_UTM2 != egaisttn.ini FSRAR_ID_2 $FSRAR_ID_2_EGAISTTN_INI"
		fi
		org_info_utm2="$(mktemp)"
		curl -s "$UTM2_URL/api/rsa" | jq '.rows | arrays | map(select(.Owner_ID == "'"$FSRAR_ID_UTM2"'"))[0]' >"$org_info_utm2"
		INN_UTM2="$(jq -r ".INN" "$org_info_utm2")"
		KPP_UTM2="$(jq -r ".KPP" "$org_info_utm2")"
		NAME_UTM2="$(jq -r ".Full_Name" "$org_info_utm2")"
		if [[ -z "$INN_UTM2" || -z "$NAME_UTM2" ]]; then
			print_error "Не удалось получить данные организации по FSRAR_ID $FSRAR_ID_UTM2 из УТМ2"
		fi
		echo -ne '\e[96m'
		echo -n "УТМ2"
		echo -ne '\t\e[0m'
		echo -n "$INN_UTM2\\$KPP_UTM2"
		echo -ne '\t'
		echo -n "$INN_UTM2\\$FSRAR_ID_UTM2"
		echo -ne '\t\e[94m'
		echo "$NAME_UTM2"
	fi

	if [[ "$FSRAR_ID_UTM_EXT" ]]; then
		org_info_utm_ext="$(mktemp)"
		curl -s "$UTM_EXT_URL/api/rsa" | jq '.rows | arrays | map(select(.Owner_ID == "'"$FSRAR_ID_UTM_EXT"'"))[0]' >"$org_info_utm_ext"
		INN_UTM_EXT="$(jq -r ".INN" "$org_info_utm_ext")"
		KPP_UTM_EXT="$(jq -r ".KPP" "$org_info_utm_ext")"
		NAME_UTM_EXT="$(jq -r ".Full_Name" "$org_info_utm_ext")"
		if [[ -z "$INN_UTM_EXT" || -z "$NAME_UTM_EXT" ]]; then
			print_error "Не удалось получить данные организации по FSRAR_ID $FSRAR_ID_UTM_EXT из внешнего УТМ"
		fi
		echo -ne '\e[96m'
		echo -n "УТМ_внешний"
		echo -ne '\t\e[0m'
		echo -n "$INN_UTM_EXT\\$KPP_UTM_EXT"
		echo -ne '\t'
		echo -n "$INN_UTM_EXT\\$FSRAR_ID_UTM_EXT"
		echo -ne '\t\e[94m'
		echo "$NAME_UTM_EXT"
	fi

	tabs 1,+17,+17,+12

	if [[ "$FSRAR_ID_UTM1" ]]; then
		echo -ne '\e[96m'
		echo -n "УТМ1:"
		echo -e '\e[0m'
		search_utm "$TTN1_DIR" "$UTM1_URL" "$EGAISDOC1_DIR"
	fi

	if [[ "$FSRAR_ID_UTM2" ]]; then
		echo -ne '\e[96m'
		echo -n "УТМ2:"
		echo -e '\e[0m'
		search_utm "$TTN2_DIR" "$UTM2_URL" "$EGAISDOC2_DIR"
	fi

	if [[ "$FSRAR_ID_UTM_EXT" ]]; then
		echo -ne '\e[96m'
		echo -n "УТМ_внешний:"
		echo -e '\e[0m'
		search_utm "$TTN_EXT_DIR" "$UTM_EXT_URL"
	fi

) 200>/var/lock/.ttnload_start.exclusivelock
