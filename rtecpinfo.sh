#!/bin/bash

source "${BASH_SOURCE%/*}/common.sh"

for LIBPKCS11 in "/opt/aktivco/rutokenecp/i386/librtpkcs11ecp.so" \
	"/opt/aktivco/rutokenecp/amd64/librtpkcs11ecp.so" \
	"/usr/lib/librtpkcs11ecp.so" \
	"/opt/utm/lib/librtpkcs11ecp.so" \
	"/root/librtpkcs11ecp.so"; do
	if [[ -f "$LIBPKCS11" && -r "$LIBPKCS11" && -s "$LIBPKCS11" ]]; then
		break
	fi
done

LEGAL_PERSON_MAPPING_TABLE='
Караванова Ольга Александровна  , ООО "АКРУКС"    , 1659208718
Мухамедшина Айгюль Талгатовна   , ООО "АЛЬНИТАК"  , 1659208820
Кочкина Елена Николаевна        , ООО "АСЦЕЛЛА"   , 1659211693
Фахрутдинов Ильдар Агзамович    , ООО "МЕЙССА"    , 1644096744
Долгих Юлия Александровна       , ООО "МИНТАКА"   , 1659208740
Реданских Наталья Александровна , ООО "ТАБИТ"     , 1659208757
Исламов Альберт Габдулхаевич    , ООО "АЛЬНИЛАМ"  , 1659208845
Митрофанова Яна Юрьевна         , ООО "БЕЙД"      , 1659208838
Тотушева Зайнаб Ибрагимовна     , ООО "АЛИАЛ"     , 1660349657
Порфирьева Светлана Борисовна   , ООО "ВИКТОРИЯ"  , 1644096180
Сулейманова Алия Ришатовна      , ООО "ВЫМПЕЛ"    , 1660347201
Загирова Гульназ Хафизовна      , ООО "ДИАЛКО"    , 1660346991
Саетова Оксана Владимировна     , ООО "КРОНА"     , 1660340005
Зиманова Ирина Петровна         , ООО "ПЕГАС"     , 1657253779
Анисимова Наталья Николаевна    , ООО "РУСМАРКЕТ" , 1660343863
Гаврилова Александра Юрьевна    , ООО "САТУ"      , 1660349488
Григорьева Елена Николаевна     , ООО "ЮЛАЛКО"    , 1660344472'

token_slots="$(mktemp)"
token_slot="$(mktemp)"
certificates="$(mktemp)"
certificate="$(mktemp)"
subject="$(mktemp)"

pkcs11-tool --module "$LIBPKCS11" --list-token-slots >"$token_slots"
for slot_index in $(grep -aioP 'Slot\s+\K\d+' "$token_slots"); do
	grep -aiF -A7 "Slot $slot_index" "$token_slots" >"$token_slot"
	echo -ne '\e[94m'

	SERIAL_NUM="$(grep -aioP 'serial num\s*:\s*\K.*' "$token_slot")"
	TOKEN_MODEL="$(grep -aioP 'token model\s*:\s*\K.*' "$token_slot")"
	if pkcs11-tool --module "$LIBPKCS11" --slot-index "$slot_index" --list-mechanisms |& grep -aiF 'ECDSA' &>/dev/null; then
		TOKEN_MODEL="$TOKEN_MODEL (>=)3.0"
	else
		TOKEN_MODEL="$TOKEN_MODEL (<=)2.0"
	fi
	echo "$TOKEN_MODEL $SERIAL_NUM ($((0x$SERIAL_NUM))) номер $slot_index:"

	pkcs11-tool --module "$LIBPKCS11" --slot-index "$slot_index" --list-objects --type cert 2>&1 >"$certificates" | grep -aiFv 'Using slot'
	for id in $(grep -aioP '^\s*ID:\s*\K\w+' "$certificates" | sort -rg | uniq); do
		pkcs11-tool --module "$LIBPKCS11" --slot-index "$slot_index" --read-object --type cert --id "$id" 2>&1 >"$certificate" | grep -aiFv 'Using slot'
		if [[ -s "$certificate" ]]; then
			openssl x509 -in "$certificate" -inform DER -subject -noout -nameopt utf8,sep_multiline,lname >"$subject"
			NOT_AFTER_DATE="$(openssl x509 -in "$certificate" -inform DER -enddate -noout | grep -aioP 'notAfter=\s*\K.*')"
			if [[ "$NOT_AFTER_DATE" ]]; then
				echo -ne '\e[36m'
				DAYS_LEFT="$((($(date --date="$NOT_AFTER_DATE" '+%s') - $(date '+%s')) / 86400))"
				echo -n "  $(date --date="$NOT_AFTER_DATE" '+%H:%M %d.%m.%y') ($DAYS_LEFT)"
			fi
			echo -ne '\t'

			if openssl x509 -in "$certificate" -inform DER -noout -checkend 0 &>/dev/null; then
				echo -ne '\e[32m'
			else
				echo -ne '\e[91m'
			fi

			mapfile -t COMMON_NAME < <(grep -aioP 'commonName=\s*\K.*' "$subject" | tr -s ' ' '\n')
			if [[ "${COMMON_NAME[0]}" && "${COMMON_NAME[1]}" && "${COMMON_NAME[2]}" ]] &&
				matching_line="$(echo -n "$LEGAL_PERSON_MAPPING_TABLE" | grep -aiP "(?=.*${COMMON_NAME[0]})(?=.*${COMMON_NAME[1]})(?=.*${COMMON_NAME[2]})")"; then
				NAME="$(echo -n "$matching_line" | cut -d ',' -f 2 | grep -aioP '\S.*\S')"
				INN="$(echo -n "$matching_line" | cut -d ',' -f 3 | grep -aioP '\S.*\S')"
			else
				NAME="${COMMON_NAME[*]}"
				INN="$(grep -aioP '1.2.643.100.4=\s*\K.*|1.2.643.3.131.1.1=\s*\K.*' "$subject")"
			fi
			echo -n "$NAME"
			if [[ "$INN" ]]; then
				echo -ne '\e[36m'
				echo -n " / $INN"
			fi
			echo
		fi
	done
done

echo -ne '\e[0m'
