#!/bin/bash

(

	flock -x -w 10 200 || exit 1

	source "${BASH_SOURCE%/*}/common.sh"

	temp_file="$(mktemp)"

	function write_notice() {
		echo -n '{"message":"' >"/linuxcash/cash/data/tmp/notification_temp.json"
		echo -n "$1" | sed 's/\"/\\\"/g' >>"/linuxcash/cash/data/tmp/notification_temp.json"
		echo '", "action": "undefined"}' >>"/linuxcash/cash/data/tmp/notification_temp.json"
		mv "/linuxcash/cash/data/tmp/notification_temp.json" "/linuxcash/cash/data/tmp/notification.json"
	}

	function clear_notice() {
		write_notice ""
	}

	mkdir -p "/root/notifications"

	while true; do
		for notification_file in $(find -O3 "/root/notifications" -mindepth 1 -maxdepth 1 -type f -readable -not -empty); do
			while true; do
				SCREEN_WIDTH="$(xdpyinfo 2>/dev/null | grep -aoP 'dimensions:\h+\K\d+')"
				if [[ "$SCREEN_WIDTH" ]]; then
					break
				fi
				red_print "Не удалось получить ширину экрана"
				sleep 10s
			done
			NOTICE_MAX_LENGTH="$((SCREEN_WIDTH * 72 / 1000 - 2))"
			NOTIFICATION_FILE_CONTENT="$(<"$notification_file")"
			if ((NOTICE_MAX_LENGTH < ${#NOTIFICATION_FILE_CONTENT})); then
				for ((i = 1; i <= NOTICE_MAX_LENGTH; i += 5)); do
					write_notice "${NOTIFICATION_FILE_CONTENT:0:i}"
					sleep 1s
				done
				write_notice "${NOTIFICATION_FILE_CONTENT:0:NOTICE_MAX_LENGTH}"
				sleep 5s
				for ((i = 5; i <= ${#NOTIFICATION_FILE_CONTENT} - NOTICE_MAX_LENGTH; i += 5)); do
					write_notice "${NOTIFICATION_FILE_CONTENT:i:NOTICE_MAX_LENGTH}"
					sleep 1.5s
				done
				write_notice "${NOTIFICATION_FILE_CONTENT: -NOTICE_MAX_LENGTH}"
				sleep 10s
			else
				for ((i = 1; i <= ${#NOTIFICATION_FILE_CONTENT}; i += 5)); do
					write_notice "${NOTIFICATION_FILE_CONTENT:0:i}"
					sleep 1s
				done
				write_notice "$NOTIFICATION_FILE_CONTENT"
				sleep 15s
			fi

			clear_notice

			sleep "$((RANDOM % 20 + 10))s"
		done
		sleep "$((RANDOM % 10 + 5))m"
	done

) 200>/var/lock/.notificationd.exclusivelock
