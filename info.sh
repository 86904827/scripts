#!/bin/bash

source "${BASH_SOURCE%/*}/common.sh"

FSRAR_ID_UTM1="$(curl -s "$UTM1_URL/api/rsa/orginfo" | jq -r ".cn | values")"
FSRAR_ID_UTM2="$(curl -s "$UTM2_URL/api/rsa/orginfo" | jq -r ".cn | values")"

if [[ "$FSRAR_ID_UTM1" ]]; then
	ORG_INFO_UTM1="$(curl -s "$UTM1_URL/api/rsa" | grep -ahoP ".*\K{[^{]*?$FSRAR_ID_UTM1.*?}")"
	INN_UTM1="$(echo "$ORG_INFO_UTM1" | grep -ahoPi '"inn"\W?"\K\d+(?=")')"
	KPP_UTM1="$(echo "$ORG_INFO_UTM1" | grep -ahoPi '"kpp"\W?"\K\d+(?=")')"
	NAME_UTM1="$(echo "$ORG_INFO_UTM1" | grep -ahoPi '"full_name"\W?"\K.*?(?=(?<!\\)")')"
	if [[ -z "$INN_UTM1" || -z "$NAME_UTM1" ]]; then
		red_print "Не удалось получить данные организации по FSRAR_ID $FSRAR_ID_UTM1"
	fi
fi
if [[ "$FSRAR_ID_UTM2" ]]; then
	ORG_INFO_UTM2="$(curl -s "$UTM2_URL/api/rsa" | grep -ahoP ".*\K{[^{]*?$FSRAR_ID_UTM2.*?}")"
	INN_UTM2="$(echo "$ORG_INFO_UTM2" | grep -ahoPi '"inn"\W?"\K\d+(?=")')"
	KPP_UTM2="$(echo "$ORG_INFO_UTM2" | grep -ahoPi '"kpp"\W?"\K\d+(?=")')"
	NAME_UTM2="$(echo "$ORG_INFO_UTM2" | grep -ahoPi '"full_name"\W?"\K.*?(?=(?<!\\)")')"
	if [[ -z "$INN_UTM2" || -z "$NAME_UTM2" ]]; then
		red_print "Не удалось получить данные организации по FSRAR_ID $FSRAR_ID_UTM2"
	fi
fi

tabs 1,18,+25,+28


echo -e "exchange:\t$(<flags/exchangesystems)"
echo -e "loaddate:\t$(<flags/loaddate)"

if [[ "$FSRAR_ID_UTM1" && "$FSRAR_ID_UTM1" != "$FSRAR_ID_1_EGAISTTN_INI" ]]; then
	red_print "FSRAR_ID 1 в egaisttn.ini и УТМ1 не совпадают"
	fsrar_id_mismatch=true
fi
if [[ "$FSRAR_ID_UTM2" && "$FSRAR_ID_UTM2" != "$FSRAR_ID_2_EGAISTTN_INI" ]]; then
	red_print "FSRAR_ID 2 в egaisttn.ini и УТМ2 не совпадают"
	fsrar_id_mismatch=true
fi
if [[ "$fsrar_id_mismatch" == "true" ]]; then
	echo -e "egaisttn.ini:\t1: $FSRAR_ID_1_EGAISTTN_INI 2: $FSRAR_ID_2_EGAISTTN_INI"
fi
if [[ "$FSRAR_ID_UTM1" || "$FSRAR_ID_UTM2" ]]; then
	echo -e "FSRAR_ID:\t1: $FSRAR_ID_UTM1 2: $FSRAR_ID_UTM2"
else
	echo -e "egaisttn.ini:\t1: $FSRAR_ID_1_EGAISTTN_INI 2: $FSRAR_ID_2_EGAISTTN_INI"
fi

if [[ "$INN_NCASH_INI" != "$INN_UTM1" || "$KPP_NCASH_INI" != "$KPP_UTM1" ]]; then
	if [[ "$INN_UTM1" ]]; then
		red_print "ИНН\\КПП в ncash.ini и УТМ1 не совпадают"
	fi
	echo -e "ncash.ini:\t$INN_NCASH_INI\\\\$KPP_NCASH_INI\t$INN_NCASH_INI\\\\$FSRAR_ID_1_EGAISTTN_INI\t$NAME_NCASH_INI"
fi
if [[ "$INN_UTM1" ]]; then
	echo -e "УТМ1:\t$INN_UTM1\\\\$KPP_UTM1\t$INN_UTM1\\\\$FSRAR_ID_UTM1\t$NAME_UTM1"
fi
if [[ "$INN_UTM2" ]]; then
	echo -e "УТМ2:\t$INN_UTM2\\\\$KPP_UTM2\t$INN_UTM2\\\\$FSRAR_ID_UTM2\t$NAME_UTM2"
fi

tabs -8

#"${BASH_SOURCE%/*}/rtecpinfo.sh"
"${BASH_SOURCE%/*}/kkminfo.sh"
