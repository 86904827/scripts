#!/bin/bash

source "${BASH_SOURCE%/*}/common.sh"

waybill_optimized="$(mktemp)"

disai_html_data="$(mktemp)"

barcodes="$(mktemp)"

MINRETAILPRICE_LITRE_VODKA_ALKOTORG="$((281 * 2))"
MINRETAILPRICE_LITRE_VODKA_OTHER="$((412 * 2))"
MINRETAILPRICE_LITRE_COGNAC="$((517 * 2))"
MINRETAILPRICE_LITRE_BRANDY="$((375 * 2))"
MINRETAILPRICE_LITRE_SPARKLING_WINE="$(echo "239 / 0.75" | bc -l)"

function print_error() {
	local message="$1"
	red_print "$(date '+%H:%M:%S') $barcode : $message"
}

function check_bananas() {
	if ! check_name "$(mysql --skip-column-names -B -e "SELECT HIGH_PRIORITY name FROM dictionaries.barcodes WHERE barcode = '202105002';")"; then
		echo -ne '\e[91;4m'
		echo -n "Возможно своя 1С!"
		echo -e '\e[0m'
	fi
}

function check_name() {
	[[ "$1" && "$1" != "NULL" ]]
}

function check_barcode() {
	[[ "$1" =~ ^[[:digit:]]{1,16}$ ]]
}

function check_department() {
	[[ "$1" =~ ^[1-6]$ ]]
}

function check_price() {
	[[ "$1" =~ ^[[:digit:]]+\.?[[:digit:]]*$ ]] && (($(echo "$1 > 1" | bc -l) == 1))
}

function check_number() {
	[[ "$1" =~ ^[[:digit:]]+$ ]]
}

function get_properties() {
	local properties
	mapfile -t properties < <(mysql --skip-column-names -B -e "
		SELECT HIGH_PRIORITY COUNT(NULL),        code, name, measure, tmctype, price FROM dictionaries.barcodes1 WHERE barcode = '$barcode';
		SELECT HIGH_PRIORITY COUNT(NULL),        code, name, measure, tmctype, price FROM dictionaries.barcodes2 WHERE barcode = '$barcode';
		SELECT HIGH_PRIORITY COUNT(NULL), dcode, code, name, measure, op_mode, price FROM dictionaries.tmc1      WHERE bcode = '$barcode';
		SELECT HIGH_PRIORITY COUNT(NULL), dcode, code, name, measure, op_mode, price FROM dictionaries.tmc2      WHERE bcode = '$barcode';
" | cut -d $'\t' -f 2- | tr '\t' '\n')

	if ! check_name "$name"; then
		if check_name "${properties[1]}"; then
			name="${properties[1]}"
		elif check_name "${properties[6]}"; then
			name="${properties[6]}"
		elif check_name "${properties[12]}"; then
			name="${properties[12]}"
		elif check_name "${properties[18]}"; then
			name="${properties[18]}"
		fi
	fi

	if ! check_number "$code"; then
		if check_number "${properties[0]}"; then
			code="${properties[0]}"
		elif check_number "${properties[5]}"; then
			code="${properties[5]}"
		elif check_number "${properties[11]}"; then
			code="${properties[11]}"
		elif check_number "${properties[17]}"; then
			code="${properties[17]}"
		fi
	fi

	if ! check_department "$department"; then
		if check_department "${properties[10]}"; then
			department="${properties[10]}"
		elif check_department "${properties[16]}"; then
			department="${properties[16]}"
		fi
	fi

	if ! check_number "$measure"; then
		if check_number "${properties[2]}"; then
			measure="${properties[2]}"
		elif check_number "${properties[7]}"; then
			measure="${properties[7]}"
		elif check_number "${properties[13]}"; then
			measure="${properties[13]}"
		elif check_number "${properties[19]}"; then
			measure="${properties[19]}"
		fi
	fi

	if ! check_number "$tmctype"; then
		if check_number "${properties[3]}"; then
			tmctype="${properties[3]}"
		elif check_number "${properties[8]}"; then
			tmctype="${properties[8]}"
		fi
	fi

	if ! check_number "$op_mode"; then
		if check_number "${properties[14]}"; then
			op_mode="${properties[14]}"
		elif check_number "${properties[20]}"; then
			op_mode="${properties[20]}"
		fi
	fi

	if ! check_price "$price"; then
		if check_price "${properties[4]}"; then
			price="${properties[4]}"
		elif check_price "${properties[9]}"; then
			price="${properties[9]}"
		elif check_price "${properties[15]}"; then
			price="${properties[15]}"
		elif check_price "${properties[21]}"; then
			price="${properties[21]}"
		fi
	fi
}

function select_properties() {
	local properties
	mapfile -t properties < <(mysql --skip-column-names -B -e "
		SELECT HIGH_PRIORITY COUNT(NULL), code, name, measure, tmctype, price FROM dictionaries.barcodes WHERE barcode = '$barcode';
		SELECT HIGH_PRIORITY COUNT(NULL), dcode, op_mode FROM dictionaries.tmc WHERE code = (SELECT code FROM dictionaries.barcodes WHERE barcode = '$barcode');
" | cut -d $'\t' -f 2- | tr '\t' '\n')

	if ! check_number "$code"; then
		if check_number "${properties[0]}"; then
			code="${properties[0]}"
		fi
	fi

	if ! check_name "$name"; then
		if check_name "${properties[1]}"; then
			name="${properties[1]}"
		fi
	fi

	if ! check_number "$measure"; then
		if check_number "${properties[2]}"; then
			measure="${properties[2]}"
		fi
	fi

	if ! check_number "$tmctype"; then
		if check_number "${properties[3]}"; then
			tmctype="${properties[3]}"
		fi
	fi

	if ! check_price "$price"; then
		if check_price "${properties[4]}"; then
			price="${properties[4]}"
		fi
	fi

	if ! check_department "$department"; then
		if check_department "${properties[5]}"; then
			department="${properties[5]}"
		fi
	fi

	if ! check_number "$op_mode"; then
		if check_number "${properties[6]}"; then
			op_mode="${properties[6]}"
		fi
	fi
}

function null_properties() {
	code=
	name=
	department=
	op_mode=
	tmctype=
	measure=
	price=
}

function print_properties() {
	echo -ne '\e[32m'

	if ! check_barcode "$barcode"; then
		echo -ne '\e[91m'
		local barcode=-
	fi

	if ! check_name "$name"; then
		echo -ne '\e[91m'
		local name=-
	fi

	if ! check_department "$department"; then
		echo -ne '\e[91m'
		local department=-
	fi

	if ! check_number "$op_mode"; then
		local op_mode=-
	fi

	if ! check_number "$tmctype"; then
		local tmctype=-
	fi

	if ! check_number "$measure"; then
		local measure=-
	fi

	if ! check_price "$price"; then
		local price=-
	fi

	echo -e "$barcode\t$department\t$op_mode\t$tmctype\t$measure\t$price\t$name"
}

function good() {

	if check_barcode "$barcode"; then
		local barcode="$barcode"
	else
		return
	fi

	if check_name "$name"; then
		local name="$name"
	else
		local name
	fi

	if check_department "$department"; then
		local department="$department"
	else
		local department
	fi

	if check_number "$op_mode"; then
		local op_mode="$op_mode"
	else
		local op_mode
	fi

	if check_number "$tmctype"; then
		local tmctype="$tmctype"
	else
		local tmctype
	fi

	if check_number "$measure"; then
		local measure="$measure"
	else
		local measure
	fi

	if check_price "$price"; then
		local price="$price"
	else
		local price
	fi

	if check_number "$quant_mode"; then
		local quant_mode="$quant_mode"
	else
		local quant_mode
	fi

	local code price_mode minprice minretailprice_litre minretailprice

	if ! check_name "$name" || ! check_number "$measure" || ! check_number "$tmctype" || ! check_department "$department" || ! check_number "$op_mode"; then
		for WAYBILL_PATH in $(grep -aFls "EAN13>$barcode<" $(find -O3 "$TTN1_DIR" "$TTN_EXT_DIR" -mindepth 2 -maxdepth 2 -mtime -180 -name "WayBill_v4.xml" 2>/dev/null) $(find -O3 "$TTN2_DIR" -mindepth 2 -maxdepth 2 -mtime -60 -name "WayBill_v4.xml" 2>/dev/null) /dev/null | sort -rg); do
			parse_waybill "$WAYBILL_PATH" "$waybill_optimized"
			parse_waybill_positions "$waybill_optimized"
			source <(xmlstarlet sel -t \
				-o "WAYBILL_POSITION_EAN13_INDEX='" -v 'count(/*/*/WayBill_v4/Content/Position[EAN13 = "'"$barcode"'"][1]/preceding-sibling::Position)+1' -o "'" -n "$waybill_optimized")
			if [[ "$WAYBILL_POSITION_EAN13_INDEX" ]]; then
				if ! check_name "$name"; then
					if [[ "${WAYBILL_POSITION_PRODUCT_CAPACITY[WAYBILL_POSITION_EAN13_INDEX]}" ]]; then
						name="${WAYBILL_POSITION_PRODUCT_FULLNAME[WAYBILL_POSITION_EAN13_INDEX]} $(python2 -c "print('{:g}'.format(float(${WAYBILL_POSITION_PRODUCT_CAPACITY[WAYBILL_POSITION_EAN13_INDEX]})))")л"
					else
						name="${WAYBILL_POSITION_PRODUCT_FULLNAME[WAYBILL_POSITION_EAN13_INDEX]}"
					fi
				fi
				local vcode="${WAYBILL_POSITION_PRODUCT_PRODUCTVCODE[WAYBILL_POSITION_EAN13_INDEX]}"
				if ((vcode >= 500 && vcode <= 537)); then
					if ! check_department "$department"; then
						department=2
					fi
					if ! check_number "$op_mode"; then
						op_mode=64
					fi
				else
					if ! check_department "$department"; then
						department=1
					fi
					if ! check_number "$op_mode"; then
						op_mode=192
					fi
					if ! check_number "$tmctype"; then
						tmctype=1
					fi
					if ! check_number "$measure"; then
						measure=1
					fi

					if ((vcode == 200 || (vcode >= 2001 && vcode <= 2004))); then
						if ((WAYBILL_SHIPPER_INN == 1659091192)); then
							minretailprice_litre="$MINRETAILPRICE_LITRE_VODKA_ALKOTORG"
						else
							minretailprice_litre="$MINRETAILPRICE_LITRE_VODKA_OTHER"
						fi
					elif ((vcode == 229 || vcode == 2291 || vcode == 2292 || vcode == 230 || vcode == 231 || vcode == 233 || vcode == 2331 || vcode == 234 || vcode == 2341 || vcode == 235 || vcode == 2351)); then
						minretailprice_litre="$MINRETAILPRICE_LITRE_COGNAC"
					elif ((vcode == 232 || (vcode >= 2321 && vcode <= 2327) || vcode == 28011 || vcode == 28012)); then
						minretailprice_litre="$MINRETAILPRICE_LITRE_BRANDY"
					elif ((vcode == 4219 || vcode == 42110 || vcode == 42111 || vcode == 440 || vcode == 4401 || vcode == 4402 || vcode == 441 || vcode == 4411 || vcode == 4422 || vcode == 442 || vcode == 4421 || vcode == 443 || vcode == 450 || vcode == 4501 || vcode == 451 || vcode == 452 || vcode == 453 || vcode == 4531)); then
						minretailprice_litre="$MINRETAILPRICE_LITRE_SPARKLING_WINE"
					else
						minretailprice_litre=
					fi
					if [[ "$minretailprice_litre" ]]; then
						minretailprice="$(echo "$minretailprice_litre * ${WAYBILL_POSITION_PRODUCT_CAPACITY[WAYBILL_POSITION_EAN13_INDEX]}" | bc -l)"
					else
						minretailprice=10
					fi
				fi
				break
			fi
		done
	fi

	select_properties

	if ! check_number "$code" || ! check_name "$name" || ! check_number "$measure" || ! check_number "$tmctype" || ! check_price "$price" || ! check_department "$department" || ! check_number "$op_mode"; then
		get_properties
	fi

	if ! check_name "$name"; then
		if [[ "$disai_limit_exceeded" == "true" ]]; then
			print_error "DISAI лимит превышен"
			return 1
		fi

		curl --compressed -s -F "search_query=$barcode" "http://ru.disai.org/" | iconv -f "WINDOWS-1251" -t "UTF-8" >"$disai_html_data"
		if grep -ahFi 'исчерпали количество возможных обращений к базе данных откликов торговых сетей' "$disai_html_data" >/dev/null; then
			disai_limit_exceeded=true
			print_error "DISAI лимит превышен"
			return 1
		else
			name="$(grep -ahoP -m1 '383838.*?>\K.*?(?=<)' "$disai_html_data")"
			if ! check_name "$name"; then
				name="$(grep -ahoP -m1 '<title>\K.*(?=, .*?</title>)' "$disai_html_data")"
			fi
		fi
	fi

	if check_name "$name"; then
		name="${name//\\n/ }"
		name="$(echo "$name" | tr -s ' ')"

		if ((op_mode & 32768 || tmctype == 3)); then
			department=3
			op_mode=32768
			tmctype=3
			measure=1
		elif ((op_mode & 128 || tmctype == 1)); then
			department=1
			op_mode=192
			tmctype=1
			measure=1

			if ! check_price "$minretailprice"; then
				minretailprice=10
			fi
		fi

		if ! check_number "$code"; then
			code="$barcode"
		fi

		if ! check_department "$department"; then
			department=4
		fi

		if ! check_number "$op_mode"; then
			op_mode=0
		fi

		if ! check_number "$tmctype"; then
			tmctype=0
		fi

		if ! check_number "$measure"; then
			measure=1
		fi

		if check_price "$price"; then
			price_mode=1
		else
			price=NULL
			price_mode=2
		fi

		if ! check_number "$quant_mode"; then
			quant_mode=15
		fi

		local _name="${name//\'/\\\'}"
		mysql -e "
REPLACE INTO dictionaries.barcodes1 (name, code, barcode, tmctype, measure, price, minprice, minretailprice) VALUES ('$_name', '$code', '$barcode', '$tmctype', '$measure', '$price', '$minprice', '$minretailprice');
REPLACE INTO dictionaries.barcodes2 (name, code, barcode, tmctype, measure, price, minprice, minretailprice) VALUES ('$_name', '$code', '$barcode', '$tmctype', '$measure', '$price', '$minprice', '$minretailprice');
REPLACE INTO dictionaries.tmc1 (name, code, bcode, dcode, op_mode, measure, price, minprice, minretailprice, price_mode, bcode_mode, quant_mode, vatcode1, vatcode2, vatcode3, vatcode4, vatcode5) VALUES ('$_name', '$code', '$barcode', '$department', '$op_mode', '$measure', '$price', '$minprice', '$minretailprice', '$price_mode', '3', '$quant_mode', '301', '301', '301', '301', '301');
REPLACE INTO dictionaries.tmc2 (name, code, bcode, dcode, op_mode, measure, price, minprice, minretailprice, price_mode, bcode_mode, quant_mode, vatcode1, vatcode2, vatcode3, vatcode4, vatcode5) VALUES ('$_name', '$code', '$barcode', '$department', '$op_mode', '$measure', '$price', '$minprice', '$minretailprice', '$price_mode', '3', '$quant_mode', '301', '301', '301', '301', '301');
DELETE IGNORE FROM dictionaries.prices1 WHERE barcode='$barcode';
DELETE IGNORE FROM dictionaries.prices2 WHERE barcode='$barcode';"
	fi
	print_properties
}

function find_barcodes() {
	grep -ahoPs 'Поиск товара со штрих-кодом: \K\d{1,16}' $(find -O3 "$CASHLOGS_DIR" -mindepth 2 -maxdepth 2 -type f -readable -name "terminal.log" -mtime -7 | sort -g) "$TERMINAL_LOG" | sort -r -g -u >"$barcodes"
}

function parse_barcode_parameter() {
	if check_barcode "$1"; then
		barcode_array="$1"
	elif [[ "$1" =~ ^[[:digit:]]{1,16}-[[:digit:]]{1,16}$ ]]; then
		mapfile -t barcode_array < <(seq $(echo "$1" | tr '-' ' '))
	else
		return 1
	fi
	return 0
}

check_bananas

tabs 1,+17,+3,+6,+2,+6,+11,+9

if [[ "$1" || "$2" ]]; then
	if [[ "$1" == "a" || "$1" == "а" ]]; then
		find_barcodes
		if [[ -s "$barcodes" ]]; then
			echo -ne '\e[36m'
			echo "Штрихкоды ($(wc -l "$barcodes" | cut -f1 -d' ')):"
			for barcode in $(<"$barcodes"); do
				if [[ "$barcode" =~ ^[[:digit:]]{8}$ || "$barcode" =~ ^[[:digit:]]{13}$ ]]; then
					good
				fi
			done
			echo
		fi
	else
		if parse_barcode_parameter "$1"; then
			department="$2"
			flags="$3"
			price="$4"
		else
			if [[ -f "$1" ]]; then
				if [[ -r "$1" && -s "$1" ]]; then
					mapfile -t name_array <"$1"
				fi
			else
				name_array="$1"
			fi
			if ! parse_barcode_parameter "$2"; then
				if [[ -f "$2" ]]; then
					if [[ -r "$2" && -s "$2" ]]; then
						mapfile -t barcode_array <"$2"
					fi
				fi
			fi
			department="$3"
			flags="$4"
			price="$5"
		fi

		if ((${#name_array[*]} <= 1)) && ! check_name "${name_array[0]}" && ! check_department "$department" && [[ -z "$flags" ]] && ! check_price "$price"; then
			for ((i = 0; i < ${#barcode_array[*]}; i++)); do
				barcode="${barcode_array[j]}"
				null_properties
				select_properties
				print_properties
				null_properties
			done
		else
			if [[ "$flags" =~ [ЛлLl] ]]; then
				measure=779
			elif [[ "$flags" =~ [ВвBb] ]]; then
				measure=2
			else
				measure=1
			fi

			if [[ "$flags" =~ [ОоOo] ]]; then
				op_mode=64
			else
				op_mode=0
			fi

			if [[ "$flags" =~ [МмMm] ]]; then
				tmctype=7
			else
				tmctype=0
			fi

			if [[ "$flags" =~ [ЕеEe] ]]; then
				tmctype=1
				op_mode=192
				measure=1
			elif [[ "$flags" =~ [ТтTt] ]]; then
				tmctype=3
				op_mode=32768
				measure=1
			elif [[ "$flags" =~ [РрPp] ]]; then
				tmctype=7
				op_mode=64
				measure=779
				quant_mode=46
			fi

			for ((i = 0, j = 0; i < ${#name_array[*]} || j < ${#barcode_array[*]}; i++, j++)); do
				if ((i >= ${#name_array[*]} && i > 0)); then
					name_array[i]="${name_array[i - 1]}"
				fi
				if ((j >= ${#barcode_array[*]} && j > 0)); then
					barcode_array[j]="$((${barcode_array[j - 1]} + 1))"
				fi
				barcode="${barcode_array[j]}"
				name="${name_array[i]}"
				good
			done
		fi
	fi
else
	if [[ -f "$CHECK_IMG" && -r "$CHECK_IMG" && -s "$CHECK_IMG" ]]; then
		echo -ne '\e[36m'
		GOODS_ARRAY="$(mktemp)"
		jq '.goodsItems | values' "$CHECK_IMG" >"$GOODS_ARRAY"
		GOODS_ARRAY_LENGTH="$(jq -r 'length | values' "$GOODS_ARRAY")"
		echo "Штрихкоды из текущего чека ($GOODS_ARRAY_LENGTH):"
		goods_position="$(mktemp)"
		for ((i = 0; i < $GOODS_ARRAY_LENGTH; i++)); do
			jq ".[$i] | values" "$GOODS_ARRAY" >"$goods_position"

			barcode="$(jq -r ".bcode | values" "$goods_position")"
			null_properties
			department="$(jq -r ".dept | values" "$goods_position")"
			measure="$(jq -r ".measure | values" "$goods_position")"
			#quantity="$(jq -r ".bquant | values" "$goods_position")"
			price="$(jq -r ".price | values" "$goods_position")"
			name="$(jq -r ".name | values" "$goods_position")"
			select_properties
			print_properties
			null_properties
		done
		echo
	fi
	find_barcodes
	if [[ -s "$barcodes" ]]; then
		echo -ne '\e[36m'
		echo "Штрихкоды ($(wc -l "$barcodes" | cut -f1 -d' ')):"
		for barcode in $(<"$barcodes"); do
			null_properties
			select_properties
			print_properties
			null_properties
		done
		echo
	fi
fi

check_bananas

echo -ne '\e[0m'

tabs -8
