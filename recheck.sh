#!/bin/bash

source "${BASH_SOURCE%/*}/common_input_funcs.sh"

mark="$(mktemp)"

function exit_if_needed() {
	if ((stop_on_failure)); then
		red_print "Остановка ввода, остановка скрипта"
		exit 1
	fi
}

function enter() {
	if [[ -f "$mark_argument" && -r "$mark_argument" && -s "$mark_argument" ]]; then
		excise_type=
		cat "$mark_argument" >"$mark"
	elif [[ "$mark_argument" ]]; then
		excise_type=
		echo -n "$mark_argument" >"$mark"
	fi

	if [[ -s "$mark" ]] && grep -Pw '\w{150}' "$mark" &>/dev/null; then
		quantity=1
		excise_type="ALCOHOL"
	fi

	if ((check_mark)) && [[ "$excise_type" == "ALCOHOL" && "$(curl -s "$UTM1_URL/api/mark/check?code=$(<"$mark")" | jq '.owner | booleans')" == "false" ]]; then
		log_error_details "Акцизная марка не прошла проверку в УТМ $(<"$mark")"
		exit_if_needed
		return 1
	fi

	if [[ "$barcode_argument" ]]; then
		barcode="$barcode_argument"
	fi

	if [[ "$department_argument" ]]; then
		department="$department_argument"
	fi

	if [[ "$quantity_argument" ]]; then
		quantity="$quantity_argument"
	fi
	quantity="$(roundp "$quantity" 3)"

	if [[ "$price_argument" == "-" ]]; then
		price=
	elif [[ "$price_argument" && "$price_argument" != "=" ]]; then
		price="$price_argument"
	else
		price="$(roundp "$price")"
	fi
	if [[ "$price" && "$price_mul" ]]; then
		price="$(jq -n "$price * $price_mul")"
	fi
	price="$(roundp "$price" 2)"

	sleep "$default_delay"
	escape
	if [[ "$(mysql --skip-column-names -B -e "SELECT * FROM dictionaries.actionparameter WHERE parametervalue = '1' AND cmactioncode = '10' AND parametername = 'mode'")" ]]; then
		input_department_code
		if input_codes; then
			input_price
			input_quantity
		else
			echo "Была получена ошибка при вводе штрихкода/кода/марки"
			exit_if_needed
		fi
	else
		input_price
		input_quantity
		input_department_code
		if ! input_codes; then
			echo "Была получена ошибка при вводе штрихкода/кода/марки"
			exit_if_needed
		fi
	fi

	return 0
}

function display_help_message() {
	echo -ne '\e[36m'
	echo "Использование:"
	echo "./recheck.sh [опции] [файл]"
	echo "Опции:"
	echo "  -d, --dept, --department номер"
	echo "      Указать номер отдела"
	echo "  -p, --price цена"
	echo "      Указать цену:"
	echo "      - Если указан дефис (-), ввод цены будет пропущен"
	echo "      - Можно указать дробное или целое число"
	echo "      По умолчанию используются цены из файла"
	echo "  --price-mul множитель"
	echo "      Умножать цену на указанный множитель перед вводом"
	echo "  --quantity количество"
	echo "      Указать количество"
	echo "  --email адрес"
	echo "      Указать адрес электронной почты"
	echo "  --skip"
	echo "      Пропускать ввод позиции при возникновении ошибки"
	echo "  --check-mark"
	echo "      Проверять каждую акцизную марку в УТМ"
	echo "  --stop-on-failure"
	echo "      Останавливать ввод позиции при возникновении ошибки"
	echo "  -h, --help"
	echo "      Показать это сообщение"
	echo "  --barcode штрихкод"
	echo "      Указать штрихкод"
	echo "  --mark марка"
	echo "      Указать код маркировки или акцизную марку, или файл с ней"
	echo -e '\e[0m'
}

if ! TEMP=$(getopt --name 'recheck.sh' --longoptions 'barcode:,mark:,dept:,department:,price:,price-mul:,quantity:,email:,skip,check-mark,help,stop-on-failure' --options 'd:,p:,h' -- "$@"); then
	display_help_message
	exit 1
fi
eval set -- "$TEMP"
unset TEMP

while true; do
	case "$1" in
	'--barcode')
		barcode_argument="$2"
		shift 2
		;;
	'--mark')
		mark_argument="$2"
		shift 2
		;;
	'-d' | '--dept' | '--department')
		department_argument="$2"
		shift 2
		;;
	'-p' | '--price')
		price_argument="$(roundp "$2")"
		shift 2
		;;
	'--price-mul')
		price_mul="$(roundp "$2")"
		shift 2
		;;
	'--quantity')
		quantity_argument="$(roundp "$2")"
		shift 2
		;;
	'--email')
		email_argument="$2"
		shift 2
		;;
	'--skip')
		skip=1
		shift
		;;
	'--check-mark')
		check_mark=1
		shift
		;;
	'--stop-on-failure')
		stop_on_failure=1
		shift
		;;
	'-h' | '--help')
		display_help_message
		exit 0
		;;
	'--')
		shift
		break
		;;
	*)
		red_print "Неизвестный параметр '$1' проигнорирован"
		shift
		;;
	esac
done

for arg; do
	if [[ "$arg" && "$arg" != "--" && -f "$arg" && -r "$arg" && -s "$arg" ]]; then
		FILE="$arg"
		break
	fi
done

if [[ -z "$FILE" ]]; then
	enter
elif file -b "$FILE" | grep -Fi "XML" &>/dev/null; then
	waybill_optimized="$(mktemp)"

	parse_waybill "$FILE" "$waybill_optimized"
	parse_waybill_positions "$waybill_optimized"

	for ((k = 1; k <= WAYBILL_NUM_POSITIONS; k++)); do
		for ((j = 1; j <= WAYBILL_POSITION_NUM_BOXES[k]; j++)); do
			source <(xmlstarlet sel -t \
				-o "WAYBILL_POSITION_BOXPOS_NUM_EXCISE_MARKS[j]='" -v 'count(/*/*/WayBill_v4/Content/Position'"[$k]"'/InformF2/MarkInfo/boxpos'"[$j]"'/amclist/amc)' -o "'" -n "$waybill_optimized")
			for ((i = 1; i <= WAYBILL_POSITION_BOXPOS_NUM_EXCISE_MARKS[j]; i++)); do
				barcode="${WAYBILL_POSITION_EAN13[$k]}"
				department=
				excise_type="ALCOHOL"
				measure=
				name="${WAYBILL_POSITION_PRODUCT_FULLNAME[$k]}"
				price="${WAYBILL_POSITION_PRICE[$k]}"
				quantity=1

				source <(xmlstarlet sel -t \
					-o "EXCISE_MARK='" -v '/*/*/WayBill_v4/Content/Position'"[$k]"'/InformF2/MarkInfo/boxpos'"[$j]"'/amclist/amc'"[$i]" -o "'" -n "$waybill_optimized")
				echo -n "$EXCISE_MARK" >"$mark"

				enter
			done
		done
	done
else
	goods_array="$(mktemp)"
	goods_file_type=
	if jq -e '.goodsItems | values' "$FILE" >"$goods_array"; then
		goods_file_type="check"

		if [[ "$(jq -r ".status | values" "$FILE")" == 1 ]]; then
			red_print "Сторнированный(аннулированный) чек"
		fi

		valcodes="$(mktemp)"
		jq -r '.moneyItems | map(.valcode) | values' "$FILE" >"$valcodes"
		for ((i = 0; i < $(jq -r 'length | values' "$valcodes"); i++)); do
			if (($(jq -r ".[$i] | values" "$valcodes") != 1)); then
				echo -ne '\e[91;4m'
				echo -n "Безналичный расчёт!"
				echo -e '\e[0m'
			fi
		done
	elif jq -e '.goods | values' "$FILE" >"$goods_array"; then
		goods_file_type="custom_json"

		email="$(jq -r ".email | values" "$FILE")"
		valut_code="$(jq -r ".valutCode | values" "$FILE")"
	else
		red_print "Не удалось распознать формат файла $GOODS_FILE"
		exit 1
	fi

	goods_position="$(mktemp)"

	GOODS_ARRAY_LENGTH="$(jq -r 'length | values' "$goods_array")"
	for ((i = 0; i < $GOODS_ARRAY_LENGTH; i++)); do
		jq ".[$i] | values" "$goods_array" >"$goods_position"

		if [[ "$goods_file_type" == "check" ]]; then
			if ! barcode="$(jq -e -r ".bcode | values" "$goods_position")"; then
				log_error_details "Ошибка при получении значения ключа .bcode"
				exit_if_needed
			fi
			department=
			excise_type="$(jq -r ".exciseType | values" "$goods_position")"
			measure="$(jq -r ".measure | values" "$goods_position")"
			name="$(jq -r ".name | values" "$goods_position")"
			price="$(jq -r ".price | values" "$goods_position")"
			quantity="$(jq -r ".bquant | values" "$goods_position")"

			if [[ "$excise_type" == "DRAFTBEER" ]]; then
				>"$mark"
			else
				jq -r ".markingCode // .exciseMark | values" "$goods_position" | tr -d '\n' >"$mark"
			fi
		elif [[ "$goods_file_type" == "custom_json" ]]; then
			if ! barcode="$(jq -e -r ".bcode | values" "$goods_position")"; then
				log_error_details "Ошибка при получении значения ключа .bcode"
				exit_if_needed
			fi
			department="$(jq -r ".dcode | values" "$goods_position")"
			excise_type=
			measure=
			name=
			price="$(jq -r ".price | values" "$goods_position")"
			quantity="$(jq -r ".quantity | values" "$goods_position")"

			jq -r ".amark | values" "$goods_position" | tr -d '\n' >"$mark"
		fi

		enter
	done
fi

escape

if [[ "$email_argument" ]]; then
	email="$email_argument"
fi
if input_customer_email; then
	select_currency

	green_print "Ввод данных из файла $FILE завершен"
else
	red_print "Ошибка при вводе адреса электронной почты"
	exit_if_needed
fi

exit 0
