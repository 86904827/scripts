#!/bin/bash

(

	flock -x -w 10 200 || exit 1

	source "${BASH_SOURCE%/*}/common.sh"

	if [[ -d "/linuxcash/logs/data" ]] && [[ "$(find -O3 /linuxcash/logs/data -mindepth 1 -maxdepth 1 -type f -mmin -1440)" ]]; then
		exit 0
	fi
	sleep "$((RANDOM % 3600))s"

	for LIBPKCS11 in "/opt/aktivco/rutokenecp/i386/librtpkcs11ecp.so" "/opt/aktivco/rutokenecp/amd64/librtpkcs11ecp.so" "/usr/lib/librtpkcs11ecp.so" "/opt/utm/lib/librtpkcs11ecp.so" "/root/librtpkcs11ecp.so"; do
		if [[ -f "$LIBPKCS11" && -r "$LIBPKCS11" && -s "$LIBPKCS11" ]]; then
			break
		fi
	done

	DATA_TMP_DIR="$(mktemp -d)"
	cd "$DATA_TMP_DIR"

	biosdecode &>"biosdecode.txt"
	blkid &>"blkid.txt"
	df -h &>"df_h.txt"
	df &>"df.txt"
	dmesg -x &>"dmesg_x.txt"
	dmidecode &>"dmidecode.txt"
	dpkg-query --list &>"dpkg-query_list.txt"
	dumpe2fs -h /dev/sda1 &>"dumpe2fs_h_sda1.txt"
	dumpe2fs -h /dev/sda5 &>"dumpe2fs_h_sda5.txt"
	dumpe2fs -h /dev/sda6 &>"dumpe2fs_h_sda6.txt"
	fdisk -l /dev/sda &>"fdisk_l_sda.txt"
	free -h &>"free_h.txt"
	ifconfig &>"ifconfig.txt"
	ip a &>"ip_a.txt"
	ip l &>"ip_l.txt"
	ip n &>"ip_n.txt"
	ip r &>"ip_r.txt"
	iw dev &>"iw_dev.txt"
	iw phy &>"iw_phy.txt"
	last &>"last.txt"
	lastb &>"lastb.txt"
	last -Fwx &>"last_Fwx.txt"
	lastb -Fwx &>"lastb_Fwx.txt"
	lsblk -f &>"lsblk_f.txt"
	lsblk -t &>"lsblk_t.txt"
	lsblk &>"lsblk.txt"
	lscpu &>"lscpu.txt"
	lsmod &>"lsmod.txt"
	lspci -vvknn &>"lspci_vvknn.txt"
	lspci -vvknnt &>"lspci_vvknnt.txt"
	lspci &>"lspci.txt"
	lsusb -v &>"lsusb_v.txt"
	lsusb -vt &>"lsusb_vt.txt"
	lsusb &>"lsusb.txt"
	lsusb.py -Ciu &>"lsusb_py_Ciu.txt"
	lsusb.py &>"lsusb_py.txt"
	mysqlreport &>"mysqlreport.txt"
	ps -ef &>"ps_ef.txt"
	ps -efHww &>"ps_efHww.txt"
	pstree -alpU &>"pstree_alpU.txt"
	sensors &>"sensors.txt"
	ss -apntu &>"ss_apntu.txt"
	ss -rap &>"ss_rap.txt"
	sysctl -a &>"sysctl_a.txt"
	timedatectl &>"timedatectl.txt"
	uname -a &>"uname_a.txt"
	uptime -p &>"uptime_p.txt"
	uptime &>"uptime.txt"
	usb-devices &>"usb_devices.txt"
	vboxmanage list --long vms &>"vboxmanage_list_long_vms.txt"
	vboxmanage list runningvms &>"vboxmanage_list_runningvms.txt"
	vboxmanage list usbhost &>"vboxmanage_list_usbhost.txt"
	w &>"w.txt"
	who &>"who.txt"
	xrandr &>"xrandr.txt"

	smartctl -s on /dev/sda &>/dev/null
	smartctl --xall /dev/sda &>"smartctl_xall.txt"

	#lsof +c 0 -n -P -c '^mysqld' -c '^java' -c '^qpidd' | grep -ahFv -e '/dev/zero' -e '/opt/utm' &>"lsof.txt"
	#lsof +c 0 -n -P -F n | grep -ahPv '^p' | cut -c 2- | sort -u &>lsof.txt

	/linuxcash/cash/bin/InfoClient &>"InfoClient.txt"

	mkdir "PKCS11"
	pkcs11-tool --module "$LIBPKCS11" --list-slots &>"PKCS11/slots.txt"
	pkcs11-tool --module "$LIBPKCS11" --list-token-slots &>"PKCS11/token_slots.txt"
	for slot_index in 0 1 2; do
		mkdir "PKCS11/$slot_index"
		pkcs11-tool --module "$LIBPKCS11" --list-mechanisms &>"PKCS11/$slot_index/mechanisms.txt"
		pkcs11-tool --module "$LIBPKCS11" --slot-index "$slot_index" --list-objects &>"PKCS11/$slot_index/objects.txt"
		for id in $(grep -ahoP '^\h*ID:\h+\K\w+' "PKCS11/$slot_index/objects.txt" | sort -rg | uniq); do
			pkcs11-tool --module "$LIBPKCS11" --slot-index "$slot_index" --read-object --type cert --id "$id" 2>/dev/null | openssl x509 -inform DER -text -nameopt utf8,sep_multiline,lname &>"PKCS11/$slot_index/cert_$id.txt"
		done
		mkdir "PKCS11/$slot_index/12345678"
		pkcs11-tool --module "$LIBPKCS11" --slot-index "$slot_index" --login --pin "12345678" --list-objects &>"PKCS11/$slot_index/12345678/objects.txt"
		for id in $(grep -ahoP '^\h*ID:\h+\K\w+' "PKCS11/$slot_index/12345678/objects.txt" | sort -rg | uniq); do
			pkcs11-tool --module "$LIBPKCS11" --slot-index "$slot_index" --login --pin "12345678" --read-object --type cert --id "$id" 2>/dev/null | openssl x509 -inform DER -text -nameopt utf8,sep_multiline,lname &>"PKCS11/$slot_index/12345678/cert_$id.txt"
		done
	done

	/root/rtecpinfo.sh &>"rtecpinfo.txt"
	/root/kkminfo.sh &>"kkminfo.txt"
	/root/ttnstatus.sh &>"ttnstatus.txt"
	/root/goods.sh &>"goods.txt"

	mkdir UTM1
	curl -s "$UTM1_URL/diagnosis" &>"UTM1/diagnosis.txt"
	curl -s "$UTM1_URL/api/rsa/orginfo" &>"UTM1/api_rsa_orginfo.txt"
	curl -s "$UTM1_URL/api/gost/orginfo" &>"UTM1/api_gost_orginfo.txt"
	curl -s "$UTM1_URL/api/rsa" &>"UTM1/api_rsa.txt"
	curl -s "$UTM1_URL/api/info/list" &>"UTM1/api_info_list.txt"
	curl -s "$UTM1_URL/api/info/srvtime" &>"UTM1/api_info_srvtime.txt"
	curl -s "$UTM1_URL/api/info/version" &>"UTM1/api_info_version.txt"
	curl -s "$UTM1_URL/api/services/orgs" &>"UTM1/api_services_orgs.txt"
	curl -s "$UTM1_URL/opt/out" &>"UTM1/opt_out.txt"
	curl -s "$UTM1_URL/opt/in" &>"UTM1/opt_in.txt"

	mkdir UTM2
	curl -s "$UTM2_URL/diagnosis" &>"UTM2/diagnosis.txt"
	curl -s "$UTM2_URL/api/rsa/orginfo" &>"UTM2/api_rsa_orginfo.txt"
	curl -s "$UTM2_URL/api/gost/orginfo" &>"UTM2/api_gost_orginfo.txt"
	curl -s "$UTM2_URL/api/rsa" &>"UTM2/api_rsa.txt"
	curl -s "$UTM2_URL/api/info/list" &>"UTM2/api_info_list.txt"
	curl -s "$UTM2_URL/api/info/srvtime" &>"UTM2/api_info_srvtime.txt"
	curl -s "$UTM2_URL/api/info/version" &>"UTM2/api_info_version.txt"
	curl -s "$UTM2_URL/api/services/orgs" &>"UTM2/api_services_orgs.txt"
	curl -s "$UTM2_URL/opt/out" &>"UTM2/opt_out.txt"
	curl -s "$UTM2_URL/opt/in" &>"UTM2/opt_in.txt"

	curl -s "2ip.ru" &>"2ip_ru.txt"
	curl -s "ip.me" &>"ip_me.txt"

	ls -AlR /root/ttnload &>"ttnload.txt"
	ls -AlR /root/ttnload2 &>"ttnload2.txt"

	mkdir root
	cp --parents --preserve --recursive -H --target-directory=root /proc/meminfo /proc/cpuinfo /proc/cmdline /proc/mounts /proc/bus/input/devices /proc/scsi/scsi /proc/locks \
		/etc/default/grub /etc/network/interfaces /etc/wpa_supplicant.conf /etc/hosts /etc/resolv.conf /etc/puppet/puppet.conf /etc/passwd /etc/shadow /etc/group /etc/os-release /etc/rc.local \
		/linuxcash/cash/conf/bcode.ini.d/bcode.ini /linuxcash/cash/conf/frinit.conf /linuxcash/cash/data/cash.reg /linuxcash/cash/conf/ncash.ini /linuxcash/cash/conf/ncash.ini.d/ncash_hwfr* /linuxcash/cash/conf/ncash.ini.d/egaisttn.ini /linuxcash/cash/conf/ncash.ini.d/markedgoods.ini /linuxcash/cash/conf/ncash.ini.d/pay1.ini /linuxcash/cash/conf/ncash.ini.d/pay2.ini /linuxcash/cash/conf/ncash.ini.d/tax* /linuxcash/cash/conf/ncash.ini.d/white.ini /linuxcash/cash/conf/plugins/egais.xml /linuxcash/cash/conf/drivers \
		/opt/comproxy/ComProxy.ini \
		$(find -O3 "/opt/utm/transport/xml/" -type f -mtime -1 -size -50k) \
		$(find -O3 "/opt/utm2/transport/xml/" -type f -mtime -1 -size -50k)

	for f in /linuxcash/logs/current/trace.txt /linuxcash/logs/current/terminal.log /linuxcash/logs/current/frdriver.log /var/log/syslog /var/log/kern.log /var/log/Xorg.255.log /var/log/dmesg /var/log/dpkg.log /var/log/apt/history.log /var/log/apt/term.log /var/log/aptitude /var/log/mysql/error.log /var/log/mysql.log /var/log/mysql.err /var/log/utm-trans.out.log /opt/utm/transport/l/transport_transaction.log /opt/utm/transport/l/transport_info.log /opt/utm2/transport/l/transport_transaction.log /opt/utm2/transport/l/transport_info.log /root/.bash_history /root/.mysql_history; do
		mkdir --parents "root/$(dirname "$f")"
		tail --bytes=300K "$f" &>"root/$f"
	done

	mkdir /linuxcash/logs/data &>/dev/null
	XZ_OPT='-7e' tar cvvfJ "/linuxcash/logs/data/data$(date '+%s').tar.xz" --remove-files *
	mkdir "/linuxcash/net/server/server/data/$(hostname)" &>/dev/null
	rsync --quiet --size-only --perms --chmod=444 --inplace --partial --times $(find "/linuxcash/logs/data" -mindepth 1 -maxdepth 1 -type f -mtime -7 -regextype posix-egrep -regex ".*/data[[:digit:]]+\.tar\.xz" | sort -g | tail -n 3) "/linuxcash/net/server/server/data/$(hostname)/"

) 200>/var/lock/.data_collector.exclusivelock
