#!/bin/bash

source "${BASH_SOURCE%/*}/common.sh"

T1=0.5
T2=2
T3=0.5
T4=0.5
T5=10
T6=0.5
T7=0.5

N=10
N1=100
BMAX=256

RECEIVED_DATA_FILE="$TMPDIR/RECEIVED_DATA_FILE"
TMP_SOCAT_LOG="$TMPDIR/TMP_SOCAT_LOG"
GLOBAL_LOGFILE="$TMPDIR/GLOBAL_LOGFILE"
COMMAND_RESPONSE_FILE="$TMPDIR/COMMAND_RESPONSE_FILE"
DATASET_FILE="$TMPDIR/DATASET_FILE"
FRAME_FILE="$TMPDIR/FRAME_FILE"
CURRENT_BYTE_FILE="$TMPDIR/CURRENT_BYTE_FILE"
PROPERTIES_FILE="$TMPDIR/PROPERTIES_FILE"
KKT_JSON_FILE="$TMPDIR/KKT_JSON_FILE"

STX=$'\x02'
EOT=$'\x04'
ETX=$'\x03'
ENQ=$'\x05'
ACK=$'\x06'
DLE=$'\x10'
NAK=$'\x15'

STX_HEX='02'
EOT_HEX='04'
ETX_HEX='03'
ENQ_HEX='05'
ACK_HEX='06'
DLE_HEX='10'
NAK_HEX='15'

BAUDRATE="57600"
PASSWORD="0000"

ATOL2_FOLDER="/linuxcash/logs/current/atol2"
mkdir "$ATOL2_FOLDER" &>/dev/null

exec > >(tee -a "$GLOBAL_LOGFILE")

cleanup_on_exit() {
    local exit_status="$?"
    kill "$SCRIPT_TIMEOUT_PID" &>/dev/null
    if [[ "$SOCAT_LOGFILE" ]]; then
        cp "$TMP_SOCAT_LOG" "$SOCAT_LOGFILE"
    fi
    if [[ "$LOG_OUTPUT_FILE" && "$LOG_LAST_OUTPUT_FILE" ]]; then
        cp "$GLOBAL_LOGFILE" "$LOG_OUTPUT_FILE"
        cp "$GLOBAL_LOGFILE" "$LOG_LAST_OUTPUT_FILE"
    fi
    if [[ "$JSON_OUTPUT_FILE" && "$JSON_LAST_OUTPUT_FILE" ]]; then
        cp "$KKT_JSON_FILE" "$JSON_OUTPUT_FILE"
        cp "$KKT_JSON_FILE" "$JSON_LAST_OUTPUT_FILE"

        if [[ "$PFFRR_INN_STR" ]]; then
            local target_dir="/linuxcash/net/server/server/kkt/atol2/$(hostname)"

            mkdir "$target_dir" &>/dev/null
            if [[ -d "$target_dir" ]]; then
                if [[ "${#PFFRR_INN_STR}" == 12 ]]; then
                    cp "$JSON_OUTPUT_FILE" "/linuxcash/logs/current/kkt_client_fr2.json"
                    rsync --timeout 5 -hhhiiv --stats "$JSON_OUTPUT_FILE" "$target_dir/kkt_client_fr2.json" >>"$LOG_OUTPUT_FILE" 2>&1
                elif [[ "${#PFFRR_INN_STR}" == 10 ]]; then
                    cp "$JSON_OUTPUT_FILE" "/linuxcash/logs/current/kkt_client_fr1.json"
                    rsync --timeout 5 -hhhiiv --stats "$JSON_OUTPUT_FILE" "$target_dir/kkt_client_fr1.json" >>"$LOG_OUTPUT_FILE" 2>&1
                fi
            fi
        fi
    fi
    rm -rf -- "$TMPDIR"
}
trap cleanup_on_exit EXIT

SCRIPT_TIMEOUT_SECONDS=30

timeout_kill() {
    sleep "$SCRIPT_TIMEOUT_SECONDS"
    echo "$SCRIPT_TIMEOUT_SECONDS секунд истекло. Терминирование скрипта."
    kill "$$" &>/dev/null
}

timeout_kill &
disown
SCRIPT_TIMEOUT_PID="$!"

display_usage() {
    cat <<EOF
Опции:
  --help                Показать эту справку и выйти.
  --device <path>       Устройство порта.
  --baudrate <rate>     Скорость порта (по умолчанию: $BAUDRATE).
  --password <pwd>      Пароль (по умолчанию: $PASSWORD).
EOF
    exit 1
}

check_dependencies() {
    local dependencies=(socat xxd timeout iconv stdbuf hexdump jq sponge)
    for tool in "${dependencies[@]}"; do
        if ! command -v "$tool" &>/dev/null; then
            echo -e "Ошибка: '$tool' не установлен!"
            exit 1
        fi
    done
}

parse_command_line_arguments() {
    if ! TEMP="$(getopt \
        --name "$0" \
        --longoptions help,device:,baudrate:,password: \
        --options '' \
        -- "$@")"; then
        display_usage
    fi
    eval set -- "$TEMP"

    while true; do
        case "$1" in
        --help) display_usage ;;
        --device)
            TTY_DEVICE="$2"
            shift 2
            ;;
        --baudrate)
            BAUDRATE="$2"
            shift 2
            ;;
        --password)
            PASSWORD="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Неизвестная опция: $1"
            display_usage
            ;;
        esac
    done

    COMMAND_PAYLOAD="$1"
}

declare -a ERROR_CODES=(
    [0]="Ошибок нет"
    [8]="Неверная цена (сумма)"
    [10]="Неверное количество"
    "Переполнение счетчика наличности"
    "Невозможно сторно последней операции"
    "Сторно по коду невозможно (в чеке зарегистрировано меньшее количество товаров с указанным кодом)"
    "Невозможен повтор последней операции"
    "Повторная скидка на операцию невозможна"
    "Скидка/надбавка на предыдущую операцию невозможна"
    "Неверный код товара"
    "Неверный штрихкод товара"
    "Неверный формат"
    "Неверная длина"
    "ККТ заблокирована в режиме ввода даты"
    "Требуется подтверждение ввода даты"
    [24]="Нет больше данных для передачи ПО ККТ"
    "Нет подтверждения или отмены регистрации прихода"
    "Отчет с гашением прерван. Вход в режим невозможен."
    "Отключение контроля наличности невозможно (не настроены необходимые способы расчета)"
    [30]="Вход в режим заблокирован"
    "Проверьте дату и время"
    "Дата и время в ККТ меньше чем в ФН"
    "Невозможно закрыть архив"
    [61]="Товар не найден"
    "Весовой штрихкод с количеством ≠1.000"
    "Переполнение буфера чека"
    "Недостаточное количество товара"
    "Сторнируемое количество больше проданного"
    "Заблокированный товар не найден в буфере чека"
    "Данный товар не продавался в чеке, сторно невозможно"
    [70]="Неверная команда от ККТ"
    [102]="Команда не реализуется в данном режиме ККТ"
    "Нет бумаги"
    "Нет связи с принтером чеков"
    "Механическая ошибка печатающего устройства"
    "Неверный тип чека"
    "Нет больше строк картинки/штрихкода"
    "Неверный номер регистра"
    "Недопустимое целевое устройство"
    "Нет места в массиве картинок/штрихкодов"
    "Неверный номер картинки/штрихкода (картинка/штрихкод отсутствует)"
    "Сумма сторно больше, чем было получено данным способом расчета"
    "Сумма не наличных платежей превышает сумму чека"
    "Сумма платежей меньше суммы чека"
    "Накопление меньше суммы возврата или аннулирования"
    [117]="Переполнение суммы платежей"
    "Предыдущая операция не завершена"
    "Ошибка GSM-модуля"
    [122]="Данная модель ККТ не может выполнить команду"
    "Неверная величина скидки / надбавки"
    "Операция после скидки / надбавки невозможна"
    "Неверная секция"
    "Неверная форма/способ расчета"
    "Переполнение при умножении"
    "Операция запрещена в таблице настроек"
    "Переполнение итога чека"
    "Открыт чек аннулирования – операция невозможна"
    [132]="Переполнение буфера контрольной ленты"
    [134]="Вносимая клиентом сумма меньше суммы чека"
    "Открыт чек возврата – операция невозможна"
    "Смена превысила 24 часа"
    "Открыт чек прихода – операция невозможна"
    "Переполнение ФП"
    [140]="Неверный пароль"
    "Буфер контрольной ленты не переполнен"
    "Идет обработка контрольной ленты"
    "Обнуленная касса (повторное гашение невозможно)"
    [145]="Неверный номер таблицы"
    "Неверный номер ряда"
    "Неверный номер поля"
    "Неверная дата"
    "Неверное время"
    "Сумма чека по секции меньше суммы сторно"
    "Подсчет суммы сдачи невозможен"
    "В ККТ нет денег для выплаты"
    [154]="Чек закрыт – операция невозможна"
    "Чек открыт – операция невозможна"
    "Смена открыта, операция невозможна"
    [158]="Заводской номер/MAC-адрес уже задан"
    "Исчерпан лимит перерегистраций"
    [162]="Неверный номер смены"
    "Неверный тип отчета"
    "Недопустимый пароль"
    "Недопустимый заводской номер ККТ"
    "Недопустимый РНМ"
    "Недопустимый ИНН"
    "ККТ не фискализирована"
    "Не задан заводской номер"
    "Нет отчетов"
    "Режим не активизирован"
    "Нет указанного чека в ЭЖ"
    "Нет больше записей в ЭЖ"
    "Некорректный код /лицензия или номер"
    "Отсутствуют данные в буфере ККТ"
    "Требуется выполнение общего гашения"
    "Команда не разрешена введенными кодами защиты /лицензиями ККТ"
    "Невозможно отмена скидки/надбавки"
    "Невозможно закрыть чек данным способом расчета (в чеке присутствуют операции без контроля наличных)"
    "Неверный номер маршрута"
    "Неверный номер начальной зоны"
    "Неверный номер конечной зоны"
    "Неверный тип тарифа"
    "Неверный тариф"
    [186]="Ошибка обмена с фискальным модулем"
    [190]="Необходимо провести профилактические работы"
    "Неверные номера смен в ККТ и ФН"
    [200]="Нет устройства, обрабатывающего данную команду"
    "Нет связи с внешним устройством"
    "Ошибочное состояние ТРК"
    "Больше одной регистрации в чеке"
    "Ошибочный номер ТРК"
    "Неверный делитель"
    [208]="Активизация данного ФН в составе данной ККТ невозможна"
    "Перегрев головки принтера"
    "Ошибка обмена с ФН на уровне интерфейса I2C"
    "Ошибка формата передачи ФН"
    "Неверное состояние ФН"
    "Неисправимая ошибка ФН"
    "Ошибка КС ФН"
    "Закончен срок эксплуатации ФН"
    "Архив ФН переполнен"
    "В ФН переданы неверная дата или время"
    "В ФН нет запрошенных данных"
    "Переполнение ФН (итог чека)"
    "Буфер переполнен"
    "Невозможно напечатать вторую фискальную копию"
    [223]="Сумма налога больше суммы регистраций по чеку и/или итога или больше суммы регистрации"
    "Начисление налога на последнюю операцию невозможно"
    "Неверный номер ФН"
    [228]="Сумма сторно налога больше суммы зарегистрированного налога данного типа"
    [230]="Операция невозможна, недостаточно питания"
    "Некорректное значение параметров команды ФН"
    "Превышение размеров TLV данных ФН"
    "Нет транспортного соединения ФН"
    "Исчерпан ресурс КС ФН"
    "Исчерпан ресурс хранения ФД для отправки ОФД"
    "Сообщение от ОФД не может быть принято ФН"
    "В ФН есть неотправленные ФД"
    "Запросить расширенный код ошибки в регистре 55"
    "Исчерпан ресурс Ожидания передачи сообщения в ФН"
    "Продолжительность смены ФН более 24 часов"
    "Неверная разница во времени между двумя операциями ФН"
    "Неверная команда"
    "Количество позиций, подлежащих учету в документе, превысило разрешенный лимит"
    "Отсутствуют данные в команде"
    "Продажа подакцизного товара"
    "Превышение максимального размера чека"
)

declare -A KKT_STATES=(
    ["0.0"]="Выбор"

    ["1.0"]="Ожидание команды (регистрация)"
    ["1.1"]="Ввод пароля (регистрация)"
    ["1.2"]="Ожидание ввода секции"
    ["1.3"]="Ожидание сторно по штрихкоду"
    ["1.4"]="Прием платежей"
    ["1.5"]="Ожидание печати отложенного документа"
    ["1.6"]="Печать отложенного документа"
    ["1.7"]="Формирование документа"

    ["2.0"]="Ожидание команды (Х-отчеты)"
    ["2.1"]="Ввод пароля (Х-отчеты)"
    ["2.2"]="Формирование Х-отчета (или предварительная обработка контрольной ленты)"
    ["2.3"]="Формирование служебного отчета"

    ["3.0"]="Ожидание команды (Z-отчеты)"
    ["3.1"]="Ввод пароля (Z-отчеты)"
    ["3.2"]="Формирование Z-отчета или контрольной ленты"
    ["3.3"]="Подтверждение гашения"
    ["3.4"]="Ввод даты с клавиатуры"
    ["3.5"]="Ожидание подтверждения общего гашения"
    ["3.6"]="Идет общее гашение"

    ["4.0"]="Ожидание команды (программирование)"
    ["4.1"]="Ввод пароля (программирование)"

    ["5.0"]="Ожидание команды (ввод ЗН)"
    ["5.1"]="Ввод пароля (ввод ЗН)"
    ["5.3"]="Ввод данных (ввод ЗН)"
    ["5.4"]="Подтверждение ввода (ввод ЗН)"

    ["6.0"]="Ожидание команды (доступ к ФН)"
    ["6.2"]="Формирование отчета (доступ к ФН)"

    ["7.1"]="Идет обнуление таблиц и гашение операционных регистров"
    ["7.2"]="Выполняется тестовый прогон"
    ["7.3"]="Режим ввода времени с клавиатуры"
    ["7.4"]="Режим тестов (для технологической ККТ)"
    ["7.5"]="Ввод даты после сбоя часов"
    ["7.6"]="Ввод времени после сбоя часов"
    ["7.7"]="Начальная инициализация ККТ"
    ["7.8"]="Ожидание подтверждения обнуления таблиц"
    ["7.9"]="Разные накопители памяти"
    ["7.10"]="ККТ не инициализирована"
    ["7.11"]="ККТ заблокирована при вводе даты, меньшей даты последней записи ФН"
    ["7.12"]="Подтверждение ввода даты"
    ["7.13"]="Оповещение о переводе часов на летнее/зимнее время"
    ["7.14"]="Блокировка при ошибке ФН"
    ["7.15"]="Блокировка при аппаратных ошибках"
)

declare -a ATOL_MODELS=(
    [57]="АТОЛ 25Ф"
    [61]="АТОЛ 30Ф"
    [62]="АТОЛ 55Ф"
    [63]="АТОЛ FPrint-22ПТК"
    [64]="АТОЛ 52Ф"
    [67]="АТОЛ 11Ф"
    [69]="АТОЛ 77Ф"
    [72]="АТОЛ 90Ф"
    [77]="АТОЛ 42ФС"
    [78]="АТОЛ 15Ф"
    [80]="АТОЛ 50Ф"
    [81]="АТОЛ 20Ф"
    [82]="АТОЛ 91Ф"
    [84]="АТОЛ 92Ф"
)

declare -a FN_LIFE_PHASES=(
    [0]="настройка ФН"
    [1]="готовность к активизации"
    [3]="фискальный режим ФН"
    [7]="постфискальный режим"
    [15]="доступ к архиву ФН"
)

declare -a CURRENT_DOCUMENTS=(
    [0]="нет открытого документа"
    [1]="отчёт о регистрации ККТ"
    [2]="отчёт об открытии смены"
    [4]="кассовый чек"
    [8]="отчёт о закрытии смены"
    [16]="отчёт о закрытии фискального режима"
    [18]="отчет об изменении параметров регистрации ККТ в связи с заменой ФН"
    [19]="отчет об изменении параметров регистрации ККТ"
    [20]="кассовый чек коррекции"
    [23]="отчет о текущем состоянии расчетов"
)

declare -a PORT_TYPES=(
    [1]="RS-232"
    [4]="USB"
    [5]="Bluetooth"
    [6]="Ethernet"
    [7]="WiFi"
)

declare -a CHECK_STATES=(
    [0]="закрыт"
    [1]="приход"
    [2]="возврат прихода"
    [4]="расход"
    [5]="возврат расхода"
)

declare -a DEVICE_TYPES=(
    "Тип не определен"
    "ККТ"
    "Весы"
    "Блок Memo Plus™"
    "Принтер этикеток"
    "Терминал сбора данных"
    "Дисплей покупателя"
    "Сканер штрихкода, PIN-клавиатура, ресторанная клавиатура"
)

declare -a FFD_VERSIONS=(
    [2]="ФФД 1.05"
    "ФФД 1.1"
)

init_serial() {
    coproc SERIAL {
        stdbuf -o0 socat -d -d -v -x \
            "OPEN:${TTY_DEVICE},raw,echo=0,cs8,b${BAUDRATE},parenb=0,cstopb=1,ignoreeof" \
            STDIO 2>"$TMP_SOCAT_LOG"
    }
}

send_char() {
    echo -n "$1" >&"${SERIAL[1]}"
}

wait_byte() {
    timeout "$1" dd of="$CURRENT_BYTE_FILE" status=none bs=1 count=1 <&"${SERIAL[0]}"
    xxd -p -u "$CURRENT_BYTE_FILE"
}

wait_t7() {
    sleep "$T7"
}

compute_crc_file() {
    printf "%02X" "$(($(hexdump -v -e '1/1 "%u ^ "' "$1")3))"
}

build_frame() {
    sed 's/\x10/\x10\x10/g; s/\x03/\x10\x03/g' "$1" >"$2"

    {
        echo -n "$STX"
        cat "$2"
        echo -n "$ETX"
        compute_crc_file "$2" | xxd -r -p
    } | sponge "$2"
}

active_sender() {
    local frc=0

    while ((frc < N1)); do
        ((frc++))

        local rc=0
        local connected=0
        while ((rc < 5)); do
            ((rc++))
            send_char "$ENQ"

            local b
            b="$(wait_byte "$T1")"
            case "$b" in
            "")
                echo "   [warn] Нет ответа (таймаут T1=$T1). Повтор RC=$rc."
                continue
                ;;
            "$ACK_HEX")
                connected=1
                break
                ;;
            "$NAK_HEX")
                echo "   [warn] Получен NAK → повторим ENQ."
                ;;
            "$ENQ_HEX")
                echo "   [info] Коллизия: другая сторона шлёт ENQ. Сброс rc=0, wait_t7..."
                rc=0
                wait_t7
                ;;
            *)
                echo "   [warn] Неожиданный байт 0x$b → подождём T7 и повторим."
                wait_t7
                ;;
            esac
        done

        if ((connected == 0)); then
            echo "   [error] Не получили ACK за 5 попыток. Переходим к следующей глоб. попытке."
            continue
        fi

        echo -n "$PASSWORD$COMMAND_PAYLOAD" | xxd -r -p >"$DATASET_FILE"
        build_frame "$DATASET_FILE" "$FRAME_FILE"

        local dataRC=0
        while ((dataRC < N)); do
            ((dataRC++))
            cat "$FRAME_FILE" >&"${SERIAL[1]}"

            local resp
            resp="$(wait_byte "$T3")"
            case "$resp" in
            "")
                echo "   [error] Нет ответа (таймаут T3=$T3)."
                continue
                ;;
            "$ACK_HEX")
                send_char "$EOT"
                return 0
                ;;
            "$NAK_HEX")
                echo "   [warn] Кадр отвергнут (NAK). Повтор передачи кадра."
                send_char "$EOT"
                continue
                ;;
            "$ENQ_HEX")
                if ((dataRC == 1)); then
                    echo "   [info] Получен ENQ при dataRC=1, ждём T3 и попробуем снова."
                    sleep "$T3"
                    continue
                else
                    echo "   [warn] Получен ENQ при dataRC>$dataRC. Шлём EOT и завершаем."
                    send_char "$EOT"
                    break
                fi
                ;;
            *)
                echo "   [warn] Неизвестный байт. Шлём EOT и повтор."
                send_char "$EOT"
                continue
                ;;
            esac
        done

        echo "   [error] Превышено число попыток передачи кадра dataRC=$N."
    done

    echo ">> [error] Превышен лимит FRC=$N1. Нет связи."
    return 1
}

active_receiver() {
    local rc=0
    while ((rc < N1)); do
        ((rc++))
        local b
        b="$(wait_byte "$T5")"
        case "$b" in
        "")
            echo "   [info] Нет байта за T5=$T5 (rc=$rc). Повтор."
            continue
            ;;
        "$ENQ_HEX")
            send_char "$ACK"

            local stx
            stx="$(wait_byte "$T2")"
            if [[ "$stx" != "$STX_HEX" ]]; then
                echo "   [error] Ожидали STX, получили 0x$stx."
                continue
            fi

            : >"$RECEIVED_DATA_FILE"
            RD_HEX=
            local bc=0
            local dle_flag=0

            while true; do
                local x
                x="$(wait_byte "$T6")"
                if [[ -z "$x" ]]; then
                    echo "   [warn] Таймаут (T6=$T6) при приёме данных. Шлём NAK."
                    send_char "$NAK"
                    break
                fi

                if [[ "$x" == "$ETX_HEX" ]] && ((!dle_flag)); then
                    local crc_byte
                    crc_byte="$(wait_byte "$T6")"
                    if [[ -z "$crc_byte" ]]; then
                        echo "   [error] Не получили CRC, шлём NAK."
                        send_char "$NAK"
                        break
                    fi

                    if [[ "$(compute_crc_file "$RECEIVED_DATA_FILE")" == "$crc_byte" ]]; then
                        send_char "$ACK"

                        local postb
                        postb="$(wait_byte "$T4")"
                        case "$postb" in
                        "$EOT_HEX")
                            return 0
                            ;;
                        "$STX_HEX")
                            echo "   [info] Приходит новый STX (следующий кадр?)."
                            return 0
                            ;;
                        *)
                            echo "   [warn] Не получили EOT/STX, а байт 0x$postb."
                            return 1
                            ;;
                        esac
                    else
                        echo "   [error] CRC не совпала, шлём NAK."
                        send_char "$NAK"
                        break
                    fi
                else
                    if ((dle_flag)); then
                        dle_flag=0
                        RD_HEX+="$x"
                    elif [[ "$x" == "$DLE_HEX" ]]; then
                        dle_flag=1
                    else
                        RD_HEX+="$x"
                    fi
                    cat "$CURRENT_BYTE_FILE" >>"$RECEIVED_DATA_FILE"
                    ((bc++))
                    if ((bc > BMAX)); then
                        echo "   [error] Переполнение буфера (> $BMAX). Шлём NAK."
                        send_char "$NAK"
                        break
                    fi
                fi
            done
            ;;
        *)
            echo "   [info] Получили 0x$b, а ждали ENQ."
            ;;
        esac
    done

    echo ">> [error] Не получили ENQ или не завершили приём корректно за N1=$N1 попыток."
    return 1
}

bit() {
    local number="$1"
    local bit_index="$2"
    echo "$(((number >> bit_index) & 1))"
}

ternary() {
    if [[ $1 == 1 ]]; then
        echo "$2"
    else
        echo "$3"
    fi
}

h2d() {
    local offset="$1"
    local length="${2:-1}"
    echo "$((16#${RD_HEX:offset*2:length*2}))"
}

h2dle() {
    local offset="$1"
    local length="${2:-1}"
    echo "$((16#$(echo "${RD_HEX:offset*2:length*2}" | tac -rs .. | tr -d '\n')))"
}

b2d() {
    local offset="$1"
    local length="${2:-1}"
    echo "$((10#${RD_HEX:offset*2:length*2}))"
}

hre() {
    local offset="$1"
    local length="${2:-1}"
    echo "${RD_HEX:offset*2:length*2}"
}

hree() {
    local offset="$1"
    echo "${RD_HEX:offset*2}"
}

decode_state() {
    local mode_dec="$1"
    local main_mode=$((mode_dec & 0x0F))
    local sub_mode=$(((mode_dec >> 4) & 0x0F))

    local key="${main_mode}.${sub_mode}"
    if [[ -n "${KKT_STATES[$key]}" ]]; then
        echo "${KKT_STATES[$key]} ($key)"
    else
        echo "Неизвестный режим ($key)"
    fi
}

parse_device_model() {
    local model_code="$1"
    local device_type="${2:-1}"
    local model_name

    if ((device_type == 1)); then
        if [[ -n "${ATOL_MODELS[$model_code]}" ]]; then
            model_name="${ATOL_MODELS[$model_code]}"
        else
            model_name="(model $model_code)"
        fi
    else
        model_name="Не определено для данного типа"
    fi

    echo "$model_name"
}

decode_error() {
    # переменная error_code_hex берётся из контекста (is_error).
    local error_code_dec=$((16#$error_code_hex))
    if [[ "${ERROR_CODES[$error_code_dec]}" ]]; then
        echo "Ошибка: ${ERROR_CODES[$error_code_dec]} (код $error_code_dec)"
    else
        echo "Неизвестная ошибка (код $error_code_dec)"
    fi
}

is_error() {
    local error_code_hex="${RD_HEX:$1*2:2}"
    if [[ "$error_code_hex" == "00" ]]; then
        return 1
    else
        decode_error
        return 0
    fi
}

check_header() {
    local expected_header="$1"
    local received_header="${RD_HEX:0:2}"
    if [[ "$received_header" != "$expected_header" ]]; then
        echo "[error] Ожидался заголовок $expected_header, но получен $received_header"
        return 1
    fi
    return 0
}

parse_kkt_status_response() {
    check_header "44" || return 1

    KSR_CASHIER_NUMBER="$(h2d 1)"
    KSR_HALL_NUMBER="$(h2d 2)"
    KSR_DATE_YEAR="$(b2d 3)"
    KSR_DATE_MONTH="$(b2d 4)"
    KSR_DATE_DAY="$(b2d 5)"
    KSR_TIME_HOUR="$(b2d 6)"
    KSR_TIME_MINUTE="$(b2d 7)"
    KSR_TIME_SECOND="$(b2d 8)"
    KSR_STATUS_FLAGS="$(h2d 9)"
    KSR_SERIAL_NUMBER_HEX="$(hre 10 4)"
    KSR_MODEL_CODE="$(h2d 14)"
    KSR_RESERVED_HEX="$(hre 15 2)"
    KSR_MODE_CODE="$(h2d 17)"
    KSR_RECEIPT_NUMBER="$(b2d 18 2)"
    KSR_SHIFT_NUMBER="$(b2d 20 2)"
    KSR_CHECK_STATE="$(h2d 22)"
    KSR_RECEIPT_SUM="$(h2dle 23 5)"
    KSR_DECIMAL_POINT_POS="$(h2d 28)"
    KSR_PORT_CODE="$(h2d 29)"
    KSR_FLAG_FISCALIZED="$(bit "$KSR_STATUS_FLAGS" 0)"
    KSR_FLAG_SHIFT_OPEN="$(bit "$KSR_STATUS_FLAGS" 1)"
    KSR_FLAG_DRAWER_CLOSED="$(bit "$KSR_STATUS_FLAGS" 2)"
    KSR_FLAG_PAPER_PRESENT="$(bit "$KSR_STATUS_FLAGS" 3)"
    KSR_FLAG_COVER_OPEN="$(bit "$KSR_STATUS_FLAGS" 5)"
    KSR_FLAG_FN_ACTIVATED="$(bit "$KSR_STATUS_FLAGS" 6)"
    KSR_FLAG_BATTERY_LOW="$(bit "$KSR_STATUS_FLAGS" 7)"

    KSR_MODE_DESCRIPTION="$(decode_state "$KSR_MODE_CODE")"
    KSR_MODEL_NAME="$(parse_device_model "$KSR_MODEL_CODE")"

    KSR_CHECK_STATE_NAME="${CHECK_STATES[$KSR_CHECK_STATE]:-«неизвестное состояние»}"

    printf -v KSR_DATE_STR "%02d.%02d.%02d" "$KSR_DATE_DAY" "$KSR_DATE_MONTH" "$KSR_DATE_YEAR"
    printf -v KSR_TIME_STR "%02d:%02d:%02d" "$KSR_TIME_HOUR" "$KSR_TIME_MINUTE" "$KSR_TIME_SECOND"

    KSR_PORT_DESCRIPTION="${PORT_TYPES[$KSR_PORT_CODE]:-(неизвестный)}"

    jq \
        --arg var0 "$KSR_CASHIER_NUMBER" \
        --arg var1 "$KSR_HALL_NUMBER" \
        --arg var2 "$KSR_TIME_STR" \
        --arg var3 "$KSR_DATE_STR" \
        --arg var4 "$KSR_MODEL_CODE" \
        --arg var5 "$KSR_MODEL_NAME" \
        --arg var6 "$KSR_SERIAL_NUMBER_HEX" \
        --arg var7 "$KSR_MODE_CODE" \
        --arg var8 "$KSR_MODE_DESCRIPTION" \
        --arg var9 "$KSR_PORT_CODE" \
        --arg varA "$KSR_PORT_DESCRIPTION" \
        --arg varB "$KSR_FLAG_FISCALIZED" \
        --arg varC "$KSR_FLAG_FN_ACTIVATED" \
        --arg varD "$KSR_FLAG_DRAWER_CLOSED" \
        --arg varE "$KSR_FLAG_SHIFT_OPEN" \
        --arg varF "$KSR_FLAG_PAPER_PRESENT" \
        --arg varG "$KSR_FLAG_COVER_OPEN" \
        --arg varH "$KSR_RECEIPT_NUMBER" \
        --arg varI "$KSR_SHIFT_NUMBER" \
        --arg varJ "$KSR_DECIMAL_POINT_POS" \
        --arg varK "$KSR_CHECK_STATE" \
        --arg varL "$KSR_RECEIPT_SUM" \
        --arg varM "$KSR_FLAG_BATTERY_LOW" \
        --arg varN "$KSR_CHECK_STATE_NAME" \
        '
       .["статус_ккт"] = {
         "текущий_кассир":       ($var0 | tonumber),
         "номер_ккт_в_зале":     ($var1 | tonumber),
         "фискализирована":      ($varB == "1"),
         "фн_активизирован":     ($varC == "1"),
         "текущее_время_ккт":    $var2,
         "текущая_дата_ккт":     $var3,
         "код_модели_ккт":       ($var4 | tonumber),
         "название_модели_ккт":  $var5,
         "заводской_номер_hex":  $var6,
         "код_режима_работы":    ($var7 | tonumber),
         "описание_режима_работы": $var8,
         "код_порта":            ($var9 | tonumber),
         "описание_порта":       $varA,
         "денежный_ящик_закрыт": ($varD == "1"),
         "смена_открыта":        ($varE == "1"),
         "бумага_в_наличии":     ($varF == "1"),
         "крышка_принтера_открыта": ($varG == "1"),
         "номер_текущего_чека":  ($varH | tonumber),
         "номер_смены":          ($varI | tonumber),
         "позиция_десятичной_точки": ($varJ | tonumber),
         "код_состояния_чека":   ($varK | tonumber),
         "описание_состояния_чека": $varN,
         "текущая_сумма_в_чеке": ($varL | tonumber),
         "батарея_разряжена":    ($varM == "1")
       }
      ' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Текущий кассир: $KSR_CASHIER_NUMBER"
    echo "Номер ККТ в зале: $KSR_HALL_NUMBER"
    echo "Касса $(ternary "$KSR_FLAG_FISCALIZED" "фискализирована" "не фискализирована")"
    echo "ФН $(ternary "$KSR_FLAG_FN_ACTIVATED" "активизирован" "не активизирован")"
    echo "Текущее время ККТ: $KSR_TIME_STR"
    echo "Текущая дата ККТ: $KSR_DATE_STR"
    echo "Модель ККТ: $KSR_MODEL_NAME"
    echo "Заводской номер (HEX): $KSR_SERIAL_NUMBER_HEX"
    echo "Текущий режим работы: $KSR_MODE_DESCRIPTION"
    echo "Соединение с ПК по порту: $KSR_PORT_DESCRIPTION"
    echo "Денежный ящик: $(ternary "$KSR_FLAG_DRAWER_CLOSED" "закрыт" "открыт")"
    echo "Смена: $(ternary "$KSR_FLAG_SHIFT_OPEN" "открыта" "закрыта")"
    echo "Датчик чековой ленты: $(ternary "$KSR_FLAG_PAPER_PRESENT" "бумага есть" "нет бумаги")"
    echo "Крышка принтера: $(ternary "$KSR_FLAG_COVER_OPEN" "открыта" "закрыта")"
    echo "Номер текущего чека: $KSR_RECEIPT_NUMBER"
    echo "Номер последней закрытой смены: $KSR_SHIFT_NUMBER"
    echo "Положение десятичной точки в денежных суммах: $KSR_DECIMAL_POINT_POS"
    echo "Состояние чека: $KSR_CHECK_STATE_NAME"
    echo "Текущая сумма в чеке: $KSR_RECEIPT_SUM"
}

parse_kkt_state_code_response() {
    check_header "55" || return 1

    KSCR_MODE_CODE="$(h2d 1)"
    KSCR_FLAGS_BYTE="$(h2d 2)"
    KSCR_MODE_STR="$(decode_state "$KSCR_MODE_CODE")"
    KSCR_FLAG_NO_PAPER="$(bit "$KSCR_FLAGS_BYTE" 0)"
    KSCR_FLAG_NO_PRINTER_CONN="$(bit "$KSCR_FLAGS_BYTE" 1)"
    KSCR_FLAG_CUTTER_ERROR="$(bit "$KSCR_FLAGS_BYTE" 3)"
    KSCR_FLAG_PRINTER_ERROR="$(bit "$KSCR_FLAGS_BYTE" 4)"

    jq \
        --arg var0 "$KSCR_MODE_CODE" \
        --arg var1 "$KSCR_MODE_STR" \
        --arg var2 "$KSCR_FLAG_NO_PAPER" \
        --arg var3 "$KSCR_FLAG_NO_PRINTER_CONN" \
        --arg var4 "$KSCR_FLAG_CUTTER_ERROR" \
        --arg var5 "$KSCR_FLAG_PRINTER_ERROR" '
       .["состояние_принтера"] = {
         "код_режима_работы":     ($var0 | tonumber),
         "описание_режима_работы": $var1,
         "нет_бумаги":            ($var2 == "1"),
         "нет_связи_с_принтером": ($var3 == "1"),
         "ошибка_отрезчика":      ($var4 == "1"),
         "ошибка_принтера":       ($var5 == "1")
       }' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Текущий режим работы ККТ: $KSCR_MODE_STR"
    if ((KSCR_FLAG_NO_PRINTER_CONN == 1)); then
        echo "Связь с принтером: НЕТ (невозможно определить состояние бумаги, отрезчика и т.п.)"
    else
        echo "Связь с принтером: есть"
        echo "Состояние бумаги в принтере: $(ternary "$KSCR_FLAG_NO_PAPER" "бумага отсутствует" "бумага есть")"
        echo "Ошибка отрезчика: $(ternary "$KSCR_FLAG_CUTTER_ERROR" "ДА" "нет")"
        echo "Ошибка принтера (например перегрев): $(ternary "$KSCR_FLAG_PRINTER_ERROR" "ДА" "нет")"
    fi
}

parse_factory_number_response() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFNR_FACTORY_NUMBER="$(hre 2 7)"

    jq \
        --arg var0 "$PFNR_FACTORY_NUMBER" '
       .["заводской_номер"] = {
         "номер": $var0
       }' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Заводской номер ККТ: $PFNR_FACTORY_NUMBER"
}

parse_control_unit_version_response() {
    check_header "55" || return 1
    is_error 1 && return 1

    PCUVR_VERSION="$(hre 2)"
    PCUVR_VERSION_STR="${PCUVR_VERSION:0:1}.${PCUVR_VERSION:1:1}"

    jq \
        --arg ver "$PCUVR_VERSION_STR" '
        .["блок_управления"] = {
           "версия":   $ver
        }
        ' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Версия и сборка электронного модуля/блока управления: $PCUVR_VERSION_STR"
}

parse_ffd_version_response() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFVR_KKT_FFD_VERSION="$(h2d 2)"
    PFVR_FN_FFD_VERSION="$(h2d 3)"
    PFVR_FFD_VERSION="$(h2d 4)"
    PFVR_DAY="$(b2d 5)"
    PFVR_MONTH="$(b2d 6)"
    PFVR_YEAR="$(b2d 7)"

    printf -v PFVR_DATE_STR "%02d.%02d.%02d" \
        "$PFVR_DAY" \
        "$PFVR_MONTH" \
        "$PFVR_YEAR"

    PFVR_KKT_FFD_STR="${FFD_VERSIONS[$PFVR_KKT_FFD_VERSION]:-неизвестная версия}"
    PFVR_FN_FFD_STR="${FFD_VERSIONS[$PFVR_FN_FFD_VERSION]:-неизвестная версия}"
    PFVR_FFD_STR="${FFD_VERSIONS[$PFVR_FFD_VERSION]:-неизвестная версия}"

    jq \
        --arg var0 "$PFVR_KKT_FFD_VERSION" \
        --arg var1 "$PFVR_KKT_FFD_STR" \
        --arg var2 "$PFVR_FN_FFD_VERSION" \
        --arg var3 "$PFVR_FN_FFD_STR" \
        --arg var4 "$PFVR_FFD_VERSION" \
        --arg var5 "$PFVR_FFD_STR" \
        --arg var6 "$PFVR_DATE_STR" '
       .["версия_ффд"] = {
         "версия_ффд_ккт_код":          ($var0 | tonumber),
         "версия_ффд_ккт":              $var1,
         "версия_ффд_фн_код":           ($var2 | tonumber),
         "версия_ффд_фн":               $var3,
         "версия_ффд_код":              ($var4 | tonumber),
         "версия_ффд":                  $var5,
         "дата_ффд":              $var6,
       }' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Версия ФФД ККТ: $PFVR_KKT_FFD_STR"
    echo "Версия ФФД ФН: $PFVR_FN_FFD_STR"
    echo "Версия ФФД: $PFVR_FFD_STR"
    echo "Дата ФФД: $PFVR_DATE_STR"
}

parse_firmware_version_response_01() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFVR_FIRMWARE_VERSION_MAJOR="$(b2d 2)"
    PFVR_FIRMWARE_VERSION_MINOR="$(b2d 3)"
    PFVR_FIRMWARE_BUILD_NUMBER="$(b2d 5 2)"

    PFVR_FIRMWARE_VERSION_STR="${PFVR_FIRMWARE_VERSION_MAJOR}.${PFVR_FIRMWARE_VERSION_MINOR}.${PFVR_FIRMWARE_BUILD_NUMBER}"

    jq \
        --arg major "$PFVR_FIRMWARE_VERSION_MAJOR" \
        --arg minor "$PFVR_FIRMWARE_VERSION_MINOR" \
        --arg build "$PFVR_FIRMWARE_BUILD_NUMBER" '
        .["прошивка"] = {
           "основная_версия": ($major | tonumber),
           "минорная_версия": ($minor | tonumber),
           "номер_сборки":    ($build | tonumber)
        }
        ' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Версия ПО (прошивка): $PFVR_FIRMWARE_VERSION_STR"
}

parse_firmware_version_response_03() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFVR_BOOTLOADER_VERSION_MAJOR="$(b2d 2)"
    PFVR_BOOTLOADER_VERSION_MINOR="$(b2d 3)"
    PFVR_BOOTLOADER_BUILD_NUMBER="$(b2d 5 2)"

    PFVR_BOOTLOADER_VERSION_STR="${PFVR_BOOTLOADER_VERSION_MAJOR}.${PFVR_BOOTLOADER_VERSION_MINOR}.${PFVR_BOOTLOADER_BUILD_NUMBER}"

    jq \
        --arg major "$PFVR_BOOTLOADER_VERSION_MAJOR" \
        --arg minor "$PFVR_BOOTLOADER_VERSION_MINOR" \
        --arg build "$PFVR_BOOTLOADER_BUILD_NUMBER" '
        .["загрузчик"] = {
           "основная_версия": ($major | tonumber),
           "минорная_версия": ($minor | tonumber),
           "номер_сборки":    ($build | tonumber)
        }
        ' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Версия ПО (загрузчик): $PFVR_BOOTLOADER_VERSION_STR"
}

parse_firmware_version_response_91() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFVR_CONFIG_VERSION_MAJOR="$(b2d 2)"
    PFVR_CONFIG_VERSION_MINOR="$(b2d 3)"
    PFVR_CONFIG_BUILD_NUMBER="$(b2d 5 2)"

    PFVR_CONFIG_VERSION_STR="${PFVR_CONFIG_VERSION_MAJOR}.${PFVR_CONFIG_VERSION_MINOR}.${PFVR_CONFIG_BUILD_NUMBER}"

    jq \
        --arg major "$PFVR_CONFIG_VERSION_MAJOR" \
        --arg minor "$PFVR_CONFIG_VERSION_MINOR" \
        --arg build "$PFVR_CONFIG_BUILD_NUMBER" '
        .["конфигурация"] = {
           "основная_версия": ($major | tonumber),
           "минорная_версия": ($minor | tonumber),
           "номер_сборки":    ($build | tonumber)
        }
        ' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Версия ПО (конфигурация): $PFVR_CONFIG_VERSION_STR"
}

parse_firmware_version_response_A0() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFVR_RELEASE_VERSION_MAJOR="$(b2d 2)"
    PFVR_RELEASE_VERSION_MINOR="$(b2d 3)"
    PFVR_RELEASE_BUILD_NUMBER="$(b2d 5 2)"

    PFVR_RELEASE_VERSION_STR="${PFVR_RELEASE_VERSION_MAJOR}.${PFVR_RELEASE_VERSION_MINOR}.${PFVR_RELEASE_BUILD_NUMBER}"

    jq \
        --arg major "$PFVR_RELEASE_VERSION_MAJOR" \
        --arg minor "$PFVR_RELEASE_VERSION_MINOR" \
        --arg build "$PFVR_RELEASE_BUILD_NUMBER" '
        .["номер_релиза"] = {
           "основная_версия": ($major | tonumber),
           "минорная_версия": ($minor | tonumber),
           "номер_сборки":    ($build | tonumber)
        }
        ' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Версия ПО (релиз): $PFVR_RELEASE_VERSION_STR"
}

parse_current_shift_params_response() {
    check_header "55" || return 1
    is_error 1 && return 1

    PCSPR_SHIFT_STATUS="$(h2d 2)"
    PCSPR_SHIFT_NUMBER="$(h2dle 3 2)"
    PCSPR_LAST_CHECK_NUMBER="$(h2dle 5 2)"

    jq \
        --arg var0 "$PCSPR_SHIFT_STATUS" \
        --arg var1 "$PCSPR_SHIFT_NUMBER" \
        --arg var2 "$PCSPR_LAST_CHECK_NUMBER" \
        --arg var3 "$docMeaning" \
        '
       .["параметры_смены"] = {
         "смена_открыта":                  ($var0 == "1"),
         "номер_смены":                   ($var1 | tonumber),
         "номер_чека":    ($var2 | tonumber),
       }
      ' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    if [[ "$PCSPR_SHIFT_STATUS" == 1 ]]; then
        echo "Смена в ФН: открыта"
        echo "Номер текущей смены: $PCSPR_SHIFT_NUMBER"
        echo "Номер последнего сформированного чека: $PCSPR_LAST_CHECK_NUMBER"
    else
        echo "Смена в ФН: закрыта"
        echo "Номер последней закрытой смены: $PCSPR_SHIFT_NUMBER"
        echo "Количество документов в закрытой смене: $PCSPR_LAST_CHECK_NUMBER"
    fi
}

parse_fn_exchange_status_response() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFESR_EXCHANGE_STATUS_FLAGS="$(h2d 2)"
    PFESR_IS_READING_OFD_MESSAGE="$(h2d 3)"
    PFESR_OFD_MSG_COUNT="$(h2dle 4 2)"
    PFESR_OFD_FIRST_DOC_NUMBER="$(h2dle 6 4)"
    PFESR_FIRST_DOC_DT_YEAR="$(h2d 10)"
    PFESR_FIRST_DOC_DT_MONTH="$(h2d 11)"
    PFESR_FIRST_DOC_DT_DAY="$(h2d 12)"
    PFESR_FIRST_DOC_DT_HOUR="$(h2d 13)"
    PFESR_FIRST_DOC_DT_MINUTE="$(h2d 14)"

    PFESR_FLAG_TRANSPORT_CONN="$(bit "$PFESR_EXCHANGE_STATUS_FLAGS" 0)"
    PFESR_FLAG_MSG_TO_SEND="$(bit "$PFESR_EXCHANGE_STATUS_FLAGS" 1)"
    PFESR_FLAG_WAITING_OFD_ACK="$(bit "$PFESR_EXCHANGE_STATUS_FLAGS" 2)"
    PFESR_FLAG_OFD_COMMAND="$(bit "$PFESR_EXCHANGE_STATUS_FLAGS" 3)"
    PFESR_FLAG_OFD_SETTINGS_CHANGED="$(bit "$PFESR_EXCHANGE_STATUS_FLAGS" 4)"
    PFESR_FLAG_WAITING_OFD_CMD_RESPONSE="$(bit "$PFESR_EXCHANGE_STATUS_FLAGS" 5)"

    printf -v PFESR_FIRST_DOC_DATETIME "%02d.%02d.%02d %02d:%02d" \
        "$PFESR_FIRST_DOC_DT_DAY" \
        "$PFESR_FIRST_DOC_DT_MONTH" \
        "$PFESR_FIRST_DOC_DT_YEAR" \
        "$PFESR_FIRST_DOC_DT_HOUR" \
        "$PFESR_FIRST_DOC_DT_MINUTE"

    jq \
        --arg var0 "$PFESR_FLAG_TRANSPORT_CONN" \
        --arg var1 "$PFESR_FLAG_MSG_TO_SEND" \
        --arg var2 "$PFESR_FLAG_WAITING_OFD_ACK" \
        --arg var3 "$PFESR_FLAG_OFD_COMMAND" \
        --arg var4 "$PFESR_FLAG_OFD_SETTINGS_CHANGED" \
        --arg var5 "$PFESR_FLAG_WAITING_OFD_CMD_RESPONSE" \
        --arg var6 "$PFESR_IS_READING_OFD_MESSAGE" \
        --arg var7 "$PFESR_OFD_MSG_COUNT" \
        --arg var8 "$PFESR_OFD_FIRST_DOC_NUMBER" \
        --arg var9 "$PFESR_FIRST_DOC_DATETIME" '
       .["статус_обмена_с_офд"] = {
         "транспортное_соединение_установлено": ($var0 == "1"),
         "есть_сообщение_для_офд":             ($var1 == "1"),
         "ожидание_квитанции_офд":             ($var2 == "1"),
         "есть_команда_от_офд":                ($var3 == "1"),
         "настройки_офд_изменены":             ($var4 == "1"),
         "ожидание_ответа_на_команду_офд":     ($var5 == "1"),
         "идёт_чтение_сообщения_для_офд":      ($var6 == "1"),
         "количество_документов_для_офд":      ($var7 | tonumber),
         "номер_первого_документа_в_очереди":  ($var8 | tonumber),
         "дата_время_первого_документа":       $var9
       }' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Статус обмена с ОФД:"
    echo "  Транспортное соединение: $(ternary "$PFESR_FLAG_TRANSPORT_CONN" "установлено" "не установлено")"
    echo "  Наличие сообщения для ОФД: $(ternary "$PFESR_FLAG_MSG_TO_SEND" "есть" "нет")"
    echo "  Ожидание квитанции от ОФД: $(ternary "$PFESR_FLAG_WAITING_OFD_ACK" "да" "нет")"
    echo "  Наличие команды от ОФД: $(ternary "$PFESR_FLAG_OFD_COMMAND" "есть" "нет")"
    echo "  Изменение настроек соединения: $(ternary "$PFESR_FLAG_OFD_SETTINGS_CHANGED" "произошло" "нет")"
    echo "  Ожидание ответа на команду ОФД: $(ternary "$PFESR_FLAG_WAITING_OFD_CMD_RESPONSE" "да" "нет")"
    echo
    echo "Состояние чтения сообщения для ОФД: $(ternary "$PFESR_IS_READING_OFD_MESSAGE" "идёт чтение" "не идёт")"
    echo "Количество документов для передачи ОФД: $PFESR_OFD_MSG_COUNT"
    echo "Номер первого документа в очереди: $PFESR_OFD_FIRST_DOC_NUMBER"
    echo "Дата-время первого документа в очереди: $PFESR_FIRST_DOC_DATETIME"
}

parse_fn_status_response() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFNSR_LIFE_PHASE="$(h2d 2)"
    PFNSR_DOC_TYPE="$(h2d 3)"
    PFNSR_FLAG_DOC_DATA="$(h2d 4)"
    PFNSR_FLAG_SHIFT_OPEN="$(h2d 5)"
    PFNSR_WARNING_FLAGS="$(h2d 6)"
    PFNSR_DT_YEAR="$(h2d 7)"
    PFNSR_DT_MONTH="$(h2d 8)"
    PFNSR_DT_DAY="$(h2d 9)"
    PFNSR_DT_HOUR="$(h2d 10)"
    PFNSR_DT_MINUTE="$(h2d 11)"
    PFNSR_FN_NUMBER="$(hre 12 16 | xxd -r -p)"
    PFNSR_LAST_FD_NUMBER="$(h2dle 28 4)"

    printf -v PFNSR_LAST_DOC_DATETIME "%02d.%02d.%02d %02d:%02d" \
        "$PFNSR_DT_DAY" \
        "$PFNSR_DT_MONTH" \
        "$PFNSR_DT_YEAR" \
        "$PFNSR_DT_HOUR" \
        "$PFNSR_DT_MINUTE"

    PFNSR_LIFE_PHASE_STR="${FN_LIFE_PHASES[$PFNSR_LIFE_PHASE]:-неизвестная фаза ($PFNSR_LIFE_PHASE)}"
    PFNSR_CURRENT_DOC_STR="${CURRENT_DOCUMENTS[$PFNSR_DOC_TYPE]:-неизвестный документ ($PFNSR_DOC_TYPE)}"

    PFNSR_FLAG_NEED_FN_REPLACEMENT="$(bit "$PFNSR_WARNING_FLAGS" 0)"
    PFNSR_FLAG_KS_RESOURCE_EXHAUSTED="$(bit "$PFNSR_WARNING_FLAGS" 1)"
    PFNSR_FLAG_FN_MEMORY_FULL="$(bit "$PFNSR_WARNING_FLAGS" 2)"
    PFNSR_FLAG_OFD_TIMEOUT="$(bit "$PFNSR_WARNING_FLAGS" 3)"
    PFNSR_FLAG_CRITICAL_ERROR="$(bit "$PFNSR_WARNING_FLAGS" 7)"

    jq \
        --arg var0 "$PFNSR_LIFE_PHASE" \
        --arg var1 "$PFNSR_LIFE_PHASE_STR" \
        --arg var2 "$PFNSR_DOC_TYPE" \
        --arg var3 "$PFNSR_CURRENT_DOC_STR" \
        --arg var4 "$PFNSR_FLAG_DOC_DATA" \
        --arg var5 "$PFNSR_FLAG_SHIFT_OPEN" \
        --arg var6 "$PFNSR_FLAG_NEED_FN_REPLACEMENT" \
        --arg var7 "$PFNSR_FLAG_KS_RESOURCE_EXHAUSTED" \
        --arg var8 "$PFNSR_FLAG_FN_MEMORY_FULL" \
        --arg var9 "$PFNSR_FLAG_OFD_TIMEOUT" \
        --arg varA "$PFNSR_FLAG_CRITICAL_ERROR" \
        --arg varB "$PFNSR_LAST_DOC_DATETIME" \
        --arg varC "$PFNSR_FN_NUMBER" \
        --arg varD "$PFNSR_LAST_FD_NUMBER" '
       .["статус_фн"] = {
         "фаза_жизни_фн_код":           ($var0 | tonumber),
         "фаза_жизни_фн_описание":      $var1,
         "код_текущего_документа":      ($var2 | tonumber),
         "текущий_документ_описание":   $var3,
         "есть_данные_в_документе":     ($var4 == "1"),
         "смена_открыта_в_фн":          ($var5 == "1"),
         "нужна_замена_фн":             ($var6 == "1"),
         "ресурс_кс_30_дней":           ($var7 == "1"),
         "память_фн_заполнена_90":      ($var8 == "1"),
         "нет_ответа_офд_5_минут":      ($var9 == "1"),
         "критическая_ошибка_фн":       ($varA == "1"),
         "дата_и_время_последнего_документа": $varB,
         "номер_фн":                    ($varC | tonumber),
         "номер_последнего_фд":         ($varD | tonumber)
       }
      ' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Фаза жизни ФН: $PFNSR_LIFE_PHASE_STR"
    echo "Текущий открытый документ: $PFNSR_CURRENT_DOC_STR"
    echo "Данные в документе: $(ternary "$PFNSR_FLAG_DOC_DATA" "получены" "отсутствуют")"
    echo "Смена в ФН: $(ternary "$PFNSR_FLAG_SHIFT_OPEN" "открыта" "закрыта")"
    echo "Предупреждения ФН:"
    echo "  Требуется срочная замена ФН: $(ternary "$PFNSR_FLAG_NEED_FN_REPLACEMENT" "да" "нет")"
    echo "  Ресурс КС исчерпан (<30 дней): $(ternary "$PFNSR_FLAG_KS_RESOURCE_EXHAUSTED" "да" "нет")"
    echo "  Память ФН переполнена (90%+): $(ternary "$PFNSR_FLAG_FN_MEMORY_FULL" "да" "нет")"
    echo "  Превышено ожидание ответа ОФД (5 мин): $(ternary "$PFNSR_FLAG_OFD_TIMEOUT" "да" "нет")"
    echo "  Критическая ошибка ФН: $(ternary "$PFNSR_FLAG_CRITICAL_ERROR" "да" "нет")"
    echo "Дата и время последнего документа: $PFNSR_LAST_DOC_DATETIME"
    echo "Текущий номер ФН: $PFNSR_FN_NUMBER"
    echo "Номер последнего фискального документа: $PFNSR_LAST_FD_NUMBER"
}

parse_fn_number_response() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFNNR_FN_NUMBER="$(hre 2 16 | xxd -r -p)"

    jq \
        --arg var0 "$PFNNR_FN_NUMBER" '
       .["номер_фн"] = {
         "номер_установленного_фн": $var0
       }' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Номер установленного ФН: $PFNNR_FN_NUMBER"
}

parse_fn_expiry_response() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFNER_YEAR="$(h2d 2)"
    PFNER_MONTH="$(h2d 3)"
    PFNER_DAY="$(h2d 4)"
    PFNER_REMAINING_REGISTRATIONS="$(h2d 5)"
    PFNER_USED_REGISTRATIONS="$(h2d 6)"

    printf -v PFNER_DATE_STR "%02d.%02d.%02d" "$PFNER_DAY" "$PFNER_MONTH" "$PFNER_YEAR"

    jq \
        --arg var0 "$PFNER_DAY" \
        --arg var1 "$PFNER_MONTH" \
        --arg var2 "$PFNER_YEAR" \
        --arg var3 "$PFNER_REMAINING_REGISTRATIONS" \
        --arg var4 "$PFNER_USED_REGISTRATIONS" \
        --arg var5 "$PFNER_DATE_STR" '
       .["срок_действия_фн"] = {
         "дата_окончания": $var5,
         "осталось_перерегистраций": ($var3 | tonumber),
         "совершено_регистраций_перерегистраций": ($var4 | tonumber)
       }' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Срок действия ФН (ДД.ММ.ГГ): $PFNER_DATE_STR"
    echo "Осталось перерегистраций: $PFNER_REMAINING_REGISTRATIONS"
    echo "Сделано регистраций/перерегистраций: $PFNER_USED_REGISTRATIONS"
}

parse_fn_version_response() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFNVR_FN_VERSION_STR="$(hre 2 16 | xxd -r -p)"
    PFNVR_FN_TYPE_CODE="$(h2d 18)"
    PFNVR_FN_TYPE_STR="$(ternary "$PFNVR_FN_TYPE_CODE" "серийная версия" "отладочная версия")"

    jq \
        --arg var0 "$PFNVR_FN_VERSION_STR" \
        --arg var1 "$PFNVR_FN_TYPE_CODE" \
        --arg var2 "$PFNVR_FN_TYPE_STR" '
       .["версия_фн"] = {
         "текстовая_версия_фн": $var0,
         "код_типа_по_фн":      ($var1 | tonumber),
         "описание_типа_по_фн": $var2
       }' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Версия ФН (текст): $PFNVR_FN_VERSION_STR"
    echo "Тип ПО ФН: $PFNVR_FN_TYPE_STR"
}

parse_fn_fiscalization_result_response() {
    check_header "55" || return 1
    is_error 1 && return 1

    PFFRR_DT_YEAR="$(h2d 2)"
    PFFRR_DT_MONTH="$(h2d 3)"
    PFFRR_DT_DAY="$(h2d 4)"
    PFFRR_DT_HOUR="$(h2d 5)"
    PFFRR_DT_MINUTE="$(h2d 6)"
    PFFRR_INN_STR="$(hre 7 12 | xxd -r -p | tr -d ' ')"
    PFFRR_RNM_STR="$(hre 19 20 | xxd -r -p | tr -d ' ')"
    PFFRR_TAX_SYSTEM_FLAGS="$(h2d 39)"
    PFFRR_FN_MODE_FLAGS="$(h2d 40)"
    PFFRR_FD_NUMBER="$(h2dle 41 4)"
    PFFRR_FISCAL_SIGN="$(h2dle 45 4)"

    printf -v PFFRR_DATETIME_STR "%02d.%02d.%02d %02d:%02d" \
        "$PFFRR_DT_DAY" \
        "$PFFRR_DT_MONTH" \
        "$PFFRR_DT_YEAR" \
        "$PFFRR_DT_HOUR" \
        "$PFFRR_DT_MINUTE"

    PFFRR_FLAG_TAX_COMMON="$(bit "$PFFRR_TAX_SYSTEM_FLAGS" 0)"
    PFFRR_FLAG_TAX_USN_INCOME="$(bit "$PFFRR_TAX_SYSTEM_FLAGS" 1)"
    PFFRR_FLAG_TAX_USN_INOUT="$(bit "$PFFRR_TAX_SYSTEM_FLAGS" 2)"
    PFFRR_FLAG_TAX_ENVD="$(bit "$PFFRR_TAX_SYSTEM_FLAGS" 3)"
    PFFRR_FLAG_TAX_ESXN="$(bit "$PFFRR_TAX_SYSTEM_FLAGS" 4)"
    PFFRR_FLAG_TAX_PATENT="$(bit "$PFFRR_TAX_SYSTEM_FLAGS" 5)"
    PFFRR_FLAG_ENC="$(bit "$PFFRR_FN_MODE_FLAGS" 0)"
    PFFRR_FLAG_AUTONOM="$(bit "$PFFRR_FN_MODE_FLAGS" 1)"
    PFFRR_FLAG_AUTO_MODE="$(bit "$PFFRR_FN_MODE_FLAGS" 2)"
    PFFRR_FLAG_SERVICES="$(bit "$PFFRR_FN_MODE_FLAGS" 3)"
    PFFRR_FLAG_INTERNET="$(bit "$PFFRR_FN_MODE_FLAGS" 5)"

    jq \
        --arg var0 "$PFFRR_DATETIME_STR" \
        --arg var1 "$PFFRR_INN_STR" \
        --arg var2 "$PFFRR_RNM_STR" \
        --arg var3 "$PFFRR_FLAG_TAX_COMMON" \
        --arg var4 "$PFFRR_FLAG_TAX_USN_INCOME" \
        --arg var5 "$PFFRR_FLAG_TAX_USN_INOUT" \
        --arg var6 "$PFFRR_FLAG_TAX_ENVD" \
        --arg var7 "$PFFRR_FLAG_TAX_ESXN" \
        --arg var8 "$PFFRR_FLAG_TAX_PATENT" \
        --arg var9 "$PFFRR_FLAG_ENC" \
        --arg varA "$PFFRR_FLAG_AUTONOM" \
        --arg varB "$PFFRR_FLAG_AUTO_MODE" \
        --arg varC "$PFFRR_FLAG_SERVICES" \
        --arg varD "$PFFRR_FLAG_INTERNET" \
        --arg varE "$PFFRR_FD_NUMBER" \
        --arg varF "$PFFRR_FISCAL_SIGN" '
       .["результат_фискализации"] = {
         "дата_и_время_фискализации": $var0,
         "инн":         ($var1 | tonumber),
         "рнм":         $var2,
         "сно_общая":   ($var3 == "1"),
         "сно_усн_доход": ($var4 == "1"),
         "сно_усн_доход_расход": ($var5 == "1"),
         "сно_енвд":    ($var6 == "1"),
         "сно_есхн":    ($var7 == "1"),
         "сно_патент":  ($var8 == "1"),
         "режим_шифрование":    ($var9 == "1"),
         "режим_автономный":    ($varA == "1"),
         "режим_автоматический":($varB == "1"),
         "режим_услуги":        ($varC == "1"),
         "режим_интернет":      ($varD == "1"),
         "номер_фд":            ($varE | tonumber),
         "фискальный_признак":  ($varF | tonumber)
       }' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

    echo "Дата/время фискализации ФН: $PFFRR_DATETIME_STR"
    echo "ИНН: $PFFRR_INN_STR"
    echo "Регистрационный номер ККТ (РНМ): $PFFRR_RNM_STR"
    echo "Системы налогообложения (по битам):"
    echo "  Общая:               $(ternary "$PFFRR_FLAG_TAX_COMMON" "да" "нет")"
    echo "  УСН (доход):         $(ternary "$PFFRR_FLAG_TAX_USN_INCOME" "да" "нет")"
    echo "  УСН (доход-расход):  $(ternary "$PFFRR_FLAG_TAX_USN_INOUT" "да" "нет")"
    echo "  ЕНВД:                $(ternary "$PFFRR_FLAG_TAX_ENVD" "да" "нет")"
    echo "  ЕСХН:                $(ternary "$PFFRR_FLAG_TAX_ESXN" "да" "нет")"
    echo "  Патент:              $(ternary "$PFFRR_FLAG_TAX_PATENT" "да" "нет")"
    echo
    echo "Режим работы (по битам):"
    echo "  Шифрование:           $(ternary "$PFFRR_FLAG_ENC" "включено" "выключено")"
    echo "  Автономный режим:     $(ternary "$PFFRR_FLAG_AUTONOM" "да" "нет")"
    echo "  Автоматический режим: $(ternary "$PFFRR_FLAG_AUTO_MODE" "да" "нет")"
    echo "  Применение в услугах: $(ternary "$PFFRR_FLAG_SERVICES" "да" "нет")"
    echo "  Работа в Интернет:    $(ternary "$PFFRR_FLAG_INTERNET" "разрешена" "запрещена")"
    echo
    echo "Номер фискального документа (ФД): $PFFRR_FD_NUMBER"
    echo "Фискальный признак (ФП): $PFFRR_FISCAL_SIGN"
}

parse_device_info_response() {
    is_error 0 && return 1

    DIR_PROTOCOL_VER="$(h2d 1)"

    if ((DIR_PROTOCOL_VER == 1)); then
        DIR_DEVICE_TYPE_DEC="$(h2d 2)"
        DIR_DEVICE_MODEL_DEC="$(h2d 3)"
        DIR_VERSION_MAJOR="$(b2d 6)"
        DIR_VERSION_MINOR="$(b2d 7)"
        DIR_LANG_CODE="$(b2d 8)"
        DIR_BUILD_NUMBER="$(b2d 9 2)"
        DIR_DEVICE_NAME="$(hree 11 | xxd -r -p | iconv -f cp866)"

        DIR_DEVICE_TYPE_STR="${DEVICE_TYPES[$DIR_DEVICE_TYPE_DEC]:-Зарезервировано/неизв.}"
        DIR_DEVICE_MODEL_STR="$(parse_device_model "$DIR_DEVICE_MODEL_DEC" "$DIR_DEVICE_TYPE_DEC")"

        jq \
            --arg var0 "$DIR_DEVICE_TYPE_DEC" \
            --arg var1 "$DIR_DEVICE_TYPE_STR" \
            --arg var2 "$DIR_DEVICE_MODEL_DEC" \
            --arg var3 "$DIR_DEVICE_MODEL_STR" \
            --arg var4 "$DIR_DEVICE_NAME" \
            --arg var5 "$DIR_VERSION_MAJOR" \
            --arg var6 "$DIR_VERSION_MINOR" \
            --arg var7 "$DIR_BUILD_NUMBER" \
            --arg var8 "$DIR_LANG_CODE" '
           .["информация_об_устройстве"] = {
             "код_типа_устройства":    ($var0 | tonumber),
             "описание_типа_устройства": $var1,
             "код_модели_устройства": ($var2 | tonumber),
             "описание_модели_устройства": $var3,
             "название_устройства_cp866": $var4,
             "основная_версия_по":       ($var5 | tonumber),
             "минорная_версия_по":       ($var6 | tonumber),
             "номер_сборки_по":          ($var7 | tonumber),
             "код_языковой_таблицы":     ($var8 | tonumber)
           }' "$KKT_JSON_FILE" | sponge "$KKT_JSON_FILE"

        echo "Тип устройства: $DIR_DEVICE_TYPE_STR"
        echo "Модель устройства: $DIR_DEVICE_MODEL_STR"
        echo "Название устройства (CP866): $DIR_DEVICE_NAME"
        echo "Версия ПО устройства: ${DIR_VERSION_MAJOR}.${DIR_VERSION_MINOR}.${DIR_BUILD_NUMBER}"
        echo "Код языковой таблицы: $DIR_LANG_CODE"
    else
        echo "Протокол этой версии устройства не поддерживается (версия протокола = $DIR_PROTOCOL_VER)."
    fi
}

tp_command() {
    echo "===== Версии ====="
    send_command_hex "9D01"
    send_command_hex "9D03"
    send_command_hex "9D91"
    send_command_hex "9DA0"
    send_command_hex "91240000"

    echo -ne "${FG_GREEN}"
    echo "===== Получаем статус ККТ ====="
    send_command_hex "3F"
    echo -ne "${NC}"

    echo -ne "${FG_BRIGHT_BLUE}"
    echo "===== Получаем текущий статус принтера ====="
    send_command_hex "45"
    echo -ne "${NC}"

    echo -ne "${FG_YELLOW}"
    echo "===== Получаем параметры смены ====="
    send_command_hex "A410"
    echo -ne "${NC}"

    echo -ne "${FG_BRIGHT_CYAN}"
    echo "===== Получаем статус обмена с ОФД ====="
    send_command_hex "A420"
    echo -ne "${NC}"

    echo -ne "${FG_MAGENTA}"
    echo "===== Получаем статус ФН ====="
    send_command_hex "A430"
    echo -ne "${NC}"

    echo -ne "${FG_CYAN}"
    echo "===== Получаем срок действия ФН ====="
    send_command_hex "A432"
    echo -ne "${NC}"

    echo -ne "${FG_BRIGHT_YELLOW}"
    echo "===== Получаем результат фискализации ====="
    send_command_hex "A443"
    echo -ne "${NC}"

    echo -ne "${FG_GREEN}"
    echo "===== Получаем заводской номер ====="
    send_command_hex "91160000"
    echo -ne "${NC}"

    echo -ne "${FG_YELLOW}"
    echo "===== Получаем версию ФФД ====="
    send_command_hex "91360000"
    echo -ne "${NC}"
}

auto_detect_and_invoke() {
    local found_any_device=false
    for dev in /dev/ttyACM*; do
        if [[ -c "$dev" ]] &&
            udevadm info --name="$dev" --query=property >"$PROPERTIES_FILE" 2>/dev/null &&
            grep '^ID_VENDOR_ID=2912' "$PROPERTIES_FILE" &>/dev/null &&
            grep '^ID_MODEL_ID=0005' "$PROPERTIES_FILE" &>/dev/null &&
            grep '^ID_USB_INTERFACE_NUM=00' "$PROPERTIES_FILE" &>/dev/null; then
            found_any_device=true
            echo "Автообнаружено подходящее устройство: $dev"
            "$0" \
                --device "$dev" \
                --baudrate "$BAUDRATE" \
                --password "$PASSWORD" \
                "$COMMAND_PAYLOAD"
        fi
    done

    if ! $found_any_device; then
        echo "Автопоиск не нашёл ни одного подходящего устройства."
        exit 1
    fi

    exit 0
}

send_command_hex() {
    local cmd_hex="$1"

    RD_HEX=""

    COMMAND_PAYLOAD="$cmd_hex"

    timeout 0.01 dd of=/dev/null bs=4K status=none <&"${SERIAL[0]}"
    if ! active_sender; then
        echo "[error] active_sender завершился с ошибкой (команда $cmd_hex)."
        return 1
    fi
    if ! active_receiver; then
        echo "[error] active_receiver завершился с ошибкой (команда $cmd_hex)."
        return 1
    fi

    if ((${#RD_HEX} == 6)); then
        if is_error 1; then
            return 1
        fi
    fi

    case "${cmd_hex:0:2}" in
    3F) parse_kkt_status_response ;;
    45) parse_kkt_state_code_response ;;
    91)
        case "${cmd_hex:2:2}" in
        16) parse_factory_number_response ;;
        24) parse_control_unit_version_response ;;
        36) parse_ffd_version_response ;;
        esac
        ;;
    9D)
        case "${cmd_hex:2:2}" in
        01) parse_firmware_version_response_01 ;;
        03) parse_firmware_version_response_03 ;;
        91) parse_firmware_version_response_91 ;;
        A0) parse_firmware_version_response_A0 ;;
        esac
        ;;
    A4)
        case "${cmd_hex:2:2}" in
        10) parse_current_shift_params_response ;;
        20) parse_fn_exchange_status_response ;;
        30) parse_fn_status_response ;;
        31) parse_fn_number_response ;;
        32) parse_fn_expiry_response ;;
        33) parse_fn_version_response ;;
        43) parse_fn_fiscalization_result_response ;;
        esac
        ;;
    A5) parse_device_info_response ;;
    *)
        echo ">>> Нет парсера для команды: $cmd_hex"
        ;;
    esac

    return 0
}

main() {
    check_dependencies
    parse_command_line_arguments "$@"

    if [[ -z "$TTY_DEVICE" ]]; then
        auto_detect_and_invoke
    fi

    if [[ -c "$TTY_DEVICE" ]]; then
        local tty_device_basename="$(basename "$TTY_DEVICE")"
        SOCAT_LOGFILE="$ATOL2_FOLDER/socat_${tty_device_basename}_last.log"

        init_serial

        echo '{}' >"$KKT_JSON_FILE"

        local date_time_now="$(date +'%H%M%S_%d%m%y')"

        LOG_OUTPUT_FILE="$ATOL2_FOLDER/${tty_device_basename}_${date_time_now}.log"
        LOG_LAST_OUTPUT_FILE="$ATOL2_FOLDER/${tty_device_basename}_last.log"

        JSON_OUTPUT_FILE="$ATOL2_FOLDER/${tty_device_basename}_${date_time_now}.json"
        JSON_LAST_OUTPUT_FILE="$ATOL2_FOLDER/${tty_device_basename}_last.json"

        if [[ "$COMMAND_PAYLOAD" == "tp" ]]; then
            tp_command
        else
            send_command_hex "$COMMAND_PAYLOAD"
        fi

    else
        echo -e "$TTY_DEVICE не существует или не является символьным устройством."
        exit 1
    fi
}

main "$@"
