#!/bin/bash

source "${BASH_SOURCE%/*}/common.sh"

default_delay="0.3"
function incremental_sleep_delay() {
	sleep "$(jq -n "$default_delay * ([$1, 4] | min | exp2 - 1)")"
}

log_snapshot="$(mktemp)"
filtered_log_snapshot="$(mktemp)"

function filter_log_snapshot() {
	if grep -aF -A100 "$upper_limit_matching_line" "$log_snapshot" >"$filtered_log_snapshot"; then
		cp "$filtered_log_snapshot" "$log_snapshot"
	fi
}

function update_log_snapshot() {
	sleep "$default_delay"
	local terminal_log_cur_mod_time="$(stat -c "%y" "$TERMINAL_LOG")"
	if [[ "$terminal_log_prev_mod_time" != "$terminal_log_cur_mod_time" ]]; then
		terminal_log_prev_mod_time="$terminal_log_cur_mod_time"
		tail -n 100 "$TERMINAL_LOG" | grep -aPiv 'Обнаружен файл оповещения|INFO\s+(?:cpu|ram|swap|temp|disk)\s+' >"$log_snapshot"
		filter_log_snapshot
	fi
}

function pattern_exists_in_log_snapshot() {
	local num_of_last_lines="$1"
	local pattern="$2"
	local pattern_handle_func="$3"

	if matching_line="$(tail -n "$num_of_last_lines" "$log_snapshot" | grep -aiP "$pattern")"; then
		if [[ "$pattern_handle_func" ]]; then
			$pattern_handle_func "$pattern"
		fi
		if [[ "$upper_limit_matching_line" != "$matching_line" ]]; then
			filter_log_snapshot
		fi
		return 0
	else
		return 1
	fi
}

function wait_until_pattern_found() {
	if ((looping_timeout)); then
		local -i loop_beginning_mark_seconds="$(date "+%s")"
	fi
	while true; do
		if pattern_exists_in_log_snapshot "$1" "$2" "$3"; then
			break
		fi
		if ((looping_timeout)) && (("$(date "+%s")" - loop_beginning_mark_seconds > looping_timeout)); then
			red_print "Паттерн '$2' не найден в логе"
			red_print "Превышено максимальное время работы цикла"
			exit 1
		fi
		update_log_snapshot
	done
}

function log_error_details() {
	local message="$1"
	local timestamp="$(date '+%H:%M:%S')"

	echo -ne '\e[91;40m'
	echo -ne "$timestamp Ошибка: $message; Позиция: $i; Штрихкод: $barcode; Количество: $quantity; Цена: $price; Название: $name"
	echo -e '\e[0m'
}

function escape() {
	local -i input_attempt_counter
	if ((looping_timeout)); then
		local -i loop_beginning_mark_seconds="$(date "+%s")"
	fi
	for ((input_attempt_counter = 0; ; input_attempt_counter++)); do
		if pattern_exists_in_log_snapshot 3 "Сброс данных"; then
			break
		fi
		if ((input_attempt_max_count && input_attempt_counter == input_attempt_max_count)); then
			red_print "Превышено максимальное количество попыток \"сброса данных\""
			exit 1
		fi
		if ((looping_timeout)) && (("$(date "+%s")" - loop_beginning_mark_seconds > looping_timeout)); then
			red_print "Не удалось осуществить "Сброс данных" (ESC)"
			red_print "Превышено максимальное время работы цикла"
			exit 1
		fi
		xdotool key --delay 1 Escape Escape
		incremental_sleep_delay "$input_attempt_counter"
		update_log_snapshot
	done
}

function input_customer_email() {
	if [[ -z "$email" ]]; then
		return 0
	fi

	escape
	sleep "$default_delay"
	for ((input_attempt_counter = 0; ; input_attempt_counter++)); do
		xdotool key --delay 1 F11
		incremental_sleep_delay "$input_attempt_counter"
		update_log_snapshot
		if pattern_exists_in_log_snapshot 5 "COMMAND_CUSTOMER_ADDRESS_ADD"; then
			break
		fi
	done
	while true; do
		if pattern_exists_in_log_snapshot 8 "Документ не открыт" "red_print"; then
			return 1
		elif pattern_exists_in_log_snapshot 3 "Активация контекста диалога"; then
			break
		fi
		update_log_snapshot
	done
	sleep "$default_delay"
	echo -n "$email" | xclip -selection "clipboard"
	xdotool key --delay 1 ctrl+v KP_Enter
	update_log_snapshot
	wait_until_pattern_found 5 "Печатать чек?"
	wait_until_pattern_found 3 "Активация контекста диалога"
	sleep "$default_delay"
	xdotool key --delay 1 Escape
	update_log_snapshot
	wait_until_pattern_found 5 "Добавление адреса покупателя завершено"

	return 0
}

function select_currency() {
	if [[ -z "$valut_code" ]]; then
		return 0
	fi

	escape
	sleep "$default_delay"
	xdotool key --delay 1 KP_Enter
	update_log_snapshot
	wait_until_pattern_found 8 "Переход в режим подытога завершен"
	sleep "$default_delay"
	xdotool key --delay 1 Ctrl+F4
	update_log_snapshot
	wait_until_pattern_found 5 "Выберите тип оплаты"
	for ((i = 0; i < $(mysql --skip-column-names -B -e "SELECT * FROM dictionaries.valut" | cut -f1 -d$'\t' | grep -Fn "$valut_code" | cut -f1 -d':') - 1; i++)); do
		sleep "$default_delay"
		xdotool key --delay 1 Down
	done
	sleep "$default_delay"
	xdotool key --delay 1 KP_Enter
	update_log_snapshot
	wait_until_pattern_found 3 "Активация контекста сдачи"

	return 0
}

function type_price_or_quantity() {
	local num="$1"

	if ((${#num} > 5)); then
		xdotool type --delay 1 "${num:0:5}"
		sleep "$delay"
		xdotool type --delay 1 "${num:5:5}"
	else
		xdotool type --delay 1 "$num"
	fi
}

function input_price() {
	local price="$(roundp "$price" 2)"
	if [[ -z "$price" ]]; then
		return 0
	fi

	local -i input_attempt_counter

	while true; do
		sleep "$default_delay"
		for ((input_attempt_counter = 0; ; input_attempt_counter++)); do
			xdotool key --delay 1 F10
			incremental_sleep_delay "$input_attempt_counter"
			update_log_snapshot
			if pattern_exists_in_log_snapshot 5 "COMMAND_PRICE"; then
				break
			fi
		done
		while true; do
			if pattern_exists_in_log_snapshot 5 "Недопустимое значение цены"; then
				escape
				echo "Неудачный ввод цены"
				break
			elif pattern_exists_in_log_snapshot 5 "Ответ: нет данных"; then
				wait_until_pattern_found 5 "Завершили изменять модификатор 'цена'"
				echo "Неудачный ввод цены"
				break
			elif pattern_exists_in_log_snapshot 5 "Ввод цены"; then
				wait_until_pattern_found 3 "Активация контекста диалога"
				sleep "$default_delay"
				type_price_or_quantity "$price"
				sleep "$default_delay"
				xdotool key --delay 1 KP_Enter
			elif pattern_exists_in_log_snapshot 5 "Завершили изменять модификатор 'цена'"; then
				break 2
			fi
			update_log_snapshot
		done
	done

	return 0
}

function input_quantity() {
	local quantity="$(roundp "$quantity" 3)"
	if [[ -z "$quantity" || "$quantity" == "1" || "$excise_type" == "ALCOHOL" || "$excise_type" == "TOBACCO" || "$measure" == "1" ]]; then
		return 0
	fi

	sleep "$default_delay"
	for ((input_attempt_counter = 0; ; input_attempt_counter++)); do
		xdotool key --delay 1 F6
		incremental_sleep_delay "$input_attempt_counter"
		update_log_snapshot
		if pattern_exists_in_log_snapshot 5 "COMMAND_QUANT"; then
			break
		fi
	done
	wait_until_pattern_found 3 "Активация контекста диалога"
	sleep "$default_delay"
	type_price_or_quantity "$quantity"
	sleep "$default_delay"
	xdotool key --delay 1 KP_Enter
	update_log_snapshot
	wait_until_pattern_found 20 "Завершили изменять модификатор 'количество'"

	return 0
}

function input_department_code() {
	if [[ ! "$department" =~ ^[[:digit:]]$ ]]; then
		return 0
	fi

	sleep "$default_delay"
	for ((input_attempt_counter = 0; ; input_attempt_counter++)); do
		xdotool key --delay 1 F7
		incremental_sleep_delay "$input_attempt_counter"
		update_log_snapshot
		if pattern_exists_in_log_snapshot 5 "COMMAND_DEPT"; then
			break
		fi
	done
	wait_until_pattern_found 3 "Активация контекста диалога"
	sleep "$default_delay"
	xdotool type --delay 1 "$department"
	sleep "$default_delay"
	xdotool key --delay 1 KP_Enter
	update_log_snapshot
	wait_until_pattern_found 5 "Завершили изменять модификатор 'отдел'"

	return 0
}

function handle_input_decision() {
	local message="$1"
	if [[ "$message" ]]; then
		log_error_details "$message"
	fi

	if ((stop_on_failure)); then
		return 0
	fi

	if ((skip)); then
		echo "Переход к следующей позиции"
	else
		while true; do
			read -p 'Пропустить позицию s(kip), повтор ввода r(etry): ' decision
			case "$decision" in
			's' | 'skip')
				echo "Переход к следующей позиции"
				break
				;;
			'r' | 'retry')
				echo "Повторная попытка ввода"
				((i--))
				break
				;;
			esac
		done
	fi

	return 0
}

function handle_common_input_errors() {
	local error_handle_func_1="$1"
	local error_handle_func_2="$2"

	for error_string in \
		"Товар со штрих-кодом '.*?' не найден" \
		"Товару не назначена ККМ" \
		"Неверный способ ввода" \
		"Изменение количества для маркированной позиции запрещено" \
		"Количество не может быть дробным" \
		"Не указана минимальная цена товара" \
		"Товар с такой акцизной маркой запрещен к продаже"; do
		if pattern_exists_in_log_snapshot 6 "$error_string" "$error_handle_func_1"; then
			return 0
		fi
	done

	for error_string in \
		"Штрих-код уже зарегистрирован" \
		"Акцизная марка уже была зарегистрирована ранее" \
		"Код маркировки уже был зарегистрирован ранее" \
		"Позиция с таким кодом маркировки уже есть в документе"; do
		if pattern_exists_in_log_snapshot 6 "$error_string" "$error_handle_func_2"; then
			return 0
		fi
	done

	return 1
}

function input_codes() {
	local -i input_attempt_counter

	if [[ -z "$barcode" && ! (-f "$mark" && -r "$mark" && -s "$mark") || ! "$barcode" =~ ^[[:digit:]]+$ ]]; then
		return 1
	fi

	sleep "$default_delay"
	if [[ "$excise_type" != "ALCOHOL" && -s "$mark" ]]; then
		xclip -selection "clipboard" "$mark"
	else
		echo -n "$barcode" | xclip -selection "clipboard"
	fi
	xdotool key --delay 1 ctrl+v KP_Enter
	while true; do
		update_log_snapshot
		if pattern_exists_in_log_snapshot 6 "Товар со штрих-кодом '$barcode' не найден" handle_input_decision; then
			return 1
		elif pattern_exists_in_log_snapshot 6 "Товар со штрих-кодом '.*?' не найден" log_error_details; then
			if [[ "$excise_type" == "ALCOHOL" || ! -s "$mark" ]]; then
				echo "Неудачный ввод штрихкода"
				((i--))
			fi
			return 1
		elif pattern_exists_in_log_snapshot 3 "Ввод данных завершен|Добавление товара завершено"; then
			if [[ "$excise_type" == "ALCOHOL" && -s "$mark" ]]; then
				handle_input_decision "Акцизка/Маркировка не была запрошена"
				return 1
			else
				return 0
			fi
		elif handle_common_input_errors "handle_input_decision" "log_error_details"; then
			return 1
		elif pattern_exists_in_log_snapshot 5 "Сканирование акцизной марки|Сканирование кода маркировки|повторите сканирование"; then
			if [[ -s "$mark" ]]; then
				sleep "$default_delay"
				for ((input_attempt_counter = 0; ; )); do
					if pattern_exists_in_log_snapshot 5 "Сканирование акцизной марки|Сканирование кода маркировки|повторите сканирование"; then
						wait_until_pattern_found 3 "Активация контекста диалог"
						if ((input_attempt_counter == 5)); then
							echo "Акцизка/Маркировка пропущена"
							return 1
						fi
						if ((input_attempt_counter > 0)); then
							echo "Акцизка/Маркировка попытка $((input_attempt_counter + 1))"
						fi
						xdotool type --delay "$(jq -n "[$input_attempt_counter, 3] | min | exp2 * exp2")" --file "$mark"
						((input_attempt_counter++))
					fi
					incremental_sleep_delay "$input_attempt_counter"
					update_log_snapshot
					if pattern_exists_in_log_snapshot 3 "Ввод данных завершен|Добавление товара завершено"; then
						return 0
					elif handle_common_input_errors "handle_input_decision" "log_error_details"; then
						return 1
					fi
				done
			else
				handle_input_decision "Акцизка/Маркировка запрошена, но её нет"
				return 1
			fi
		elif pattern_exists_in_log_snapshot 5 "Ввод цены"; then
			wait_until_pattern_found 3 "Активация контекста диалога"
			local price="$(roundp "$price" 2)"
			if [[ "$price" ]]; then
				sleep "$default_delay"
				type_price_or_quantity "$price"
				sleep "$default_delay"
				xdotool key --delay 1 KP_Enter
			else
				handle_input_decision "Цена запрошена, но её нет"
				return 1
			fi
		fi
	done

	return 0
}
