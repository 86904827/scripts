renice -n -20 $$ &>/dev/null

function print_error() {
    local message="$1"
    echo -ne '\e[91;40m'
    echo "$(date '+%H:%M:%S') : $message"
    echo -ne '\e[0m'
}

echo "DELETE FROM dictionaries.mol WHERE LOWER(name) REGEXP 'кассир|администратор|мастер|инвентаризация|вскрыти';" | mysql dictionaries

if [[ "$1" ]]; then
    username="$1"
    declare -i CLP=0
    if [[ "$2" ]]; then
        if [[ "$2" =~ ^[[:digit:]]+$ ]]; then
            CLP="$2"
        else
            print_error "Код/Логин/Пароль это число(обычно цифра)"
            exit 2
        fi
    elif [[ "$(echo "SELECT name FROM dictionaries.mol WHERE code = '4';" | mysql --skip-column-names dictionaries)" =~ "Мастер" ]]; then
        CLP="4"
    else
        CLP="$(echo "SELECT code,login,password FROM dictionaries.mol;" | mysql --skip-column-names dictionaries | tr '\t' '\n' | sort -g | tail -n 1)"
        ((CLP++))
    fi
    if ((CLP < 4)); then
        CLP=4;
    fi
    echo "REPLACE INTO dictionaries.mol1      (code, login, password, name, locked) VALUES ('$CLP', '$CLP', '$CLP', '$username', '0');" | mysql dictionaries
    echo "REPLACE INTO dictionaries.mol2      (code, login, password, name, locked) VALUES ('$CLP', '$CLP', '$CLP', '$username', '0');" | mysql dictionaries
    echo "REPLACE INTO dictionaries.roleuser1 (id, usercode, rolecode, rule)        VALUES ('$CLP', '$CLP', '4', '1');"                 | mysql dictionaries
    echo "REPLACE INTO dictionaries.roleuser2 (id, usercode, rolecode, rule)        VALUES ('$CLP', '$CLP', '4', '1');"                 | mysql dictionaries
    echo -ne '\e[32;40m'
    echo "Добавлен пользователь с кодом/логином/паролем $CLP и ФИО $username"
    echo -ne '\e[0m'
else
    mapfile -t mol_data < <(echo "SELECT code,login,password,name FROM dictionaries.mol;" | mysql --skip-column-names dictionaries)
    tabs 1,20,+30
    echo -e "Код/Логин/Пароль\tРоль\tФИО"
    for ((i = 0; i < ${#mol_data[*]}; i++)); do
        row="${mol_data[i]}"
        usercode=$(echo "$row" | cut -d $'\t' -f 1)
        echo -n "$row" | cut -d $'\t' -f -3 | tr '\t' '/' | tr -d '\n'
        echo -ne "\t"
        echo -n "$(echo "SELECT rolename FROM dictionaries.role WHERE rolecode='$(echo "SELECT rolecode FROM dictionaries.roleuser WHERE usercode='$usercode';" | mysql --skip-column-names dictionaries)';" | mysql --skip-column-names dictionaries)"
        echo -ne "\t"
        echo "$row" | cut -d $'\t' -f 4
    done
    tabs -8
fi