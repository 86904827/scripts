#!/bin/bash

source "${BASH_SOURCE%/*}/common_input_funcs.sh"

if [[ "$1" && "$2" ]]; then
	if [[ -f "$1" && -r "$1" && -s "$1" ]]; then
		MARKS_FILE="$1"

		barcode="$2"
		price="$3"
		department="$4"

		mark="$(mktemp)"

		for ((i = 1; i <= $(wc -l "$MARKS_FILE" | cut -f 1 -d' '); i++)); do
			sed -n "${i}p" "$MARKS_FILE" | tr -d '\n' >"$mark"

			sleep "$default_delay"
			escape
			input_price
			input_department_code
			input_codes
		done

		echo "Ввод акцизных марок из файла $1 завершен"
	else
		echo "Не удалось прочитать файл $1"
	fi
else
	echo "Использование: fastrobot.sh файл_акцизок штрихкод [цена] [отдел]"
fi
