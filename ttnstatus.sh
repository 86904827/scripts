#!/bin/bash

source "${BASH_SOURCE%/*}/common.sh"

GLOBAL_SCRIPT_TIMEOUT_SECS="5m"

timeout_kill() {
	echo -e "Сработал таймаут. Завершаем скрипт..."
	kill "$$" &>/dev/null
}

sleep "$GLOBAL_SCRIPT_TIMEOUT_SECS" && timeout_kill &
disown
GLOBAL_SCRIPT_TIMEOUT_PID=$!

cleanup_on_exit() {
	kill "$GLOBAL_SCRIPT_TIMEOUT_PID" &>/dev/null

	[[ "$TMPDIR" && -d "$TMPDIR" ]] && rm -rf "$TMPDIR"
}

trap cleanup_on_exit EXIT

WAYBILL_OPTIMIZED="$TMPDIR/WAYBILL_OPTIMIZED"
FORM2REGINFO_OPTIMIZED="$TMPDIR/FORM2REGINFO_OPTIMIZED"
TICKET_OPTIMIZED="$TMPDIR/TICKET_OPTIMIZED"

POSITIONS_OUTPUT_INFO="$TMPDIR/POSITIONS_OUTPUT_INFO"
INPUT_DATA_TEMP="$TMPDIR/INPUT_DATA_TEMP"

function print_ttn_table() {
	local TTNLOAD_DIR="$1"
	local TTN_DIR="$TTNLOAD_DIR/TTN"

	if [[ ! -d "$TTN_DIR" ]]; then
		return
	fi

	TTN_PATHS="$(find -O3 "$TTN_DIR" -mindepth 1 -maxdepth 1 -type d -mtime -90 -regextype posix-egrep -regex ".*/TTN-[[:digit:]]{10}" | sort -g | tail -n 15)"
	if [[ "$TTN_PATHS" ]]; then
		echo -ne "${FG_BRIGHT_CYAN}"
		echo "$(basename "$TTNLOAD_DIR"):"
		echo -ne "${FG_CYAN}"
		echo -ne " Номер ТТН\tНомер\tДата\tСумма\tПоставщик\tTicket.xml"
		echo -e "${NC}"
		for TTN_PATH in $TTN_PATHS; do
			TTN="$(basename "$TTN_PATH")"

			WAYBILL_PATH="$TTN_PATH/WayBill_v4.xml"
			! xmllint --nowarning --noout "$WAYBILL_PATH" &>/dev/null
			WAYBILL_OK="$?"
			if ((WAYBILL_OK)); then
				parse_waybill "$WAYBILL_PATH" "$WAYBILL_OPTIMIZED"
			fi

			TICKET_PATH="$TTN_PATH/Ticket.xml"
			! xmllint --nowarning --noout "$TICKET_PATH" &>/dev/null
			TICKET_OK="$?"
			if ((TICKET_OK)); then
				parse_ticket "$TICKET_PATH" "$TICKET_OPTIMIZED"
			fi

			echo -ne "${FG_BRIGHT_BLUE}"
			echo -ne " $(echo "$TTN" | cut -c '5-')\t"
			echo -ne "${FG_CYAN}"
			if ((WAYBILL_OK)); then
				echo -ne "$WAYBILL_INTERNAL_NUMBER\t"
				echo -ne "${FG_BRIGHT_BLUE}"
				echo -ne "$WAYBILL_DATE\t"
				echo -ne "${FG_CYAN}"
				echo -ne "$WAYBILL_SUM\t"
				echo -ne "${FG_BRIGHT_BLUE}"
				echo -ne "$WAYBILL_SHIPPER_NAME\t"
			else
				echo -ne "-\t-\t-\t-\t"
			fi
			if ((TICKET_OK)); then
				echo -ne "${FG_BRIGHT_BLUE}"
				if [[ "$TICKET_OPERATIONRESULT_OPERATIONCOMMENT" ]]; then
					echo -ne "$TICKET_OPERATIONRESULT_OPERATIONCOMMENT"
				elif [[ "$TICKET_RESULT_COMMENTS" ]]; then
					echo -ne "$TICKET_RESULT_COMMENTS"
				fi
			else
				echo -ne "${FG_CYAN}"
				echo -ne "-"
			fi
			echo -e "${NC}"
		done
	fi
}

if [[ "$1" ]]; then
	if [[ "$1" == "l" || "$1" == "-l" ]]; then
		TTN="$(basename "$(find "$TTN1_DIR" -mindepth 1 -maxdepth 1 -mtime -90 -regextype posix-egrep -regex ".*/TTN-[[:digit:]]{10}" | sort -g | tail -n 1)")"
	else
		TTN="TTN-$(echo "$1" | grep -ahoP '\d{10}')"
	fi

	for TTN_DIR in "$TTN1_DIR" "$TTN2_DIR" "$TTN_EXT_DIR"; do
		WAYBILL_PATH="$TTN_DIR/$TTN/WayBill_v4.xml"
		! xmllint --nowarning --noout "$WAYBILL_PATH" &>/dev/null
		WAYBILL_OK="$?"
		if ((WAYBILL_OK)); then
			break
		fi
	done
	if ((!WAYBILL_OK)); then
		red_print "Накладная $TTN не найдена или пустая или повреждена или еще не загружена с УТМ"
		exit 2
	fi
	parse_waybill "$WAYBILL_PATH" "$WAYBILL_OPTIMIZED"
	parse_waybill_positions "$WAYBILL_OPTIMIZED"

	FORM2REGINFO_PATH="$TTN_DIR/$TTN/FORM2REGINFO.xml"
	! xmllint --nowarning --noout "$FORM2REGINFO_PATH" &>/dev/null
	FORM2REGINFO_OK="$?"
	if ((FORM2REGINFO_OK)); then
		parse_form2reginfo "$FORM2REGINFO_PATH" "$FORM2REGINFO_OPTIMIZED"
		parse_form2reginfo_positions "$FORM2REGINFO_OPTIMIZED"
	fi

	TICKET_PATH="$TTN_DIR/$TTN/Ticket.xml"
	! xmllint --nowarning --noout "$TICKET_PATH" &>/dev/null
	TICKET_OK="$?"
	if ((TICKET_OK)); then
		parse_ticket "$TICKET_PATH" "$TICKET_OPTIMIZED"
	fi

	if [[ "$2" == "d" || "$2" == "d_s" ]]; then
		if ((FORM2REGINFO_OK)); then
			mkdir -p /root/waybillacts
			waybillact_differences="/root/waybillacts/WayBillAct_Differences_$(date '+%s').xml"
			echo "$waybillact_differences"
			echo '<?xml version="1.0" encoding="utf-8"?>
<ns:Documents xmlns:pref="http://fsrar.ru/WEGAIS/ProductRef" xmlns:c="http://fsrar.ru/WEGAIS/Common" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ce="http://fsrar.ru/WEGAIS/CommonV3" xmlns:oref="http://fsrar.ru/WEGAIS/ClientRef" xmlns:wa="http://fsrar.ru/WEGAIS/ActTTNSingle_v4" Version="1.0" xmlns:ns="http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01">'"
	<ns:Owner>
		<ns:FSRAR_ID>$WAYBILL_CONSIGNEE_FSRAR_ID</ns:FSRAR_ID>
	</ns:Owner>
	<ns:Document>
		<ns:WayBillAct_v4>
			<wa:Header>
				<wa:IsAccept>Differences</wa:IsAccept>
				<wa:ACTNUMBER>$TTN</wa:ACTNUMBER>
				<wa:ActDate>$(date -Idate)</wa:ActDate>
				<wa:WBRegId>$TTN</wa:WBRegId>
				<wa:Note />
			</wa:Header>
			<wa:Content>" >>"$waybillact_differences"
		else
			red_print "Что-то не так с $FORM2REGINFO_PATH"
			exit 2
		fi
	elif [[ "$2" == "w" ]]; then
		echo "REPLACE INTO excisemarkwhite (excisemarkid) VALUES $(xmlstarlet sel -t -v '/*/*/WayBill_v4/Content/Position/InformF2/MarkInfo/boxpos/amclist/amc' "$WAYBILL_OPTIMIZED" | sed "s/^/(\'/;s/$/\'),/g" | tr -d '\n' | head -c -1);" | mysql dictionaries
		echo -ne "${FG_GREEN}"
		echo -n "Все акцизные марки из накладной добавлены в белый список"
		echo -e "${NC}"
		exit 0
	fi

	for cashlog_dir in $(find "$CASHLOGS_DIR" -mindepth 1 -maxdepth 1 -mtime -7 | sort -g); do
		grep -ahF 'Ввод данных' "$cashlog_dir/terminal.log" >>"$INPUT_DATA_TEMP"
	done
	grep -ahF 'Ввод данных' "$TERMINAL_LOG" >>"$INPUT_DATA_TEMP"
	sort -u -o "$INPUT_DATA_TEMP" "$INPUT_DATA_TEMP"

	declare -i num_scanned_excise_marks="$WAYBILL_NUM_EXCISE_MARKS" num_scanned_box_numbers=0
	for ((i = 1; i <= WAYBILL_NUM_POSITIONS; i++)); do
		if [[ "$2" == "d" || "$2" == "d_s" ]]; then
			echo "				<wa:Position>
					<wa:Identity>${WAYBILL_POSITION_IDENTITY[i]}</wa:Identity>
					<wa:InformF2RegId>${FORM2REGINFO_POSITION_INFORMF2REGID[WAYBILL_POSITION_IDENTITY[i]]}</wa:InformF2RegId>" >>"$waybillact_differences"
		fi
		position_status=""
		if [[ "$2" == "d_s" ]]; then
			position_status="allowed"
			for identity in ${@:3}; do
				if [[ "$identity" == "${WAYBILL_POSITION_IDENTITY[i]}" ]]; then
					position_status="banned"
					break
				fi
			done
		fi
		ABSENT_MARKS=
		declare -i num_scanned_excise_marks_in_position="${WAYBILL_POSITION_NUM_EXCISE_MARKS[i]}"
		unset NUM_SCANNED_EXCISE_MARKS_IN_BOX WAYBILL_POSITION_BOXPOS_BOXNUMBER WAYBILL_POSITION_BOXPOS_NUM_EXCISE_MARKS
		for ((k = 1; k <= WAYBILL_POSITION_NUM_BOXES[i]; k++)); do
			source <(xmlstarlet sel -t \
				-o "WAYBILL_POSITION_BOXPOS_BOXNUMBER[k]='" -v '/*/*/WayBill_v4/Content/Position'"[$i]"'/InformF2/MarkInfo/boxpos'"[$k]"'/boxnumber' -o "'" -n \
				-o "WAYBILL_POSITION_BOXPOS_NUM_EXCISE_MARKS[k]='" -v 'count(/*/*/WayBill_v4/Content/Position'"[$i]"'/InformF2/MarkInfo/boxpos'"[$k]"'/amclist/amc)' -o "'" -n "$WAYBILL_OPTIMIZED")
			NUM_SCANNED_EXCISE_MARKS_IN_BOX[k]="${WAYBILL_POSITION_BOXPOS_NUM_EXCISE_MARKS[k]}"
			if [[ "$position_status" == "" && "${WAYBILL_POSITION_BOXPOS_BOXNUMBER[k]}" ]] && grep -ahF "${WAYBILL_POSITION_BOXPOS_BOXNUMBER[k]}" "$INPUT_DATA_TEMP" &>/dev/null; then
				((num_scanned_box_numbers++))
			else
				for ((j = 1; j <= WAYBILL_POSITION_BOXPOS_NUM_EXCISE_MARKS[k]; j++)); do
					source <(xmlstarlet sel -t \
						-o "EXCISE_MARK='" -v '/*/*/WayBill_v4/Content/Position'"[$i]"'/InformF2/MarkInfo/boxpos'"[$k]"'/amclist/amc'"[$j]" -o "'" -n "$WAYBILL_OPTIMIZED")
					if [[ "$position_status" == "banned" ]] || ([[ "$position_status" == "" ]] && ! grep -aFl "$EXCISE_MARK" "$INPUT_DATA_TEMP" &>/dev/null); then
						ABSENT_MARKS+="$EXCISE_MARK "
						((NUM_SCANNED_EXCISE_MARKS_IN_BOX[k]--))
						((num_scanned_excise_marks_in_position--))
						((num_scanned_excise_marks--))
					fi
				done
			fi
		done
		if ((num_scanned_excise_marks_in_position == WAYBILL_POSITION_NUM_EXCISE_MARKS[i])); then
			echo -ne "${FG_GREEN}" >>"$POSITIONS_OUTPUT_INFO"
		else
			echo -ne "${FG_BRIGHT_RED}" >>"$POSITIONS_OUTPUT_INFO"
		fi
		echo -n "${WAYBILL_POSITION_IDENTITY[i]}" >>"$POSITIONS_OUTPUT_INFO"
		echo -ne '\t' >>"$POSITIONS_OUTPUT_INFO"
		echo -n "${WAYBILL_POSITION_EAN13[i]}" >>"$POSITIONS_OUTPUT_INFO"
		echo -ne '\t' >>"$POSITIONS_OUTPUT_INFO"
		if [[ "${FORM2REGINFO_POSITION_BOTTLINGDATE[i]}" ]]; then
			echo -n "$(date --date="${FORM2REGINFO_POSITION_BOTTLINGDATE[i]}" "+%d%m%y")" >>"$POSITIONS_OUTPUT_INFO"
		fi
		echo -ne '\t' >>"$POSITIONS_OUTPUT_INFO"
		echo -n "$(roundp "${WAYBILL_POSITION_PRODUCT_CAPACITY[i]}")" >>"$POSITIONS_OUTPUT_INFO"
		echo -ne '\t' >>"$POSITIONS_OUTPUT_INFO"
		if ((NUM_SCANNED_EXCISE_MARKS_IN_BOX[1] == WAYBILL_POSITION_BOXPOS_NUM_EXCISE_MARKS[1])); then
			echo -ne "${FG_GREEN}" >>"$POSITIONS_OUTPUT_INFO"
		else
			echo -ne "${FG_BRIGHT_RED}" >>"$POSITIONS_OUTPUT_INFO"
		fi
		echo -n "${NUM_SCANNED_EXCISE_MARKS_IN_BOX[1]}/${WAYBILL_POSITION_BOXPOS_NUM_EXCISE_MARKS[1]}" >>"$POSITIONS_OUTPUT_INFO"
		echo -ne '\t' >>"$POSITIONS_OUTPUT_INFO"
		if ((num_scanned_excise_marks_in_position == WAYBILL_POSITION_NUM_EXCISE_MARKS[i])); then
			echo -ne "${FG_GREEN}" >>"$POSITIONS_OUTPUT_INFO"
		else
			echo -ne "${FG_BRIGHT_RED}" >>"$POSITIONS_OUTPUT_INFO"
		fi
		echo -n "${WAYBILL_POSITION_QUANTITY[i]}" >>"$POSITIONS_OUTPUT_INFO"
		echo -ne '\t' >>"$POSITIONS_OUTPUT_INFO"
		echo -n "${WAYBILL_POSITION_PRODUCT_FULLNAME[i]}" >>"$POSITIONS_OUTPUT_INFO"
		echo >>"$POSITIONS_OUTPUT_INFO"
		for ((k = 2; k <= WAYBILL_POSITION_NUM_BOXES[i]; k++)); do
			echo -ne '\t\t\t\t' >>"$POSITIONS_OUTPUT_INFO"
			if ((NUM_SCANNED_EXCISE_MARKS_IN_BOX[k] == WAYBILL_POSITION_BOXPOS_NUM_EXCISE_MARKS[k])); then
				echo -ne "${FG_GREEN}" >>"$POSITIONS_OUTPUT_INFO"
			else
				echo -ne "${FG_BRIGHT_RED}" >>"$POSITIONS_OUTPUT_INFO"
			fi
			echo -n "${NUM_SCANNED_EXCISE_MARKS_IN_BOX[k]}/${WAYBILL_POSITION_BOXPOS_NUM_EXCISE_MARKS[k]}" >>"$POSITIONS_OUTPUT_INFO"
			echo -e '\t\t' >>"$POSITIONS_OUTPUT_INFO"
		done
		if [[ "$2" == "d" || "$2" == "d_s" ]]; then
			echo "					<wa:RealQuantity>$num_scanned_excise_marks_in_position</wa:RealQuantity>" >>"$waybillact_differences"
			if [[ "$ABSENT_MARKS" ]]; then
				echo "					<wa:MarkInfo>" >>"$waybillact_differences"
				for exciseMark in $ABSENT_MARKS; do
					echo "						<ce:amc>$exciseMark</ce:amc>" >>"$waybillact_differences"
				done
				echo "					</wa:MarkInfo>" >>"$waybillact_differences"
			fi
			echo "				</wa:Position>" >>"$waybillact_differences"
		fi
	done
	if [[ "$2" == "d" || "$2" == "d_s" ]]; then
		echo "			</wa:Content>
		</ns:WayBillAct_v4>
	</ns:Document>
</ns:Documents>" >>"$waybillact_differences"
	fi

	if [[ -t 1 ]]; then
		tabs 1,+17,+25,+28

		echo -ne "${FG_BRIGHT_BLUE}"
		echo -n "Накладная"
		echo -ne '\t'
		echo -n "$(echo "$TTN" | cut -c '5-') / "
		echo -ne "${FG_CYAN}"
		echo "$WAYBILL_INTERNAL_NUMBER"

		echo -ne "${FG_BRIGHT_BLUE}"
		echo -n "Дата"
		echo -ne '\t'
		echo "$WAYBILL_DATE"

		echo -ne "${FG_CYAN}"
		echo -n "Сумма"
		echo -ne '\t'
		echo "$WAYBILL_SUM"

		echo -ne "${FG_BRIGHT_BLUE}"
		echo -n "Грузоотправитель"
		echo -ne '\t\e[36m'
		echo -n "$WAYBILL_SHIPPER_INN\\$WAYBILL_SHIPPER_KPP"
		echo -ne '\t'
		echo -n "$WAYBILL_SHIPPER_INN\\$WAYBILL_SHIPPER_FSRAR_ID"
		echo -ne '\t'
		echo -ne "${FG_BRIGHT_BLUE}"
		echo "$WAYBILL_SHIPPER_NAME"

		echo -ne "${FG_BRIGHT_BLUE}"
		echo -n "Грузополучатель"
		echo -ne '\t'
		echo -ne "${FG_CYAN}"
		echo -n "$WAYBILL_CONSIGNEE_INN\\$WAYBILL_CONSIGNEE_KPP"
		echo -ne '\t'
		echo -n "$WAYBILL_CONSIGNEE_INN\\$WAYBILL_CONSIGNEE_FSRAR_ID"
		echo -ne '\t'
		echo -ne "${FG_BRIGHT_BLUE}"
		echo "$WAYBILL_CONSIGNEE_NAME"

		echo -ne "${FG_CYAN}"
		echo -n "Адрес поставки"
		echo -ne '\t'
		echo "$WAYBILL_CONSIGNEE_ADDRESS"

		echo -n "Ящики"
		echo -ne '\t'
		echo "$num_scanned_box_numbers/$WAYBILL_NUM_BOXES"
		if ((num_scanned_excise_marks < WAYBILL_NUM_EXCISE_MARKS)); then
			echo -ne "${FG_BRIGHT_RED}"
		else
			echo -ne "${FG_GREEN}"
		fi
		echo -e "Акцизки\t$num_scanned_excise_marks/$WAYBILL_NUM_EXCISE_MARKS"

		if ((TICKET_OK)); then
			echo -ne "${FG_BRIGHT_BLUE}"
			echo -n "Ticket"
			echo -ne '\t'
			if [[ "$TICKET_OPERATIONRESULT_OPERATIONCOMMENT" ]]; then
				echo "$TICKET_OPERATIONRESULT_OPERATIONDATE / $TICKET_OPERATIONRESULT_OPERATIONRESULT / $TICKET_OPERATIONRESULT_OPERATIONCOMMENT"
			elif [[ "$TICKET_RESULT_COMMENTS" ]]; then
				echo "$TICKET_RESULT_CONCLUSIONDATE / $TICKET_RESULT_CONCLUSION / $TICKET_RESULT_COMMENTS"
			fi
		fi

		if [[ -s "$POSITIONS_OUTPUT_INFO" ]]; then
			tabs 1,+3,+14,+7,+6,+6,+4

			echo -ne "${FG_CYAN}"
			echo -e '№\tШтрихкод\tРозлив\tОбъём\tАкциз\tОбщ\tНазвание'
			cat "$POSITIONS_OUTPUT_INFO"
		fi

		echo -ne "${NC}"

		tabs -8
	fi

	if [[ "$2" == "d" || "$2" == "d_s" ]]; then
		if [[ -t 1 ]]; then
			read -p 'Отправить акт расхождения?(да,нет): ' send_act
			if [[ "$send_act" == "да" ]]; then
				if curl -s -F "xml_file=@$waybillact_differences" "http://localhost:8082/opt/in/WayBillAct_v4" &>/dev/null; then
					rsync --quiet --perms --chmod=755 --checksum "$waybillact_differences" "$TTN_DIR/$TTN/WayBillAct_v3_$(md5sum "$waybillact_differences" | cut -f1 -d' ').xml"
					rsync --quiet --perms --chmod=755 --checksum "$waybillact_differences" "$TTN_DIR/$TTN/WayBillAct_v3.xml"
					echo -ne "${FG_GREEN}"
					echo -n "Акт расхождения направлен"
					echo -e "${NC}"
				else
					red_print "Не удалось отправить акт расхождения"
					exit 2
				fi
			else
				red_print "Отправка не подтверждена"
			fi
		else
			red_print "Отправка не подтверждена"
		fi
	fi

	if ((num_scanned_excise_marks < WAYBILL_NUM_EXCISE_MARKS)); then
		exit 1
	else
		exit 0
	fi
else
	tabs 1,+13,+17,+12,+12,+42

	print_ttn_table "$TTNLOAD_EXT_DIR"
	echo
	print_ttn_table "$TTNLOAD2_DIR"
	echo
	print_ttn_table "$TTNLOAD1_DIR"

	tabs -8
fi
