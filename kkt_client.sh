#!/bin/bash

source "${BASH_SOURCE%/*}/common.sh"

STX=$'\x02'
ETX=$'\x03'
ENQ=$'\x05'
FS=$'\x1C'

SERIAL_BAUDRATE="57600"
DEVICE_PASSWORD="PIRI"
TIMEOUT_SECS=3

PACKET_ID=$((16#20))

COMMAND_RESPONSE_FILE="$TMPDIR/COMMAND_RESPONSE_FILE"
INPUT_FILE="$TMPDIR/INPUT_FILE"
DATA_FIELDS="$TMPDIR/DATA_FIELDS"
COMMAND_PACKET="$TMPDIR/COMMAND_PACKET"
PROPERTIES_FILE="$TMPDIR/PROPERTIES_FILE"
SOCAT_TEMP_LOGFILE="$TMPDIR/SOCAT_TEMP_LOGFILE"
LOG_TEMP_FILE="$TMPDIR/LOG_TEMP_FILE"
JSON_FILTER_FILE="$TMPDIR/JSON_FILTER_FILE"

exec > >(tee -a "$LOG_TEMP_FILE")

COMPROXY_JAVA='^/opt/comproxy/jre/bin/java'
COMPROXY_SOCAT_TTYS91='^.*/socat.+(/dev/ttyS90.+/dev/ttyS91|/dev/ttyS91.+/dev/ttyS90)'
COMPROXY_SOCAT_TTYS101='^.*/socat.+(/dev/ttyS100.+/dev/ttyS101|/dev/ttyS101.+/dev/ttyS100)'

COMPROXY_INI='/opt/comproxy/ComProxy.ini'
COMPROXY_PHYSICAL_PORT="$(grep -Po '^physical_port=\K[\w/]+' "$COMPROXY_INI")"
COMPROXY_PHYSICAL_PORT2="$(grep -Po '^physical_port2=\K[\w/]+' "$COMPROXY_INI")"
COMPROXY_VIRTUAL_PORT="$(grep -Po '^virtual_port=\K[\w/]+' "$COMPROXY_INI" || echo '/dev/ttyS90')"
COMPROXY_VIRTUAL_PORT2="$(grep -Po '^virtual_port2=\K[\w/]+' "$COMPROXY_INI" || echo '/dev/ttyS100')"
COMPROXY_VIRTUAL_PORT_OUT="$(grep -Po '^virtual_port_out=\K[\w/]+' "$COMPROXY_INI" || echo '/dev/ttyS91')"
COMPROXY_VIRTUAL_PORT_OUT2="$(grep -Po '^virtual_port_out2=\K[\w/]+' "$COMPROXY_INI" || echo '/dev/ttyS101')"

KKT_CLIENT_FOLDER="/linuxcash/logs/current/kkt_client"

mkdir "$KKT_CLIENT_FOLDER" &>/dev/null

cleanup_on_exit() {
    exit_status="$?"
    ((should_handle_comproxy)) && continue_competing_comproxy
    kill "$SCRIPT_TIMEOUT_PID" &>/dev/null
    if [[ "$SOCAT_LOGFILE" ]]; then
        cp "$SOCAT_TEMP_LOGFILE" "$SOCAT_LOGFILE"
    fi
    if [[ "$LOG_OUTPUT_FILE" && "$LOG_LAST_OUTPUT_FILE" ]]; then
        cp "$LOG_TEMP_FILE" "$LOG_OUTPUT_FILE"
        cp "$LOG_TEMP_FILE" "$LOG_LAST_OUTPUT_FILE"
    fi
    if [[ "$JSON_OUTPUT_FILE" && "$JSON_LAST_OUTPUT_FILE" ]] && (("${#JU[@]}" > 0)); then
        echo -n '.' >"$JSON_FILTER_FILE"
        for ((i = 0; i < "${#JU[@]}"; i += 2)); do
            local value="${JU[i + 1]}"
            if [[ "${value::1}" != "[" ]]; then
                value="\"${value//\"/\\\"}\""
            fi
            echo -n " | setpath([\"${JU[i]//./\",\"}\"]; ${value//\\\\\"/\\\\\\\"})" >>"$JSON_FILTER_FILE"
        done
        jq -n -f "$JSON_FILTER_FILE" >"$JSON_OUTPUT_FILE"
        cp "$JSON_OUTPUT_FILE" "$JSON_LAST_OUTPUT_FILE"

        if [[ "$COMMAND_CODE_ARGUMENT" == "all" && "$INN" ]]; then
            local target_dir="/linuxcash/net/server/server/kkt/viki/$(hostname)"

            mkdir "$target_dir" &>/dev/null
            if [[ -d "$target_dir" ]]; then
                if [[ "${#INN}" == 12 ]]; then
                    cp "$JSON_OUTPUT_FILE" "/linuxcash/logs/current/kkt_client_fr2.json"
                    rsync --timeout 5 -hhhiiv --stats "$JSON_OUTPUT_FILE" "$target_dir/kkt_client_fr2.json" >>"$LOG_OUTPUT_FILE" 2>&1
                elif [[ "${#INN}" == 10 ]]; then
                    cp "$JSON_OUTPUT_FILE" "/linuxcash/logs/current/kkt_client_fr1.json"
                    rsync --timeout 5 -hhhiiv --stats "$JSON_OUTPUT_FILE" "$target_dir/kkt_client_fr1.json" >>"$LOG_OUTPUT_FILE" 2>&1
                fi
            fi
        fi
    fi
    rm -rf -- "$TMPDIR"
}

trap cleanup_on_exit EXIT

SCRIPT_TIMEOUT_SECONDS=30

terminate_on_timeout() {
    sleep "$SCRIPT_TIMEOUT_SECONDS"
    echo "$SCRIPT_TIMEOUT_SECONDS секунд истекло. Терминирование скрипта."
    kill "$$" &>/dev/null
}

terminate_on_timeout &
disown
SCRIPT_TIMEOUT_PID="$!"

stop_competing_comproxy() {
    pkill -f -STOP "$COMPROXY_SOCAT_TTYS91"
    pkill -f -STOP "$COMPROXY_SOCAT_TTYS101"
    pkill -f -STOP "$COMPROXY_JAVA"
}

continue_competing_comproxy() {
    pkill -f -CONT "$COMPROXY_SOCAT_TTYS91"
    pkill -f -CONT "$COMPROXY_SOCAT_TTYS101"
    pkill -f -CONT "$COMPROXY_JAVA"
}

is_port_exclusively_used_by_comproxy() {
    local port="$1"
    local pids
    pids=$(lsof -t "$port" 2>/dev/null)
    if [[ -z "$pids" ]]; then
        return 0
    fi
    for pid in $pids; do
        local cmdline
        cmdline=$(ps -p "$pid" -o args=)
        if [[ ! "$cmdline" =~ $COMPROXY_JAVA ]]; then
            echo "$device используется другим процессом:"
            echo "  $cmdline"
            return 1
        fi
    done
    return 0
}

display_usage() {
    cat <<EOF
Использование: $(basename "$0") [ОПЦИИ] КОМАНДА [ПАРАМЕТРЫ]

ОПЦИИ:
  --ignore-modification       Игнорировать изменения в файле /linuxcash/logs/current/frdriver.log
  --help                      Показать эту справку и выйти
  --device УСТРОЙСТВО         Указать устройство (например, /dev/ttyS0)
  --no-pause-comproxy         Отключить паузу для comproxy

КОМАНДЫ:
  ping                        Проверить связь с устройством
  all                         Показать полную информацию о ККТ
  rel                         Показать релевантную информацию о ККТ
  net                         Показать сетевые настройки
  sta                         Показать статус ККТ
  pri                         Показать статус принтера
  tax                         Показать настройки налогов
  fn                          Показать информацию о ФН
  org                         Показать информацию об организации
  act                         Показать активированные услуги
  ser                         Показать сервисную информацию
  ofd                         Показать статус ОФД
  cou                         Показать сменные счетчики
  csi                         Показать данные отчета cash_info
  rsi                         Показать данные отчета register
  list                        Список доступных устройств
  tp                          Всё, что нужно для технической поддержки
  correction                  Открыть чек коррекции
  stop-com                    Остановить comproxy
  cont-com                    Продолжить работу comproxy
  print_service_data          Печать сервисных данных
  print_last_shift_report     Печать отчета о последней смене
  print_registration_report   Печать отчета о регистрации

ПРИМЕРЫ ИСПОЛЬЗОВАНИЯ:
  1. Проверка связи с устройством:
       $(basename "$0") --device /dev/ttyS0 ping

  2. Получение полной информации о ККТ:
       $(basename "$0") --device /dev/ttyS0 all

  3. Показать сетевые настройки:
       $(basename "$0") --device /dev/ttyS0 net

  4. Показать всё, что нужно для технической поддержки:
       $(basename "$0") --device /dev/ttyS0 tp

  5. Запустить скрипт без паузы для comproxy:
       $(basename "$0") --device /dev/ttyS0 --no-pause-comproxy rel

  6. Печать отчета о последней смене:
       $(basename "$0") --device /dev/ttyS0 print_last_shift_report

  7. Показать список доступных устройств:
       $(basename "$0") list

  8. Получить помощь:
       $(basename "$0") --help


EOF
    exit 0
}

parse_command_line_arguments() {
    if ! TEMP=$(getopt --name "$0" \
        --longoptions 'ignore-modification,help,device:,no-pause-comproxy' \
        --options '' -- "$@"); then
        display_usage
    fi
    eval set -- "$TEMP"

    while true; do
        case "$1" in
        --ignore-modification)
            ignore_modification=1
            shift
            ;;
        --help)
            display_usage
            ;;
        --device)
            TTY_DEVICE="$2"
            shift 2
            ;;
        --no-pause-comproxy)
            should_handle_comproxy=0
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Неизвестная опция: $1"
            display_usage
            ;;
        esac
    done

    COMMAND_CODE_ARGUMENT="$1"
    COMMAND_CODE="$1"
    shift
    COMMAND_PARAMS=("$@")
}

check_dependencies() {
    local dependencies=(socat xxd timeout iconv stdbuf hexdump jq sponge)
    for tool in "${dependencies[@]}"; do
        if ! command -v "$tool" &>/dev/null; then
            echo -e "Ошибка: '$tool' не установлен!"
            exit 1
        fi
    done
}

calculate_crc() {
    printf '%02X' "$(($(hexdump -v -e '1/1 "%u ^ "' "$1" | head -c -3)))"
}

initialize_socat() {
    coproc SOCAT {
        stdbuf -o0 socat -d -d -v -x \
            OPEN:"$TTY_DEVICE",raw,echo=0,cs8,b"$SERIAL_BAUDRATE",parenb=0,cstopb=0,ixon=0,ixoff=0,crtscts=0,ignoreeof \
            STDIO 2>"$SOCAT_TEMP_LOGFILE"
    }
    if ! kill -0 "${SOCAT_PID}" 2>/dev/null; then
        echo -e "Ошибка: socat не стартовал."
        exit 1
    fi
}

send_command_packet() {
    cat "$COMMAND_PACKET" >&"${SOCAT[1]}"
}

# Определение ассоциативного массива для кодов ошибок
declare -A ERROR_CODES=(
    ["00"]="Команда выполнена без ошибок"
    # Ошибки выполнения команд
    ["01"]="Функция невыполнима при данном статусе ККТ"
    ["02"]="В команде указан неверный номер функции"
    ["03"]="Некорректный формат или параметр команды"
    # Ошибки протокола передачи данных
    ["04"]="Переполнение буфера коммуникационного порта"
    ["05"]="Таймаут при передаче байта информации"
    ["06"]="В протоколе указан неверный пароль"
    ["07"]="Ошибка контрольной суммы в команде"
    # Ошибки печатающего устройства
    ["08"]="Конец бумаги"
    ["09"]="Принтер не готов"
    # Ошибки даты/времени
    ["0A"]="Текущая смена больше 24 часов. Установка даты времени больше чем на 24 часа."
    ["0B"]="Разница во времени, ККТ и указанной в команде начала работы, больше 8 минут"
    ["0C"]="Вводимая дата более ранняя, чем дата последней фискальной операции"
    # Прочие ошибки
    ["0D"]="Неверный пароль доступа к ФП"
    ["0E"]="Отрицательный результат"
    ["0F"]="Для выполнения команды необходимо закрыть смену"
    # Дополнительные ошибки из второго документа
    ["10"]="Нет данных в журнале"
    ["11"]="Ошибка контрольной ленты"
    ["12"]="Ошибка посылки данных в ОФД"
    ["13"]="Критическая ошибка обновления"
    # Фатальные ошибки
    ["20"]="Фатальная ошибка ККТ. Причины возникновения данной ошибки можно уточнить в 'Статусе фатальных ошибок ККТ'"
    ["21"]="Нет свободного места в фискальной памяти ККТ"
    # Ошибки ФН
    ["41"]="Некорректный формат или параметр команды ФН"
    ["42"]="Некорректное состояние ФН"
    ["43"]="Ошибка ФН"
    ["44"]="Ошибка КС (Криптографического сопроцессора) в составе ФН"
    ["45"]="Исчерпан временной ресурс использования ФН"
    ["46"]="ФН переполнена"
    ["47"]="Неверные дата или время в ФН"
    ["48"]="Нет запрошенных данных в ФН"
    ["49"]="Переполнение (отрицательный итог документа, слишком много отделов для клиента)"
    ["4A"]="Нет ответа от ФН"
    ["4B"]="Ошибка при обмене данными с ФН"
    ["4C"]="ККТ передает в ФН данные, которые уже были переданы в составе данного документа"
    ["4D"]="Отсутствуют данные, необходимые для корректного учета в ФН"
    ["4E"]="Количество позиций в документе ФН превысило допустимый предел"
    ["50"]="Превышен размер данных TLV"
    ["51"]="Нет транспортного соединения"
    ["52"]="Исчерпан ресурс КС"
    ["54"]="Может быть выдан в нескольких случаях, в сочетании с флагами предупреждений. Если стоит флаг 'Превышено время ожидания ответа ОФД', то это значит, что время нахождения в очереди самого старого сообщения на выдачу более 30 календарных дней, необходимо передать сообщения в ОФД (Только при ККТ в режиме передачи данных). Если стоит флаг 'Архив ФН заполнен на 90%', то это означает, что Архив ФН полностью заполнен – необходимо закрыть ФН. Если флаги предупреждений отсутствуют, то это означает, что ресурс 30 дневного хранения для документов для ОФД исчерпан"
    ["55"]="Время нахождения в очереди самого старого сообщения на выдачу более 30 календарных дней."
    ["56"]="Продолжительность смены ФН более 24 часов"
    ["57"]="Разница более чем на 5 минут отличается от разницы, определенной по внутреннему таймеру ФН."
    ["58"]="Некорректный реквизит, переданный ККТ в ФН"
    ["59"]="Переданный в ФН реквизит не соответствует установкам при регистрации"
    ["60"]="Неверное сообщение от ОФД"
    ["61"]="Нет связи с ФН"
    ["62"]="Ошибка обмена с ФН"
    ["63"]="Слишком длинная команда для посылки в ФН"
    ["64"]="Неизвестный ответ сервиса обновления ключей проверки"
    ["72"]="Запрещена работа с маркированными товарами. При активации ФН 1.2 не был установлен признак работы с маркированными товарами"
    ["73"]="Нарушена правильная последовательность подачи команд для обработки товаров, имеющих КМ"
    ["74"]="Работа с маркированными товарами временно заблокирована. Необходимо отослать или выгрузить сформированные уведомления."
    ["75"]="Переполнена таблица хранения КМ"
    ["7C"]="В переданном блоке данных, который должен содержать реквизиты в TLV формате отсутствуют необходимые реквизиты."
    ["7E"]="В реквизите 2007 содержится КМ, который ранее не проверялся в ФН"
)

convert_cp866() {
    echo -n "$1" | iconv -f cp866
}

read_and_process_command_response() {
    : >"$INPUT_FILE"
    local end_time=$(($(date +%s) + TIMEOUT_SECS))

    while true; do
        dd status=none bs=4K iflag=nonblock <&"${SOCAT[0]}" 2>/dev/null >>"$INPUT_FILE"
        if LC_ALL=C sed -nz 's/.*\x02\(.\{5,\}\x03..\)/\1/p' "$INPUT_FILE" 2>/dev/null >"$COMMAND_RESPONSE_FILE" && [[ -s "$COMMAND_RESPONSE_FILE" ]]; then
            break
        elif (($(date +%s) > end_time)); then
            echo -e "Не удалось получить пакет"
            return 1
        fi
    done

    RESPONSE_HEX=$(xxd -p -u -c 1000000 "$COMMAND_RESPONSE_FILE")

    local packet_id_received="${RESPONSE_HEX:0:2}"
    if [[ "$packet_id_received" != "$PACKET_ID_SENT" ]]; then
        echo -e "ID пакета не совпадает. Получено: 0x$packet_id_received, ожидалось: 0x$PACKET_ID_SENT"
    fi

    local command_received
    command_received="$(head -c 3 "$COMMAND_RESPONSE_FILE" | tail -c 2)"
    if [[ "$command_received" != "$COMMAND_CODE" ]]; then
        echo -e "Команда не соответствует запросу. Получено: 0x$command_received. Ожидалось: 0x$COMMAND_CODE"
        COMMAND_CODE="$command_received"
    fi

    local crc_received
    crc_received="$(tail -c2 "$COMMAND_RESPONSE_FILE")"
    local crc_calculated
    crc_calculated="$(calculate_crc <(head -c-2 "$COMMAND_RESPONSE_FILE"))"
    if [[ "$crc_received" != "$crc_calculated" ]]; then
        echo -e "Контрольная сумма CRC не совпадает. Получено: $crc_received, Вычислено: $crc_calculated"
        return 1
    fi

    mapfile -t parsed_fields < <(tail -c +6 "$COMMAND_RESPONSE_FILE" | head -c-3 | tr $'\x1C' '\n')

    local error_code_hex
    error_code_hex="$(head -c 5 "$COMMAND_RESPONSE_FILE" | tail -c 2)"
    if [[ "$error_code_hex" != "00" ]]; then
        if [[ "${ERROR_CODES[$error_code_hex]}" ]]; then
            echo -e "Ошибка 0x$error_code_hex: ${ERROR_CODES[$error_code_hex]}"
        else
            echo -e "Неизвестный код ошибки 0x$error_code_hex."
        fi
        return 1
    fi
}

create_command_packet() {
    echo -n "${STX}${DEVICE_PASSWORD}" >"$COMMAND_PACKET"

    PACKET_ID_SENT=$(printf "%02X" "$PACKET_ID")
    echo -n "$PACKET_ID_SENT" | xxd -r -p >>"$COMMAND_PACKET"
    ((PACKET_ID++))
    ((PACKET_ID > 16#F0)) && PACKET_ID=$((16#20))

    echo -n "$COMMAND_CODE" >>"$COMMAND_PACKET"

    if ((${#COMMAND_PARAMS[@]} > 0)); then
        for param in "${COMMAND_PARAMS[@]}"; do
            echo -n "$param" >>"$COMMAND_PACKET"
            echo -n "${FS}" >>"$COMMAND_PACKET"
        done
    else
        echo -n "${FS}" >>"$COMMAND_PACKET"
    fi

    echo -n "${ETX}" >>"$COMMAND_PACKET"
    tail -c +2 "$COMMAND_PACKET" >"$DATA_FIELDS"
    echo -n "$(calculate_crc "$DATA_FIELDS")" >>"$COMMAND_PACKET"
}

interpret_bit_flags() {
    local value="$1"
    local table_name="$2"
    declare -A bit_table

    case "$table_name" in
    "фатальные_статусы")
        bit_table=(
            [0]="Неверная контрольная сумма NVR"
            [1]="Неверная контрольная сумма в конфигурации"
            [2]="Нет связи с ФН"
            [5]="ККТ не авторизовано"
            [6]="Фатальная ошибка ФН"
            [8]="SD карта отсутствует или неисправна"
        )
        ;;
    "текущие_флаги")
        bit_table=(
            [0]="Не выполнена команда \"Начало работы\""
            [1]="Нефискальный режим"
            [2]="Смена открыта"
            [3]="Смена больше 24 часов"
            [4]="Архив ФН закрыт"
            [5]="ФН не зарегистрирован"
            [8]="Не было завершено закрытие смены, необходимо повторить операцию"
            [9]="Ошибка контрольной ленты"
        )
        ;;
    "состояние_принтера")
        bit_table=(
            [0]="Принтер не готов"
            [1]="В принтере нет бумаги"
            [2]="Открыта крышка принтера"
            [3]="Ошибка резчика принтера"
            [7]="Нет связи с принтером"
        )
        ;;
    "статус_услуг")
        bit_table=(
            [0]="Услуга есть в ключе"
            [1]="Услуга активна"
        )
        ;;
    "предупреждающие_флаги")
        bit_table=(
            [0]="Срочная замена КС (до окончания срока действия 3 дня)"
            [1]="Исчерпание ресурса КС (до окончания срока действия 30 дней)"
            [2]="Переполнение памяти ФН (Архив ФН заполнен на 90%)"
            [3]="Превышено время ожидания ответа ОФД"
            [4]="нет"
            [5]="нет"
            [6]="нет"
            [7]="Критическая ошибка ФН"
        )
        ;;
    "статус_обмена")
        bit_table=(
            [0]="Транспортное соединение установлено"
            [1]="Есть сообщение для передачи в ОФД"
            [2]="Ожидание ответного сообщения от ОФД"
            [3]="Есть команда от ОФД"
            [4]="Изменились настройки соединения с ОФД"
            [5]="Ожидание ответа на команду от ОФД"
            [6]="Начато чтение сообщения для ОФД"
        )
        ;;
    "служебная_информация")
        bit_table=(
            [1]="Вернуть напряжение питания (мВ)"
            [2]="Вернуть температуру термоголовки"
            [3]="Вернуть количество отрезов резчика"
            [4]="Вернуть ресурс термоголовки"
            [5]="Обнулить количество отрезов резчика"
            [6]="Обнулить ресурс термоголовки"
            [7]="Вернуть напряжение на батарейке (мВ)"
            [8]="Вернуть количество отрезов резчика (необнуляемое)"
            [9]="Вернуть ресурс термоголовки (необнуляемый)"
            [10]="Вернуть тип ПУ"
            [11]="Вернуть версию BIOS ПУ"
            [12]="Вернуть серийный номер ПУ"
            [13]="Вернуть дату изменения BIOS ПУ"
        )
        ;;
    "система_налогообложения")
        bit_table=(
            [0]="Общая"
            [1]="Упрощенная Доход"
            [2]="Упрощенная Доход минус Расход"
            [3]="Единый налог на вмененный доход"
            [4]="Единый сельскохозяйственный налог"
            [5]="Патентная система налогообложения"
        )
        ;;
    "режим_работы")
        bit_table=(
            [0]="Признак шифрования"
            [1]="Признак автономного режима"
            [2]="Признак автоматического режима"
            [3]="Признак расчетов только за услуги"
            [4]="Признак АС БСО (Формируем только БСО)"
            [5]="Признак ККТ для расчетов только в Интернет"
            [6]="Признак торговли подакцизными товарами"
            [7]="Признак проведения азартных игр"
            [8]="Признак банковского платежного агента"
            [9]="Признак банковского платежного субагента"
            [10]="Признак платежного агента"
            [11]="Признак платежного субагента"
            [12]="Признак поверенного"
            [13]="Признак комиссионера"
            [14]="Применение агентами"
            [15]="Признак проведения лотереи"
        )
        ;;
    "дополнительный_режим")
        bit_table=(
            [0]="Устройство для печати находится вне корпуса ККТ"
            [1]="Признак торговли товарами подлежащими обязательной маркировке"
            [2]="Признак применения ККТ ломбардами"
            [3]="Признак применения ККТ в страховой деятельности"
        )
        ;;
    *)
        echo -e "Неизвестная таблица бит: $table_name"
        return
        ;;
    esac

    local set_bits=()
    for i in $(echo "obase=2; $value" | bc | rev | grep -o . | sed -n '/1/='); do
        set_bits+=("${bit_table[$((i - 1))]}")
    done

    if ((${#set_bits[@]} == 0)); then
        echo -e "  Нет установленных битов"
        JU+=("${table_name}_биты" "none")
    else
        for desc in "${set_bits[@]}"; do
            echo -e "  - $desc"
            JU+=("${table_name}_биты" "$desc")
        done
    fi
}

interpret_document_status() {
    local value="$1"
    local type_bits=$((value & 0x0F))
    local state_bits=$(((value & 0xF0) >> 4))
    declare -A type_table=(
        [0]="Документ закрыт"
        [1]="Сервисный документ"
        [2]="Чек на продажу (приход)"
        [3]="Чек на возврат (возврат прихода)"
        [4]="Внесение в кассу"
        [5]="Инкассация"
        [6]="Чек на покупку (расход)"
        [7]="Чек на возврат покупки (возврат расхода)"
        [9]="Чек корр. приход"
        [10]="Чек корр. расход"
        [11]="Чек корр. возврат прихода"
        [12]="Чек корр. возврат расхода"
    )
    declare -A state_table=(
        [0]="Документ закрыт"
        [1]="Устанавливается после команды 'открыть документ' (можно добавлять товарные позиции)"
        [2]="Устанавливается после первой команды 'Подытог'"
        [3]="Устанавливается после второй команды 'Подытог' или после начала команды 'Оплата' (можно только производить оплату)"
        [4]="Расчет завершен, требуется закрыть документ"
        [8]="Документ закрыт в ФН, но чек не допечатан"
    )
    local type_desc="${type_table[$type_bits]:-Неизвестный тип ($type_bits)}"
    local state_desc="${state_table[$state_bits]:-Неизвестное состояние ($state_bits)}"
    echo -e "  Тип документа: $type_desc"
    JU+=("статус_документа.тип" "$type_desc")
    echo -e "  Состояние документа: $state_desc"
    JU+=("статус_документа.состояние" "$state_desc")
}

interpret_fn_status() {
    local value="$1"
    local phase_bits=$((value & 0x0F))
    local doc_data_bit=$(((value & 0x10) >> 4))
    local shift_state_bit=$(((value & 0x20) >> 5))

    declare -A phase_table=(
        [0]="Настройка"
        [1]="Готовность к фискализации"
        [3]="Фискальный режим"
        [7]="Постфиксальный режим"
        [15]="Чтение данных из архива ФН"
    )
    declare -A doc_data_table=(
        [0]="Нет данных документа"
        [1]="Получены данные документа"
    )
    declare -A shift_state_table=(
        [0]="Смена закрыта"
        [1]="Смена открыта"
    )

    local phase_desc="${phase_table[$phase_bits]:-Неизвестная фаза ($phase_bits)}"
    local doc_data_desc="${doc_data_table[$doc_data_bit]:-Неизвестное состояние данных документа ($doc_data_bit)}"
    local shift_state_desc="${shift_state_table[$shift_state_bit]:-Неизвестное состояние смены ($shift_state_bit)}"

    echo -e "    Фаза жизни ФН: $phase_desc"
    JU+=("статус_фн.фаза" "$phase_desc")
    echo -e "    Данные документа: $doc_data_desc"
    JU+=("статус_фн.данные_документа" "$doc_data_desc")
    echo -e "    Состояние смены: $shift_state_desc"
    JU+=("статус_фн.состояние_смены" "$shift_state_desc")
}

interpret_open_document_type() {
    local value="$1"
    declare -A doc_type_table=(
        [00]="Нет открытого документа"
        [01]="Отчёт о регистрации ККТ"
        [02]="Отчёт об открытии смены"
        [04]="Кассовый чек"
        [08]="Отчёт о закрытии смены"
        [10]="Отчёт о закрытии фискального режима"
        [11]="Бланк строкой отчетности (БСО)"
        [12]="Отчет об изменении параметров регистрации ККТ в связи с заменой ФН"
        [13]="Отчет об изменении параметров регистрации ККТ"
        [14]="Кассовый чек коррекции"
        [15]="БСО коррекции"
        [17]="Отчет о текущем состоянии расчетов"
    )
    local doc_type_desc="${doc_type_table["$(printf "%02X" "$value")"]:-Неизвестный тип документа ($value)}"
    echo -e "  Тип открытого документа: $doc_type_desc"
    JU+=("текущий_документ.тип" "$doc_type_desc")
}

retrieve_data() {
    case "$COMMAND_CODE" in
    00)
        FATAL_STATUS="${parsed_fields[0]}"
        CURRENT_FLAGS="${parsed_fields[1]}"
        DOCUMENT_STATUS="${parsed_fields[2]}"
        ;;
    01)
        QUERY_NUMBER="${parsed_fields[0]}"
        parsed_fields=("${parsed_fields[@]:1}")
        case "$QUERY_NUMBER" in
        1)
            CURRENT_SHIFT_NUMBER="${parsed_fields[0]}"
            ;;
        2)
            NEXT_CHECK_NUMBER="${parsed_fields[0]}"
            ;;
        3)
            SALES_SUM_BY_PAYMENT_TYPE=("${parsed_fields[@]}")
            ;;
        4)
            PAYMENT_COUNT_BY_SALES=("${parsed_fields[@]}")
            ;;
        5)
            REFUND_SUM_BY_PAYMENT_TYPE=("${parsed_fields[@]}")
            ;;
        6)
            PAYMENT_COUNT_BY_REFUNDS=("${parsed_fields[@]}")
            ;;
        7)
            CHECK_COUNT_BY_OPERATION_TYPE=("${parsed_fields[@]}")
            ;;
        8)
            CHECK_SUM_BY_OPERATION_TYPE=("${parsed_fields[@]}")
            ;;
        9)
            DISCOUNT_SUMS=("${parsed_fields[@]}")
            ;;
        10)
            TAX_SUM_BY_SALES=("${parsed_fields[@]}")
            ;;
        11)
            TAX_SUM_BY_REFUNDS=("${parsed_fields[@]}")
            ;;
        12)
            REPORT_OPERATIONAL_COUNTER="${parsed_fields[0]}"
            REPORT_DOCUMENT_NUMBER="${parsed_fields[1]}"
            REPORT_CASH_SUM="${parsed_fields[2]}"
            REPORT_SALES_COUNT="${parsed_fields[3]}"
            REPORT_SALES_SUM="${parsed_fields[4]}"
            REPORT_REFUND_COUNT="${parsed_fields[5]}"
            REPORT_REFUND_SUM="${parsed_fields[6]}"
            REPORT_CANCELLED_COUNT="${parsed_fields[7]}"
            REPORT_CANCELLED_SUM="${parsed_fields[8]}"
            REPORT_DEPOSIT_COUNT="${parsed_fields[9]}"
            REPORT_DEPOSIT_SUM="${parsed_fields[10]}"
            REPORT_WITHDRAWAL_COUNT="${parsed_fields[11]}"
            REPORT_WITHDRAWAL_SUM="${parsed_fields[12]}"
            ;;
        13)
            SALES_SUM_BY_SECTION=("${parsed_fields[@]}")
            ;;
        14)
            REFUND_SUM_BY_SECTION=("${parsed_fields[@]}")
            ;;
        15)
            PURCHASE_CHECK_COUNT="${parsed_fields[0]}"
            PURCHASE_REFUND_CHECK_COUNT="${parsed_fields[1]}"
            ;;
        16)
            PURCHASE_SUM_BY_PAYMENT_TYPE=("${parsed_fields[@]}")
            ;;
        17)
            REFUND_PURCHASE_SUM_BY_PAYMENT_TYPE=("${parsed_fields[@]}")
            ;;
        18)
            CORRECTION_COUNT="${parsed_fields[0]}"
            CORRECTION_SUM_CASH="${parsed_fields[1]}"
            CORRECTION_SUM_CASHLESS="${parsed_fields[2]}"
            ;;
        esac
        ;;
    02)
        QUERY_NUMBER="${parsed_fields[0]}"
        parsed_fields=("${parsed_fields[@]:1}")
        case "$QUERY_NUMBER" in
        1)
            FACTORY_NUMBER="${parsed_fields[0]}"
            ;;
        2)
            FIRMWARE_ID="${parsed_fields[0]}.${parsed_fields[1]}.${parsed_fields[2]}"
            ;;
        3)
            INN="${parsed_fields[0]}"
            ;;
        4)
            REGISTRATION_NUMBER="${parsed_fields[0]}"
            ;;
        5)
            LAST_FISCAL_OPERATION_DATE="${parsed_fields[0]}"
            LAST_FISCAL_OPERATION_TIME="${parsed_fields[1]}"
            ;;
        6)
            REGISTRATION_DATE="${parsed_fields[0]}"
            ;;
        7)
            CASH_AMOUNT="${parsed_fields[0]}"
            ;;
        8)
            NEXT_DOCUMENT_NUMBER="${parsed_fields[0]}"
            ;;
        9)
            SHIFT_NUMBER="${parsed_fields[0]}"
            ;;
        10)
            NEXT_X_REPORT_NUMBER="${parsed_fields[0]}"
            ;;
        11)
            OPERATIONAL_COUNTER="${parsed_fields[0]}"
            ;;
        12)
            CUMULATIVE_TOTAL_SALES="${parsed_fields[0]}"
            CUMULATIVE_TOTAL_REFUNDS="${parsed_fields[1]}"
            CUMULATIVE_TOTAL_PURCHASES="${parsed_fields[2]}"
            CUMULATIVE_TOTAL_PURCHASE_REFUNDS="${parsed_fields[3]}"
            ;;
        14)
            FN_RESOURCE_END_DATE="${parsed_fields[0]}"
            ;;
        15)
            FIRMWARE_TYPE="${parsed_fields[0]}"
            ;;
        16)
            PAPER_SIZE="${parsed_fields[0]}"
            ;;
        17)
            SHIFT_OPEN_DATE="${parsed_fields[0]}"
            SHIFT_OPEN_TIME="${parsed_fields[1]}"
            ;;
        18)
            LINE_CHAR_COUNT="${parsed_fields[0]}"
            ;;
        19)
            CID_REGISTER_CONTENT="${parsed_fields[0]}"
            ;;
        20)
            CSD_REGISTER_CONTENT="${parsed_fields[0]}"
            ;;
        21)
            DEVICE_MODEL="${parsed_fields[0]}"
            ;;
        22)
            INTERFACE_MASK="${parsed_fields[0]}"
            ;;
        23)
            TAX_SYSTEM_BITS="${parsed_fields[0]}"
            OPERATION_MODE_BITS="${parsed_fields[1]}"
            ADDITIONAL_MODE_BITS="${parsed_fields[2]}"
            ;;
        24)
            MAX_ADDITIONAL_LINES_START="${parsed_fields[0]}"
            MAX_ADDITIONAL_LINES_END="${parsed_fields[1]}"
            ;;
        40)
            NDS_STATE="${parsed_fields[0]}"
            NDS_STATE_DESCRIPTION=""
            case $NDS_STATE in
            0) NDS_STATE_DESCRIPTION="переход на НДС 20% не был выполнен" ;;
            1) NDS_STATE_DESCRIPTION="ККТ работает с НДС 20%" ;;
            *) NDS_STATE_DESCRIPTION="Неизвестное состояние ($NDS_STATE)" ;;
            esac
            ;;
        70)
            WORKING_FIRMWARE_ID="${parsed_fields[0]}"
            ;;
        esac
        ;;
    04)
        PRINTER_STATE="${parsed_fields[0]}"
        ;;
    05)
        QUERY_NUMBER="${parsed_fields[0]}"
        parsed_fields=("${parsed_fields[@]:1}")
        case "$QUERY_NUMBER" in
        1)
            VOLTAGE="${parsed_fields[0]}"
            ;;
        2)
            TEMPERATURE="${parsed_fields[0]}"
            ;;
        3)
            CUTTER_COUNT="${parsed_fields[0]}"
            ;;
        4)
            THERMAL_RESOURCE="${parsed_fields[0]}"
            ;;
        7)
            BATTERY_VOLTAGE="${parsed_fields[0]}"
            ;;
        8)
            CUTTER_COUNT_NON_RESET="${parsed_fields[0]}"
            ;;
        9)
            THERMAL_RESOURCE_NON_RESET="${parsed_fields[0]}"
            ;;
        10)
            DEVICE_TYPE="${parsed_fields[0]}"
            ;;
        11)
            BIOS_VERSION="${parsed_fields[0]}"
            ;;
        12)
            SERIAL_NUMBER="${parsed_fields[0]}"
            ;;
        13)
            BIOS_CHANGE_DATE="${parsed_fields[0]}"
            ;;
        esac
        ;;
    11)
        QUERY_NUMBER="${COMMAND_PARAMS[0]}"
        case "$QUERY_NUMBER" in
        30)
            local line_index="${COMMAND_PARAMS[1]}"
            local value="$(convert_cp866 "${parsed_fields[0]}")"
            case "$line_index" in
            0)
                ORG_INFO_LINE_1="$value"
                ;;
            1)
                ORG_INFO_LINE_2="$value"
                ;;
            2)
                ORG_INFO_LINE_3="$value"
                ;;
            3)
                ORG_INFO_LINE_4="$value"
                ;;
            esac
            ;;
        40)
            TAX_TITLES+=("$(convert_cp866 "${parsed_fields[0]}")")
            ;;
        41)
            TAX_PERCENTAGES+=("${parsed_fields[0]}")
            ;;
        42)
            TAX_GROUP_NAME="$(convert_cp866 "${parsed_fields[0]}")"
            ;;
        73)
            IP_ADDRESS="${parsed_fields[0]}"
            ;;
        74)
            SUBNET_MASK="${parsed_fields[0]}"
            ;;
        75)
            GATEWAY_IP="${parsed_fields[0]}"
            ;;
        76)
            DNS_IP="${parsed_fields[0]}"
            ;;
        77)
            OFD_SERVER_ADDRESS="${parsed_fields[0]}"
            ;;
        78)
            OFD_SERVER_PORT="${parsed_fields[0]}"
            ;;
        79)
            FN_TIMER="${parsed_fields[0]}"
            ;;
        80)
            TIMER_S="${parsed_fields[0]}"
            ;;
        81)
            OFD_NAME="$(convert_cp866 "${parsed_fields[0]}")"
            ;;
        91)
            OKP_SERVER_IP="${parsed_fields[0]}"
            ;;
        92)
            OKP_SERVER_PORT="${parsed_fields[0]}"
            ;;
        93)
            OKP_SERVER_URL="${parsed_fields[0]}"
            ;;
        94)
            OKP_SERVER_TIMER="${parsed_fields[0]}"
            ;;
        201)
            OISM_SERVER_IP="${parsed_fields[0]}"
            ;;
        202)
            OISM_SERVER_PORT="${parsed_fields[0]}"
            ;;
        203)
            OISM_SERVER_URL="${parsed_fields[0]}"
            ;;
        204)
            OISM_SERVER_TIMER="${parsed_fields[0]}"
            ;;
        esac
        ;;
    13)
        CURRENT_DATE="${parsed_fields[0]}"
        CURRENT_TIME="${parsed_fields[1]}"
        ;;
    30) ;;
    31) ;;
    40) ;;
    58) ;;
    78)
        QUERY_NUMBER="${parsed_fields[0]}"
        parsed_fields=("${parsed_fields[@]:1}")
        case "$QUERY_NUMBER" in
        1)
            REG_NUMBER="${parsed_fields[0]}"
            ;;
        2)
            FN_STATE="${parsed_fields[0]}"
            DOC_STATE="${parsed_fields[1]}"
            WARNING_FLAGS="${parsed_fields[2]}"
            ;;
        3)
            LAST_DOC_NUMBER="${parsed_fields[0]}"
            ;;
        4)
            REG_DATE="${parsed_fields[0]}"
            REG_TIME="${parsed_fields[1]}"
            ;;
        5)
            LAST_REG_FD="${parsed_fields[0]}"
            ;;
        6)
            SHIFT_NUMBER="${parsed_fields[0]}"
            CHECK_NUMBER="${parsed_fields[1]}"
            SHIFT_STATUS="${parsed_fields[2]}"
            ;;
        7)
            EXCHANGE_STATUS="${parsed_fields[0]}"
            DOC_COUNT="${parsed_fields[1]}"
            FIRST_DOC_NUMBER="${parsed_fields[2]}"
            FIRST_DOC_DATE="${parsed_fields[3]}"
            FIRST_DOC_TIME="${parsed_fields[4]}"
            ;;
        11)
            RECEIPT_FLAG="${parsed_fields[0]}"
            DOCUMENT_DATA="${parsed_fields[1]}"
            ;;
        12)
            RECEIPT_DATA="${parsed_fields[0]}"
            ;;
        13)
            ERROR_DATA="${parsed_fields[0]}"
            ;;
        14)
            FN_VERSION="${parsed_fields[0]}"
            FN_TYPE="${parsed_fields[1]}"
            FFD_VERSION="${parsed_fields[2]}"
            KKT_VERSION="${parsed_fields[3]}"
            FFD_KKT_VERSION="${parsed_fields[4]}"

            FN_TYPE_DESC=""
            FFD_VERSION_DESC=""

            case "${FN_TYPE}" in
            0) FN_TYPE_DESC="отладочный ФН" ;;
            1) FN_TYPE_DESC="серийный ФН" ;;
            *) FN_TYPE_DESC="неизвестный тип ФН" ;;
            esac

            case "${FFD_VERSION}" in
            4) FFD_VERSION_DESC="1.2" ;;
            2) FFD_VERSION_DESC="1.05" ;;
            *) FFD_VERSION_DESC="Неизвестная версия (${FFD_VERSION})" ;;
            esac
            ;;
        15)
            DOC_TYPE="${parsed_fields[0]}"
            DOC_SIZE="${parsed_fields[1]}"
            ;;
        16)
            TLV_DATA="${parsed_fields[0]}"
            ;;
        17)
            FD_START_NUMBER="${parsed_fields[0]}"
            ;;
        18)
            APPROX_DOC_COUNT="${parsed_fields[0]}"
            FREE_MEMORY_KB="${parsed_fields[1]}"
            ;;
        19)
            FD_NUMBER="${parsed_fields[0]}"
            FP_NUMBER="${parsed_fields[1]}"
            DOC_DATE="${parsed_fields[2]}"
            DOC_TIME="${parsed_fields[3]}"
            ;;
        20)
            FN_REGISTRATION_TLV="${parsed_fields[0]}"
            ;;
        21)
            TLV_REGISTRATION_DATA="${parsed_fields[0]}"
            ;;
        22)
            CURRENT_FFD_VERSION="${parsed_fields[0]}"
            MAX_FFD_VERSION="${parsed_fields[1]}"

            CURRENT_FFD_DESC=""
            MAX_FFD_DESC=""

            case "${CURRENT_FFD_VERSION}" in
            1) CURRENT_FFD_DESC="1.05" ;;
            2) CURRENT_FFD_DESC="1.05" ;;
            4) CURRENT_FFD_DESC="1.2" ;;
            *) CURRENT_FFD_DESC="Неизвестная версия (${CURRENT_FFD_VERSION})" ;;
            esac

            case "${MAX_FFD_VERSION}" in
            2) MAX_FFD_DESC="1.05" ;;
            4) MAX_FFD_DESC="1.2" ;;
            *) MAX_FFD_DESC="Неизвестная версия (${MAX_FFD_VERSION})" ;;
            esac
            ;;
        23)
            local type="${COMMAND_PARAMS[1]}"
            local calc_attr="${COMMAND_PARAMS[2]}"

            case "$type" in
            1)
                case "$calc_attr" in
                1)
                    FULL_FN_COUNTERS_INCOME=("${parsed_fields[@]}")
                    ;;
                2)
                    FULL_FN_COUNTERS_INCOME_RETURN=("${parsed_fields[@]}")
                    ;;
                3)
                    FULL_FN_COUNTERS_EXPENSE=("${parsed_fields[@]}")
                    ;;
                4)
                    FULL_FN_COUNTERS_EXPENSE_RETURN=("${parsed_fields[@]}")
                    ;;
                esac
                ;;
            0)
                case "$calc_attr" in
                1)
                    SHIFT_COUNTERS_INCOME=("${parsed_fields[@]}")
                    ;;
                2)
                    SHIFT_COUNTERS_INCOME_RETURN=("${parsed_fields[@]}")
                    ;;
                3)
                    SHIFT_COUNTERS_EXPENSE=("${parsed_fields[@]}")
                    ;;
                4)
                    SHIFT_COUNTERS_EXPENSE_RETURN=("${parsed_fields[@]}")
                    ;;
                esac
                ;;
            esac
            ;;
        esac
        ;;
    94) ;;
    A6)
        LEGAL_NAME="$(convert_cp866 "${parsed_fields[0]}")"
        ADDRESS="$(convert_cp866 "${parsed_fields[1]}")"
        INN="${parsed_fields[2]}"
        FN_NUMBER="${parsed_fields[3]}"
        FFD_VERSION="${parsed_fields[4]}"
        REGISTRY_NUMBER="${parsed_fields[5]}"
        TAX_MODES="${parsed_fields[6]}"
        AUTONOMIC="${parsed_fields[7]}"
        OFD_PROVIDER_NAME="$(convert_cp866 "${parsed_fields[8]}")"
        OFD_PROVIDER_INN="${parsed_fields[9]}"
        OFD_PROVIDER_SERVER_HOST="${parsed_fields[10]}"
        OFD_PROVIDER_SERVER_PORT="${parsed_fields[11]}"
        OFD_PROVIDER_CHECK_URL="${parsed_fields[12]}"
        WORK_MODE="${parsed_fields[13]}"
        KKT_EXPIRE_DATE="${parsed_fields[14]}"
        LOCAL_DATE="${parsed_fields[15]}"
        LOCAL_TIME="${parsed_fields[16]}"
        PAYMENT_PLACE="$(convert_cp866 "${parsed_fields[17]}")"
        ;;
    A7)
        UUID="${parsed_fields[0]}"
        MODEL="${parsed_fields[1]}"
        MODEL_NAME="${parsed_fields[2]}"
        READABLE_MODEL="$(convert_cp866 "${parsed_fields[3]}")"
        KKT_REGISTRY_NAME="$(convert_cp866 "${parsed_fields[4]}")"
        KKT_FACTORY_NUMBER="${parsed_fields[5]}"
        IS_FN="${parsed_fields[6]}"
        ;;
    AA)
        QUERY_NUMBER="${parsed_fields[0]}"
        parsed_fields=("${parsed_fields[@]:1}")
        case "$QUERY_NUMBER" in
        1)
            KEY_ACTIVATED="${parsed_fields[0]}"
            ;;
        2)
            SERVICE_NUMBER="${parsed_fields[0]}"
            SERVICE_STATUS="${parsed_fields[1]}"
            SERVICE_DESCRIPTION="${parsed_fields[2]}"
            SERVICE_END_DATE="${parsed_fields[3]}"
            ;;
        3)
            ALL_SERVICES=("${parsed_fields[@]}")
            ;;
        4)
            PROTOCOL_VERSION="${parsed_fields[0]}"
            SERVICE_COUNT="${parsed_fields[1]}"
            SERVICE_LIST=("${parsed_fields[@]:2}")
            SERVICE_LIST_DESCRIPTION_SPACES=()
            SERVICE_LIST_DESCRIPTION_TABS=()

            local id
            local status
            local status_text
            local description
            local date
            for ((i = 0; i < ${#SERVICE_LIST[@]}; i++)); do
                SERVICE_LIST[i]="$(convert_cp866 "${SERVICE_LIST[i]}")"

                IFS=';' read -r id description status date <<<"${SERVICE_LIST[i]}"
                if ((status == 3)); then
                    status_text="активна"
                elif ((status == 1)); then
                    status_text="истекла"
                elif ((status == 0)); then
                    status_text="не активирована"
                else
                    status_text="статус $status неизвестен"
                fi
                if [[ "$date" ]]; then
                    status_text+=" (действие ключа до $date)"
                fi

                SERVICE_LIST_DESCRIPTION_SPACES[i]="$description - $status_text"
                SERVICE_LIST_DESCRIPTION_TABS[i]="$description\t- $status_text"
            done
            ;;
        esac
        ;;
    *) ;;
    esac
}

format_array() {
    local array=("$@")
    printf '"%s",' "${array[@]//\"/\\\"}" | head -c -1
}

update_json() {
    case "$COMMAND_CODE" in
    00)
        JU+=("статус.фатальный_статус" "$FATAL_STATUS")
        JU+=("статус.текущие_флаги" "$CURRENT_FLAGS")
        JU+=("статус.статус_документа" "$DOCUMENT_STATUS")
        ;;
    01)
        case "$QUERY_NUMBER" in
        1)
            JU+=("сведения_смены.номер_текущей_смены" "$CURRENT_SHIFT_NUMBER")
            ;;
        2)
            JU+=("сведения_смены.номер_следующего_чека" "$NEXT_CHECK_NUMBER")
            ;;
        3)
            JU+=("сведения_смены.суммы_продаж_по_типам_оплаты" "[$(format_array "${SALES_SUM_BY_PAYMENT_TYPE[@]}")]")
            ;;
        4)
            JU+=("сведения_смены.количество_оплат_по_продажам" "[$(format_array "${PAYMENT_COUNT_BY_SALES[@]}")]")
            ;;
        5)
            JU+=("сведения_смены.суммы_возврата_по_типам_оплаты" "[$(format_array "${REFUND_SUM_BY_PAYMENT_TYPE[@]}")]")
            ;;
        6)
            JU+=("сведения_смены.количество_оплат_по_возвратам" "[$(format_array "${PAYMENT_COUNT_BY_REFUNDS[@]}")]")
            ;;
        7)
            JU+=("сведения_смены.количество_чеков_по_типам_операций" "[$(format_array "${CHECK_COUNT_BY_OPERATION_TYPE[@]}")]")
            ;;
        8)
            JU+=("сведения_смены.суммы_чеков_по_типам_операций" "[$(format_array "${CHECK_SUM_BY_OPERATION_TYPE[@]}")]")
            ;;
        9)
            JU+=("сведения_смены.суммы_скидок_по_типам_операций" "[$(format_array "${DISCOUNT_SUMS[@]}")]")
            ;;
        10)
            JU+=("сведения_смены.суммы_налогов_по_продажам" "[$(format_array "${TAX_SUM_BY_SALES[@]}")]")
            ;;
        11)
            JU+=("сведения_смены.суммы_налогов_по_возвратам" "[$(format_array "${TAX_SUM_BY_REFUNDS[@]}")]")
            ;;
        12)
            JU+=("отчет.операционный_счетчик" "$REPORT_OPERATIONAL_COUNTER")
            JU+=("отчет.номер_документа" "$REPORT_DOCUMENT_NUMBER")
            JU+=("отчет.сумма_наличных" "$REPORT_CASH_SUM")
            JU+=("отчет.количество_продаж" "$REPORT_SALES_COUNT")
            JU+=("отчет.сумма_продаж" "$REPORT_SALES_SUM")
            JU+=("отчет.количество_возвратов" "$REPORT_REFUND_COUNT")
            JU+=("отчет.сумма_возвратов" "$REPORT_REFUND_SUM")
            JU+=("отчет.количество_аннулированных" "$REPORT_CANCELLED_COUNT")
            JU+=("отчет.сумма_аннулированных" "$REPORT_CANCELLED_SUM")
            JU+=("отчет.количество_депозитов" "$REPORT_DEPOSIT_COUNT")
            JU+=("отчет.сумма_депозитов" "$REPORT_DEPOSIT_SUM")
            JU+=("отчет.количество_снятий" "$REPORT_WITHDRAWAL_COUNT")
            JU+=("отчет.сумма_снятий" "$REPORT_WITHDRAWAL_SUM")
            ;;
        13)
            JU+=("сведения_смены.суммы_продаж_по_секциям" "[$(format_array "${SALES_SUM_BY_SECTION[@]}")]")
            ;;
        14)
            JU+=("сведения_смены.суммы_возвратов_по_секциям" "[$(format_array "${REFUND_SUM_BY_SECTION[@]}")]")
            ;;
        15)
            JU+=("сведения_смены.количество_чеков_по_покупкам" "$PURCHASE_CHECK_COUNT")
            JU+=("сведения_смены.количество_чеков_возвратов_по_покупкам" "$PURCHASE_REFUND_CHECK_COUNT")
            ;;
        16)
            JU+=("сведения_смены.суммы_покупок_по_типам_оплаты" "[$(format_array "${PURCHASE_SUM_BY_PAYMENT_TYPE[@]}")]")
            ;;
        17)
            JU+=("сведения_смены.суммы_возвратов_покупок_по_типам_оплаты" "[$(format_array "${REFUND_PURCHASE_SUM_BY_PAYMENT_TYPE[@]}")]")
            ;;
        18)
            JU+=("сведения_смены.количество_коррекций" "$CORRECTION_COUNT")
            JU+=("сведения_смены.сумма_коррекций_наличными" "$CORRECTION_SUM_CASH")
            JU+=("сведения_смены.сумма_коррекций_без_наличных" "$CORRECTION_SUM_CASHLESS")
            ;;
        esac
        ;;
    02)
        case "$QUERY_NUMBER" in
        1)
            JU+=("информация_ккт.заводской_номер" "$FACTORY_NUMBER")
            ;;
        2)
            JU+=("информация_ккт.идентификатор_прошивки" "$FIRMWARE_ID")
            ;;
        3)
            JU+=("информация_ккт.инн" "$INN")
            ;;
        4)
            JU+=("информация_ккт.регистрационный_номер" "$REGISTRATION_NUMBER")
            ;;
        5)
            JU+=("информация_ккт.последняя_фискальная_операция" "$LAST_FISCAL_OPERATION_DATE $LAST_FISCAL_OPERATION_TIME")
            ;;
        6)
            JU+=("информация_ккт.дата_регистрации" "$REGISTRATION_DATE")
            ;;
        7)
            JU+=("информация_ккт.сумма_наличных" "$CASH_AMOUNT")
            ;;
        8)
            JU+=("информация_ккт.номер_следующего_документа" "$NEXT_DOCUMENT_NUMBER")
            ;;
        9)
            JU+=("информация_ккт.номер_смены" "$SHIFT_NUMBER")
            ;;
        10)
            JU+=("информация_ккт.номер_следующего_x_отчета" "$NEXT_X_REPORT_NUMBER")
            ;;
        11)
            JU+=("информация_ккт.операционный_счетчик" "$OPERATIONAL_COUNTER")
            ;;
        12)
            JU+=("информация_ккт.нарастающий_итог.продажи" "$CUMULATIVE_TOTAL_SALES")
            JU+=("информация_ккт.нарастающий_итог.возвраты" "$CUMULATIVE_TOTAL_REFUNDS")
            JU+=("информация_ккт.нарастающий_итог.покупки" "$CUMULATIVE_TOTAL_PURCHASES")
            JU+=("информация_ккт.нарастающий_итог.возвраты_покупок" "$CUMULATIVE_TOTAL_PURCHASE_REFUNDS")
            ;;
        14)
            JU+=("информация_ккт.дата_окончания_ресурса_фн" "$FN_RESOURCE_END_DATE")
            ;;
        15)
            JU+=("информация_ккт.тип_прошивки" "$FIRMWARE_TYPE")
            ;;
        16)
            JU+=("информация_ккт.размер_бумаги" "$PAPER_SIZE")
            ;;
        17)
            JU+=("информация_ккт.дата_и_время_открытия_смены" "$SHIFT_OPEN_DATE $SHIFT_OPEN_TIME")
            ;;
        18)
            JU+=("информация_ккт.количество_символов_в_строке" "$LINE_CHAR_COUNT")
            ;;
        19)
            JU+=("информация_ккт.содержание_регистра_cid" "$CID_REGISTER_CONTENT")
            ;;
        20)
            JU+=("информация_ккт.содержание_регистра_csd" "$CSD_REGISTER_CONTENT")
            ;;
        21)
            JU+=("информация_ккт.модель_устройства" "$DEVICE_MODEL")
            ;;
        22)
            JU+=("информация_ккт.маска_интерфейса" "$INTERFACE_MASK")
            ;;
        23)
            JU+=("система_налогообложения.биты" "$TAX_SYSTEM_BITS")
            JU+=("режим_работы.биты" "$OPERATION_MODE_BITS")
            JU+=("дополнительный_режим_работы.биты" "$ADDITIONAL_MODE_BITS")
            ;;
        24)
            JU+=("информация_ккт.максимальное_число_дополнительных_строк.начало" "$MAX_ADDITIONAL_LINES_START")
            JU+=("информация_ккт.максимальное_число_дополнительных_строк.конец" "$MAX_ADDITIONAL_LINES_END")
            ;;
        40)
            JU+=("информация_ккт.состояние_ндс" "$NDS_STATE_DESCRIPTION")
            ;;
        70)
            JU+=("информация_ккт.ид_рабочей_прошивки" "$WORKING_FIRMWARE_ID")
            ;;
        esac
        ;;
    04)
        JU+=("состояние_принтера.биты" "$PRINTER_STATE")
        ;;
    05)
        case "$QUERY_NUMBER" in
        1)
            JU+=("сервисная_информация.напряжение" "$VOLTAGE")
            ;;
        2)
            JU+=("сервисная_информация.температура" "$TEMPERATURE")
            ;;
        3)
            JU+=("сервисная_информация.количество_отрезов" "$CUTTER_COUNT")
            ;;
        4)
            JU+=("сервисная_информация.ресурс_термоголовки" "$THERMAL_RESOURCE")
            ;;
        7)
            JU+=("сервисная_информация.напряжение_батарейки" "$BATTERY_VOLTAGE")
            ;;
        8)
            JU+=("сервисная_информация.количество_отрезов_необнуляемое" "$CUTTER_COUNT_NON_RESET")
            ;;
        9)
            JU+=("сервисная_информация.ресурс_термоголовки_необнуляемый" "$THERMAL_RESOURCE_NON_RESET")
            ;;
        10)
            JU+=("сервисная_информация.тип_пу" "$DEVICE_TYPE")
            ;;
        11)
            JU+=("сервисная_информация.версия_bios_пу" "$BIOS_VERSION")
            ;;
        12)
            JU+=("сервисная_информация.серийный_номер_пу" "$SERIAL_NUMBER")
            ;;
        13)
            JU+=("сервисная_информация.дата_изменения_bios_пу" "$BIOS_CHANGE_DATE")
            ;;
        esac
        ;;
    11)
        case "$QUERY_NUMBER" in
        30)
            local line_index="${COMMAND_PARAMS[1]}"
            case "$line_index" in
            0)
                JU+=("информация_об_организации.название_строка_1" "$ORG_INFO_LINE_1")
                ;;
            1)
                JU+=("информация_об_организации.название_строка_2" "$ORG_INFO_LINE_2")
                ;;
            2)
                JU+=("информация_об_организации.адрес_строка_1" "$ORG_INFO_LINE_3")
                ;;
            3)
                JU+=("информация_об_организации.адрес_строка_2" "$ORG_INFO_LINE_4")
                ;;
            esac
            ;;
        40)
            JU+=("налоговые_настройки.название_налога" "[$(format_array "${TAX_TITLES[@]}")]")
            ;;
        41)
            JU+=("налоговые_настройки.процент_налога" "[$(format_array "${TAX_PERCENTAGES[@]}")]")
            ;;
        42)
            JU+=("налоговые_настройки.название_налоговой_группы" "$TAX_GROUP_NAME")
            ;;
        73)
            JU+=("сетевые_настройки.ip_адрес" "$IP_ADDRESS")
            ;;
        74)
            JU+=("сетевые_настройки.маска_подсети" "$SUBNET_MASK")
            ;;
        75)
            JU+=("сетевые_настройки.шлюз" "$GATEWAY_IP")
            ;;
        76)
            JU+=("сетевые_настройки.dns" "$DNS_IP")
            ;;
        77)
            JU+=("сетевые_настройки.сервер_офд_адрес" "$OFD_SERVER_ADDRESS")
            ;;
        78)
            JU+=("сетевые_настройки.сервер_офд_порт" "$OFD_SERVER_PORT")
            ;;
        79)
            JU+=("сетевые_настройки.таймер_фн" "$FN_TIMER")
            ;;
        80)
            JU+=("сетевые_настройки.таймер_s" "$TIMER_S")
            ;;
        81)
            JU+=("сетевые_настройки.название_офд" "$OFD_NAME")
            ;;
        91)
            JU+=("сетевые_настройки.ip_okp_сервера" "$OKP_SERVER_IP")
            ;;
        92)
            JU+=("сетевые_настройки.порт_okp_сервера" "$OKP_SERVER_PORT")
            ;;
        93)
            JU+=("сетевые_настройки.url_okp_сервера" "$OKP_SERVER_URL")
            ;;
        94)
            JU+=("сетевые_настройки.таймер_okp_сервера" "$OKP_SERVER_TIMER")
            ;;
        201)
            JU+=("сетевые_настройки.ip_oism_сервера" "$OISM_SERVER_IP")
            ;;
        202)
            JU+=("сетевые_настройки.порт_oism_сервера" "$OISM_SERVER_PORT")
            ;;
        203)
            JU+=("сетевые_настройки.url_oism_сервера" "$OISM_SERVER_URL")
            ;;
        204)
            JU+=("сетевые_настройки.таймер_oism_сервера" "$OISM_SERVER_TIMER")
            ;;
        esac
        ;;
    13)
        JU+=("дата_и_время.ккт" "$CURRENT_DATE $CURRENT_TIME")
        ;;
    78)
        case "$QUERY_NUMBER" in
        1)
            JU+=("обмен_с_фн.рег_номер" "$REG_NUMBER")
            ;;
        2)
            JU+=("обмен_с_фн.статус_фн" "$FN_STATE")
            JU+=("обмен_с_фн.статус_документа" "$DOC_STATE")
            JU+=("обмен_с_фн.флаги_предупреждений" "$WARNING_FLAGS")
            ;;
        3)
            JU+=("обмен_с_фн.номер_последнего_документа" "$LAST_DOC_NUMBER")
            ;;
        4)
            JU+=("обмен_с_фн.дата_регистрации" "$REG_DATE")
            JU+=("обмен_с_фн.время_регистрации" "$REG_TIME")
            ;;
        5)
            JU+=("обмен_с_фн.последний_фд_регистрации" "$LAST_REG_FD")
            ;;
        6)
            JU+=("обмен_с_фн.номер_смены" "$SHIFT_NUMBER")
            JU+=("обмен_с_фн.номер_чека" "$CHECK_NUMBER")
            JU+=("обмен_с_фн.статус_смены" "$SHIFT_STATUS")
            ;;
        7)
            JU+=("обмен_с_фн.статус_обмена" "$EXCHANGE_STATUS")
            JU+=("обмен_с_фн.число_документов" "$DOC_COUNT")
            JU+=("обмен_с_фн.номер_первого_документа" "$FIRST_DOC_NUMBER")
            JU+=("обмен_с_фн.дата_первого_документа" "$FIRST_DOC_DATE")
            JU+=("обмен_с_фн.время_первого_документа" "$FIRST_DOC_TIME")
            ;;
        11)
            JU+=("обмен_с_фн.квитанция.получена" "$DOCUMENT_DATA")
            ;;
        12)
            JU+=("обмен_с_фн.данные_квитанции" "$RECEIPT_DATA")
            ;;
        13)
            JU+=("обмен_с_фн.данные_ошибки" "$ERROR_DATA")
            ;;
        14)
            JU+=("обмен_с_фн.версии.фн_прошивка" "$FN_VERSION")
            JU+=("обмен_с_фн.версии.фн_версия" "$FN_TYPE")
            JU+=("обмен_с_фн.версии.ффд" "$FFD_VERSION")
            JU+=("обмен_с_фн.версии.ккт" "$KKT_VERSION")
            JU+=("обмен_с_фн.версии.ффд_ккт" "$FFD_KKT_VERSION")
            ;;
        15)
            JU+=("обмен_с_фн.документ_tlv.тип" "$DOC_TYPE")
            JU+=("обмен_с_фн.документ_tlv.размер" "$DOC_SIZE")
            ;;
        16)
            JU+=("обмен_с_фн.данные_tlv" "$TLV_DATA")
            ;;
        17)
            JU+=("обмен_с_фн.номер_фд_начала" "$FD_START_NUMBER")
            ;;
        18)
            JU+=("обмен_с_фн.ресурс_памяти.приблизительное_число_документов" "$APPROX_DOC_COUNT")
            JU+=("обмен_с_фн.ресурс_памяти.свободно_кб" "$FREE_MEMORY_KB")
            ;;
        19)
            JU+=("обмен_с_фн.данные_регистрации.номер_фд" "$FD_NUMBER")
            JU+=("обмен_с_фн.данные_регистрации.номер_фп" "$FP_NUMBER")
            JU+=("обмен_с_фн.данные_регистрации.дата_документа" "$DOC_DATE")
            JU+=("обмен_с_фн.данные_регистрации.время_документа" "$DOC_TIME")
            ;;
        20)
            JU+=("обмен_с_фн.номер_регистрации" "$REGISTRATION_NUMBER")
            ;;
        21)
            JU+=("обмен_с_фн.данные_регистрации_tlv" "$TLV_REGISTRATION_DATA")
            ;;
        22)
            JU+=("обмен_с_фн.версии_ффд.текущая" "$CURRENT_FFD_DESC")
            JU+=("обмен_с_фн.версии_ффд.максимальная" "$MAX_FFD_DESC")
            ;;
        23) ;;
        esac
        ;;
    A6)
        JU+=("информация_кассы.официальное_название" "$LEGAL_NAME")
        JU+=("информация_кассы.адрес" "$ADDRESS")
        JU+=("информация_кассы.инн" "$INN")
        JU+=("информация_кассы.номер_фн" "$FN_NUMBER")
        JU+=("информация_кассы.версия_ффд" "$FFD_VERSION")
        JU+=("информация_кассы.номер_регистрации" "$REGISTRY_NUMBER")
        JU+=("информация_кассы.налоговые_режимы" "$TAX_MODES")
        JU+=("информация_кассы.автономный" "$AUTONOMIC")
        JU+=("информация_кассы.провайдер_офд.название" "$OFD_PROVIDER_NAME")
        JU+=("информация_кассы.провайдер_офд.инн" "$OFD_PROVIDER_INN")
        JU+=("информация_кассы.провайдер_офд.хост_сервера" "$OFD_PROVIDER_SERVER_HOST")
        JU+=("информация_кассы.провайдер_офд.порт_сервера" "$OFD_PROVIDER_SERVER_PORT")
        JU+=("информация_кассы.провайдер_офд.url_проверки" "$OFD_PROVIDER_CHECK_URL")
        JU+=("информация_кассы.режим_работы" "$WORK_MODE")
        JU+=("информация_кассы.дата_окончания_ккт" "$KKT_EXPIRE_DATE")
        JU+=("информация_кассы.локальное_время" "$LOCAL_DATE $LOCAL_TIME")
        JU+=("информация_кассы.место_оплаты" "$PAYMENT_PLACE")
        ;;
    A7)
        JU+=("информация_регистра.uuid" "$UUID")
        JU+=("информация_регистра.модель" "$MODEL")
        JU+=("информация_регистра.название_модели" "$MODEL_NAME")
        JU+=("информация_регистра.читаемая_модель" "$READABLE_MODEL")
        JU+=("информация_регистра.название_реестра_ккт" "$KKT_REGISTRY_NAME")
        JU+=("информация_регистра.заводской_номер_ккт" "$KKT_FACTORY_NUMBER")
        JU+=("информация_регистра.фн_активен" "$IS_FN")
        ;;
    AA)
        case "$QUERY_NUMBER" in
        1)
            JU+=("активированные_услуги.ключ_активирован" "$KEY_ACTIVATED")
            ;;
        2)
            JU+=("активированные_услуги.статус_услуги.номер" "$SERVICE_NUMBER")
            JU+=("активированные_услуги.статус_услуги.описание" "$SERVICE_DESCRIPTION")
            if [[ -n "$SERVICE_END_DATE" ]]; then
                JU+=("активированные_услуги.статус_услуги.дата_окончания" "$SERVICE_END_DATE")
            fi
            ;;
        3)
            JU+=("активированные_услуги.все_услуги" "[$(format_array "${ALL_SERVICES[@]}")]")
            ;;
        4)
            JU+=("активированные_услуги.версия_протокола" "$PROTOCOL_VERSION")
            JU+=("активированные_услуги.число_услуг" "$SERVICE_COUNT")
            JU+=("активированные_услуги.список_услуг" "[$(format_array "${SERVICE_LIST[@]}")]")
            JU+=("активированные_услуги.описание_услуг" "[$(format_array "${SERVICE_LIST_DESCRIPTION_SPACES[@]}")]")
            ;;
        esac
        ;;
    esac
}

print_data() {
    case "$COMMAND_CODE" in
    00)
        echo -e "Статус ККТ:"
        echo -e "  Биты фатального статуса: $FATAL_STATUS"
        interpret_bit_flags "$FATAL_STATUS" "фатальные_статусы"
        echo -e ""
        echo -e "  Биты текущих флагов: $CURRENT_FLAGS"
        interpret_bit_flags "$CURRENT_FLAGS" "текущие_флаги"
        echo -e ""
        echo -e "  Статус документа: $DOCUMENT_STATUS"
        interpret_document_status "$DOCUMENT_STATUS"
        echo -e ""
        ;;
    01)
        case "$QUERY_NUMBER" in
        1)
            echo -e "Номер текущей смены: $CURRENT_SHIFT_NUMBER"
            ;;
        2)
            echo -e "Номер следующего чека: $NEXT_CHECK_NUMBER"
            ;;
        3)
            echo -e "Суммы продаж по типам платежа: ${SALES_SUM_BY_PAYMENT_TYPE[*]}"
            ;;
        4)
            echo -e "Количество оплат по продажам: ${PAYMENT_COUNT_BY_SALES[*]}"
            ;;
        5)
            echo -e "Суммы возвратов по типам платежа: ${REFUND_SUM_BY_PAYMENT_TYPE[*]}"
            ;;
        6)
            echo -e "Количество оплат по возвратам: ${PAYMENT_COUNT_BY_REFUNDS[*]}"
            ;;
        7)
            echo -e "Количество оформленных чеков по типам операций: ${CHECK_COUNT_BY_OPERATION_TYPE[*]}"
            ;;
        8)
            echo -e "Суммы по оформленным чекам: ${CHECK_SUM_BY_OPERATION_TYPE[*]}"
            ;;
        9)
            echo -e "Суммы по скидкам: ${DISCOUNT_SUMS[*]}"
            ;;
        10)
            echo -e "Суммы налогов по продажам: ${TAX_SUM_BY_SALES[*]}"
            ;;
        11)
            echo -e "Суммы налогов по возвратам: ${TAX_SUM_BY_REFUNDS[*]}"
            ;;
        12)
            echo -e "Данные последнего X-/Z-отчёта:"
            echo -e "  Операционный счётчик: $REPORT_OPERATIONAL_COUNTER"
            echo -e "  Номер документа: $REPORT_DOCUMENT_NUMBER"
            echo -e "  Сумма наличных: $REPORT_CASH_SUM"
            echo -e "  Количество продаж: $REPORT_SALES_COUNT"
            echo -e "  Сумма продаж: $REPORT_SALES_SUM"
            echo -e "  Количество возвратов: $REPORT_REFUND_COUNT"
            echo -e "  Сумма возвратов: $REPORT_REFUND_SUM"
            echo -e "  Количество аннулированных: $REPORT_CANCELLED_COUNT"
            echo -e "  Сумма аннулированных: $REPORT_CANCELLED_SUM"
            echo -e "  Количество депозитов: $REPORT_DEPOSIT_COUNT"
            echo -e "  Сумма депозитов: $REPORT_DEPOSIT_SUM"
            echo -e "  Количество снятий: $REPORT_WITHDRAWAL_COUNT"
            echo -e "  Сумма снятий: $REPORT_WITHDRAWAL_SUM"
            ;;
        13)
            echo -e "Суммы продаж по секциям: ${SALES_SUM_BY_SECTION[*]}"
            ;;
        14)
            echo -e "Суммы возвратов по секциям: ${REFUND_SUM_BY_SECTION[*]}"
            ;;
        15)
            echo -e "Количество чеков по покупкам: $PURCHASE_CHECK_COUNT"
            echo -e "Количество чеков возвратов по покупкам: $PURCHASE_REFUND_CHECK_COUNT"
            ;;
        16)
            echo -e "Суммы покупок по типам платежа: ${PURCHASE_SUM_BY_PAYMENT_TYPE[*]}"
            ;;
        17)
            echo -e "Суммы возвратов покупок по типам платежа: ${REFUND_PURCHASE_SUM_BY_PAYMENT_TYPE[*]}"
            ;;
        18)
            echo -e "Количество коррекций: $CORRECTION_COUNT"
            echo -e "Сумма коррекций наличными: $CORRECTION_SUM_CASH"
            echo -e "Сумма коррекций без наличных: $CORRECTION_SUM_CASHLESS"
            ;;
        *)
            echo -e "Неизвестный номер запроса для 0x01: $QUERY_NUMBER"
            ;;
        esac
        ;;
    02)
        case "$QUERY_NUMBER" in
        1)
            echo -e "Заводской номер ККТ: $FACTORY_NUMBER"
            ;;
        2)
            echo -e "Идентификатор прошивки: $FIRMWARE_ID"
            ;;
        3)
            echo -e "ИНН: $INN"
            ;;
        4)
            echo -e "Регистрационный номер ККТ: $REGISTRATION_NUMBER"
            ;;
        5)
            echo -e "Дата и время последней фискальной операции: $LAST_FISCAL_OPERATION_DATE $LAST_FISCAL_OPERATION_TIME"
            ;;
        6)
            echo -e "Дата регистрации / перерегистрации: $REGISTRATION_DATE"
            ;;
        7)
            echo -e "Сумма наличных в денежном ящике: $CASH_AMOUNT"
            ;;
        8)
            echo -e "Номер следующего документа: $NEXT_DOCUMENT_NUMBER"
            ;;
        9)
            echo -e "Номер смены регистрации: $SHIFT_NUMBER"
            ;;
        10)
            echo -e "Номер следующего X отчета: $NEXT_X_REPORT_NUMBER"
            ;;
        11)
            echo -e "Текущий операционный счетчик: $OPERATIONAL_COUNTER"
            ;;
        12)
            echo -e "Нарастающий итог по продажам: $CUMULATIVE_TOTAL_SALES"
            echo -e "Нарастающий итог по возвратам: $CUMULATIVE_TOTAL_REFUNDS"
            echo -e "Нарастающий итог по покупкам: $CUMULATIVE_TOTAL_PURCHASES"
            echo -e "Нарастающий итог по возвратам покупок: $CUMULATIVE_TOTAL_PURCHASE_REFUNDS"
            ;;
        14)
            echo -e "Дата окончания временного ресурса ФН: $FN_RESOURCE_END_DATE"
            ;;
        15)
            echo -e "Тип прошивки: $FIRMWARE_TYPE"
            ;;
        16)
            echo -e "Размер бумаги текущего дизайна: $PAPER_SIZE"
            ;;
        17)
            echo -e "Дата и время открытия смены: $SHIFT_OPEN_DATE $SHIFT_OPEN_TIME"
            ;;
        18)
            echo -e "Количество символов в строке: $LINE_CHAR_COUNT"
            ;;
        19)
            echo -e "Содержание регистра CID SD карты: $CID_REGISTER_CONTENT"
            ;;
        20)
            echo -e "Содержание регистра CSD SD карты: $CSD_REGISTER_CONTENT"
            ;;
        21)
            echo -e "Модель устройства: $DEVICE_MODEL"
            ;;
        22)
            echo -e "Битовая маска поддерживаемых интерфейсов и устройств: $INTERFACE_MASK"
            ;;
        23)
            echo -e "Система налогообложения:"
            interpret_bit_flags "$TAX_SYSTEM_BITS" "система_налогообложения"
            echo -e "Режим работы:"
            interpret_bit_flags "$OPERATION_MODE_BITS" "режим_работы"
            echo -e "Дополнительный режим работы:"
            interpret_bit_flags "$ADDITIONAL_MODE_BITS" "дополнительный_режим"
            ;;
        24)
            echo -e "Максимальное количество дополнительных строк в начале: $MAX_ADDITIONAL_LINES_START"
            echo -e "Максимальное количество дополнительных строк в конце: $MAX_ADDITIONAL_LINES_END"
            ;;
        40)
            echo -e "Состояние перехода на НДС 20%: $NDS_STATE_DESCRIPTION"
            ;;
        70)
            echo -e "Рабочий идентификатор прошивки: $WORKING_FIRMWARE_ID"
            ;;
        *)
            echo -e "Неизвестный номер запроса для get-kkt-info: $QUERY_NUMBER"
            ;;
        esac
        ;;
    04)
        echo -e "Состояние принтера:"
        interpret_bit_flags "$PRINTER_STATE" "состояние_принтера"
        ;;
    05)
        case "$QUERY_NUMBER" in
        1)
            echo -e "Напряжение питания: $VOLTAGE мВ"
            ;;
        2)
            echo -e "Температура термоголовки: $TEMPERATURE °C"
            ;;
        3)
            echo -e "Количество отрезов резчика: $CUTTER_COUNT"
            ;;
        4)
            echo -e "Ресурс термоголовки: $THERMAL_RESOURCE мм"
            ;;
        7)
            echo -e "Напряжение на батарейке: $BATTERY_VOLTAGE мВ"
            ;;
        8)
            echo -e "Количество отрезов резчика (необнуляемое): $CUTTER_COUNT_NON_RESET"
            ;;
        9)
            echo -e "Ресурс термоголовки (необнуляемый): $THERMAL_RESOURCE_NON_RESET"
            ;;
        10)
            echo -e "Тип ПУ: $DEVICE_TYPE"
            ;;
        11)
            echo -e "Версия BIOS ПУ: $BIOS_VERSION"
            ;;
        12)
            echo -e "Серийный номер ПУ: $SERIAL_NUMBER"
            ;;
        13)
            echo -e "Дата изменения BIOS ПУ: $BIOS_CHANGE_DATE"
            ;;
        *)
            echo -e "Неизвестный номер запроса для get-service-info: $QUERY_NUMBER"
            ;;
        esac
        ;;
    11)
        case "$QUERY_NUMBER" in
        30)
            local line_index="${COMMAND_PARAMS[1]}"
            case "$line_index" in
            0)
                echo -e "  Наименование организации, 1-ая строка: $ORG_INFO_LINE_1"
                ;;
            1)
                echo -e "  Наименование организации, 2-ая строка: $ORG_INFO_LINE_2"
                ;;
            2)
                echo -e "  Адрес организации, 1-ая строка: $ORG_INFO_LINE_3"
                ;;
            3)
                echo -e "  Адрес организации, 2-ая строка: $ORG_INFO_LINE_4"
                ;;
            esac
            ;;
        40)
            echo -e "Название налога: ${TAX_TITLES[*]}"
            ;;
        41)
            echo -e "Ставка налога: ${TAX_PERCENTAGES[*]}"
            ;;
        42)
            echo -e "Название налоговой группы в чеке: $TAX_GROUP_NAME"
            ;;
        73)
            echo -e "IP-адрес ККТ: $IP_ADDRESS"
            ;;
        74)
            echo -e "Маска подсети: $SUBNET_MASK"
            ;;
        75)
            echo -e "IP-адрес шлюза: $GATEWAY_IP"
            ;;
        76)
            echo -e "IP-адрес DNS: $DNS_IP"
            ;;
        77)
            echo -e "Адрес сервера ОФД для отправки документов: $OFD_SERVER_ADDRESS"
            ;;
        78)
            echo -e "Порт сервера ОФД: $OFD_SERVER_PORT"
            ;;
        79)
            echo -e "Таймер ФН: $FN_TIMER"
            ;;
        80)
            echo -e "Таймер С: $TIMER_S"
            ;;
        81)
            echo -e "Наименование ОФД: $OFD_NAME"
            ;;
        91)
            echo -e "IP-адрес ОКП-сервера: $OKP_SERVER_IP"
            ;;
        92)
            echo -e "Порт ОКП-сервера: $OKP_SERVER_PORT"
            ;;
        93)
            echo -e "URL ОКП-сервера: $OKP_SERVER_URL"
            ;;
        94)
            echo -e "Таймер ОКП-сервера: $OKP_SERVER_TIMER"
            ;;
        201)
            echo -e "IP-адрес ОИСМ-сервера: $OISM_SERVER_IP"
            ;;
        202)
            echo -e "Порт ОИСМ-сервера: $OISM_SERVER_PORT"
            ;;
        203)
            echo -e "URL ОИСМ-сервера: $OISM_SERVER_URL"
            ;;
        204)
            echo -e "Таймер ОИСМ-сервера: $OISM_SERVER_TIMER"
            ;;
        *)
            echo -e "Неизвестный номер запроса для 11: $QUERY_NUMBER"
            ;;
        esac
        ;;
    13)
        echo -e "Дата/Время ККТ: $CURRENT_DATE $CURRENT_TIME"
        ;;
    78)
        case "$QUERY_NUMBER" in
        1)
            echo -e "Регистрационный номер ФН: $REG_NUMBER"
            ;;
        2)
            echo -e "Статус ФН:"
            interpret_fn_status "$FN_STATE"
            echo -e "  Состояние текущего документа: $DOC_STATE"
            interpret_open_document_type "$DOC_STATE"
            echo -e "  Флаги предупреждения:"
            interpret_bit_flags "$WARNING_FLAGS" "предупреждающие_флаги"
            ;;
        3)
            echo -e "Номер последнего фискального документа: $LAST_DOC_NUMBER"
            ;;
        4)
            echo -e "Дата регистрации ФН: $REG_DATE"
            echo -e "Время регистрации ФН: $REG_TIME"
            ;;
        5)
            echo -e "Номер ФД последней регистрации: $LAST_REG_FD"
            ;;
        6)
            echo -e "Состояние текущей смены:"
            echo -e "  Номер смены: $SHIFT_NUMBER"
            echo -e "  Номер чека в смене: $CHECK_NUMBER"
            echo -e "  Статус смены: $SHIFT_STATUS"
            ;;
        7)
            echo -e "Состояние обмена с ОФД:"
            interpret_bit_flags "$EXCHANGE_STATUS" "статус_обмена"
            echo -e "  Количество документов для передачи в ОФД: $DOC_COUNT"
            echo -e "  Номер первого документа для передачи в ОФД: $FIRST_DOC_NUMBER"
            echo -e "  Дата первого документа для передачи в ОФД: $FIRST_DOC_DATE"
            echo -e "  Время первого документа для передачи в ОФД: $FIRST_DOC_TIME"
            ;;
        11)
            if [[ "$RECEIPT_FLAG" == "1" ]]; then
                echo -e "Квитанция получена. Данные документа: $DOCUMENT_DATA"
            else
                echo -e "Документ не получен."
            fi
            ;;
        12)
            echo -e "Квитанция о получении документа из архива: $RECEIPT_DATA"
            ;;
        13)
            echo -e "Последние ошибки ФН: $ERROR_DATA"
            ;;
        14)
            echo -e "Версии:"
            echo -e "  Версия прошивки ФН: $FN_VERSION"
            echo -e "  Версия ФН: $FN_TYPE ($FN_TYPE_DESC)"
            echo -e "  Версия ФФД: $FFD_VERSION ($FFD_VERSION_DESC)"
            echo -e "  Версия ККТ: $KKT_VERSION"
            echo -e "  Версия ФФД ККТ: $FFD_KKT_VERSION"
            ;;
        15)
            echo -e "Документ из архива в формате TLV:"
            echo -e "  Тип документа: $DOC_TYPE"
            echo -e "  Размер данных документа: $DOC_SIZE байт"
            ;;
        16)
            echo -e "Данные TLV документа: $TLV_DATA"
            ;;
        17)
            echo -e "Номер ФД начала смены: $FD_START_NUMBER"
            ;;
        18)
            echo -e "Ресурс свободной памяти в ФН:"
            echo -e "  Приблизительное количество документов 5-летнего хранения: $APPROX_DOC_COUNT"
            echo -e "  Размер свободной области для записи документов 30-дневного хранения: $FREE_MEMORY_KB КБ"
            ;;
        19)
            echo -e "Данные последней регистрации/перерегистрации:"
            echo -e "  Номер ФД: $FD_NUMBER"
            echo -e "  Номер ФП: $FP_NUMBER"
            echo -e "  Дата документа: $DOC_DATE"
            echo -e "  Время документа: $DOC_TIME"
            ;;
        20)
            echo -e "Номер регистрации: $REGISTRATION_NUMBER"
            ;;
        21)
            echo -e "Данные регистрации в TLV: $TLV_REGISTRATION_DATA"
            ;;
        22)
            echo -e "Версия ФФД ФН:"
            echo -e "  Текущая версия ФФД: $CURRENT_FFD_DESC"
            echo -e "  Максимальная поддерживаемая версия ФФД: $MAX_FFD_DESC"
            ;;
        23) ;;
        *)
            echo -e "Ответ на обмен информацией с ФН:"
            echo -e "  Номер запроса: $QUERY_NUMBER"
            echo -e "  Данные: ${parsed_fields[*]}"
            ;;
        esac
        ;;
    A6)
        echo -e "Название организации: $LEGAL_NAME"
        echo -e "Адрес: $ADDRESS"
        echo -e "ИНН: $INN"
        echo -e "Регистрационный номер ФН: $FN_NUMBER"
        echo -e "Версия ФФД: $FFD_VERSION"
        echo -e "Регистрационный номер ККТ: $REGISTRY_NUMBER"
        echo -e "Режимы налогообложения: $TAX_MODES"
        echo -e "Автономный режим: $AUTONOMIC"
        echo -e "Провайдер ОФД - Название: $OFD_PROVIDER_NAME"
        echo -e "Провайдер ОФД - ИНН: $OFD_PROVIDER_INN"
        echo -e "Провайдер ОФД - Сервер хост: $OFD_PROVIDER_SERVER_HOST"
        echo -e "Провайдер ОФД - Сервер порт: $OFD_PROVIDER_SERVER_PORT"
        echo -e "Провайдер ОФД - URL проверки: $OFD_PROVIDER_CHECK_URL"
        echo -e "Режим работы: $WORK_MODE"
        echo -e "Дата истечения KKT: $KKT_EXPIRE_DATE"
        echo -e "Локальное время: $LOCAL_DATE $LOCAL_TIME"
        echo -e "Место оплаты: $PAYMENT_PLACE"
        ;;
    A7)
        echo -e "UUID: $UUID"
        echo -e "Модель: $MODEL"
        echo -e "Название модели: $MODEL_NAME"
        echo -e "Читаемая модель: $READABLE_MODEL"
        echo -e "Название реестра KKT: $KKT_REGISTRY_NAME"
        echo -e "Заводской номер KKT: $KKT_FACTORY_NUMBER"
        echo -e "ФН активен: $IS_FN"
        ;;
    AA)
        case "$QUERY_NUMBER" in
        1)
            echo -e "Ключ активирован: $KEY_ACTIVATED"
            ;;
        2)
            echo -e "Статус услуги:"
            echo -e "  Номер услуги: $SERVICE_NUMBER"
            interpret_bit_flags "$SERVICE_STATUS" "статус_услуг"
            echo -e "  Описание услуги: $SERVICE_DESCRIPTION"
            if [[ -n "$SERVICE_END_DATE" ]]; then
                echo -e "  Дата окончания действия услуги: $SERVICE_END_DATE"
            fi
            ;;
        3)
            echo -e "Все активированные услуги:"
            for service in "${ALL_SERVICES[@]}"; do
                echo -e "  $service"
            done
            ;;
        4)
            echo -e "Версия протокола: $PROTOCOL_VERSION"
            echo -e "Количество известных услуг: $SERVICE_COUNT"
            echo -e "Список услуг :"
            tabs 1,+30
            for ((i = 0; i < ${#SERVICE_LIST_DESCRIPTION_TABS[@]}; i++)); do
                echo -e "${SERVICE_LIST_DESCRIPTION_TABS[i]}"
            done
            tabs -8
            ;;
        *)
            echo -e "Неизвестный номер запроса для activate-services: $QUERY_NUMBER"
            ;;
        esac
        ;;
    *)
        echo -e "Неизвестная команда: $COMMAND_CODE"
        ;;
    esac
}

execute_single_command() {
    if [[ "$1" ]]; then
        COMMAND_CODE="$1"
        shift
        COMMAND_PARAMS=("$@")
    fi

    if create_command_packet; then
        if send_command_packet; then
            if read_and_process_command_response; then
                retrieve_data
                if [[ "$COMMAND_CODE_ARGUMENT" == "all" ]]; then
                    update_json
                fi
                if ((!NO_PRINT_DATA)); then
                    print_data
                fi
            fi
        fi
    fi
    unset COMMAND_PARAMS
}

fetch_and_display_kkt_status() {
    execute_single_command "00"
}

fetch_and_display_shift_counters() {
    echo -e "Сменные счетчики и регистры:"
    for i in {1..18}; do
        execute_single_command "01" "$i"
    done
}

fetch_and_display_factory_number_kkt() {
    execute_single_command "02" "1"
}

fetch_and_display_firmware_version() {
    execute_single_command "02" "2"
}

fetch_and_display_inn() {
    execute_single_command "02" "3"
}

fetch_and_display_registration_number_kkt() {
    execute_single_command "02" "4"
}

fetch_and_display_fn_resource_end_date() {
    execute_single_command "02" "14"
}

fetch_and_display_tax_system_and_modes() {
    execute_single_command "02" "23"
}

fetch_and_display_nds_state() {
    echo -e "Функция НДС 20%:"
    execute_single_command "02" "40"
}

fetch_and_display_kkt_info() {
    echo -e "Сведения о ККТ:"
    for i in {1..12} {14..18} {21..24} 40 70; do
        execute_single_command "02" "$i"
    done
}

fetch_and_display_printer_status() {
    execute_single_command "04"
}

fetch_and_display_battery_voltage() {
    execute_single_command "05" "7"
}

fetch_and_display_kkt_service_info() {
    echo -e "Сервисная информация:"
    for i in {7..13}; do
        execute_single_command "05" "$i"
    done
}

fetch_and_display_organization_info() {
    echo -e "Наименование и адрес организации:"
    for i in {0..3}; do
        execute_single_command "11" "30" "$i"
    done
}

fetch_and_display_tax_settings() {
    unset TAX_TITLES TAX_PERCENTAGES
    declare -a TAX_TITLES TAX_PERCENTAGES

    for ((i = 0; i <= 5; i++)); do
        execute_single_command "11" "40" "$i"
        execute_single_command "11" "41" "$i"
    done

    tabs 1,+10,+18
    echo -e "\tНазвание налога\tСтавка налога"
    for ((i = 0; i < ${#TAX_TITLES[@]}; i++)); do
        echo -e "Налог $i :\t${TAX_TITLES[$i]}\t${TAX_PERCENTAGES[$i]}"
        JU+=("tax_settings.tax_$i" "${TAX_TITLES[$i]},${TAX_PERCENTAGES[$i]}")
    done
    tabs -8
}

fetch_and_display_network_settings() {
    NO_PRINT_DATA=1
    for i in {73..81} {91..94} {201..204}; do
        execute_single_command "11" "$i" "0"
    done
    NO_PRINT_DATA=0

    display_network_configuration
}

fetch_and_display_kkt_date_time() {
    execute_single_command "13"
}

fetch_and_display_registration_number_fn() {
    execute_single_command "78" "1"
}

fetch_and_display_fn_status() {
    execute_single_command "78" "2"
}

fetch_and_display_ofd_exchange_status() {
    execute_single_command "78" "7"
}

fetch_and_display_fn_exchange_info() {
    echo -e "Обмен информацией с ФН:"
    for i in {1..7} 14 17 19 22; do
        execute_single_command "78" "$i"
    done
}

fetch_and_display_fn_counters() {
    echo -e "Счетчики ФН:"
    for type in 1 0; do
        for calc_type in 1 2 3 4; do
            execute_single_command "78" "23" "$type" "$calc_type"
        done
    done
    display_fn_counters
}

fetch_and_display_cash_info() {
    echo -e "Данные отчёта cash_info:"
    execute_single_command "A6"
}

fetch_and_display_register_info() {
    echo -e "Данные отчёта register:"
    execute_single_command "A7"
}

fetch_and_display_all_activated_services() {
    echo -e "Активированные услуги:"
    execute_single_command "AA" "4"
}

display_network_configuration() {
    tabs 1,+35
    echo -e "Сетевые установки и серверы"
    echo -e "IP-адрес : ${IP_ADDRESS}"
    echo -e "MAC-адрес : "
    echo -e "URL ОФД : ${OFD_SERVER_ADDRESS}"
    echo -e "IP-адрес ОФД : ${GATEWAY_IP}\tПорт ОФД : ${OFD_SERVER_PORT}"
    echo -e "Название ОФД : ${OFD_NAME}"
    echo -e "Таймер ФН : ${FN_TIMER}   Таймер С : ${TIMER_S}"
    echo -e "URL ОИСМ : ${OISM_SERVER_URL}"
    echo -e "IP-адрес ОИСМ : ${OISM_SERVER_IP}\tПорт ОИСМ : ${OISM_SERVER_PORT}"
    echo -e "Таймер ОИСМ : ${OISM_SERVER_TIMER}   Таймер ОКП : ${OKP_SERVER_TIMER}"
    echo -e "URL ОКП : ${OKP_SERVER_URL}"
    echo -e "IP-адрес ОКП : ${OKP_SERVER_IP}\tПорт ОКП : ${OKP_SERVER_PORT}"
    tabs -8
}

display_fn_counters() {
    local counter_titles=(
        "Кол-во операций данного признака расчета"
        "Итоговая сумма расчета, указанного в чеке (БСО)"
        "Итоговая сумма по чекам (БСО) наличными"
        "Итоговая сумма по чекам (БСО) электронными"
        "Итоговая сумма по чекам (БСО) предоплатой (зачетом аванса)"
        "Итоговая сумма по чекам (БСО) постоплатой (в кредит)"
        "Итоговая сумма по чеку (БСО) встречным предоставлением"
        "Итоговая сумма НДС чека по ставке 18%"
        "Итоговая сумма НДС чека по ставке 10%"
        "Итоговая сумма НДС чека по ставке 0%"
        "Итоговая сумма расчета по чеку без НДС"
        "Итоговая сумма НДС чека по ставке 18/118"
        "Итоговая сумма НДС чека по ставке 10/110"
        "Кол-во чеков коррекций данного признака расчета"
        "Итоговая сумма по чекам коррекции"
    )

    local counter_keys=(
        "количество_операций_по_признаку_расчета"
        "итоговая_сумма_расчета_указанного_в_чеке"
        "итоговая_сумма_наличными"
        "итоговая_сумма_электронными"
        "итоговая_сумма_предоплатой"
        "итоговая_сумма_постоплатой"
        "итоговая_сумма_встречным_предоставлением"
        "итоговая_сумма_ндс_18"
        "итоговая_сумма_ндс_10"
        "итоговая_сумма_ндс_0"
        "итоговая_сумма_без_ндс"
        "итоговая_сумма_ндс_18_118"
        "итоговая_сумма_ндс_10_110"
        "количество_чеков_коррекции"
        "итоговая_сумма_чеков_коррекции"
    )

    tabs 1,+65,+17,+17,+17

    echo -e "Счетчики всего ФН:"
    echo -e "\tПриход\tВозврат прихода\tРасход\tВозврат расхода"
    for ((i = 0; i < 15; i++)); do
        local v1="${FULL_FN_COUNTERS_INCOME[i]}"
        local v2="${FULL_FN_COUNTERS_INCOME_RETURN[i]}"
        local v3="${FULL_FN_COUNTERS_EXPENSE[i]}"
        local v4="${FULL_FN_COUNTERS_EXPENSE_RETURN[i]}"
        echo -e "${counter_titles[i]}:\t$v1\t$v2\t$v3\t$v4"
        JU+=("счетчики_фн.весь_фн.${counter_keys[i]}" "[\"$v1\",\"$v2\",\"$v3\",\"$v4\"]")
    done

    echo

    echo -e "Сменные счетчики:"
    echo -e "\tПриход\tВозврат прихода\tРасход\tВозврат расхода"
    for ((i = 0; i < 15; i++)); do
        local v1="${SHIFT_COUNTERS_INCOME[i]}"
        local v2="${SHIFT_COUNTERS_INCOME_RETURN[i]}"
        local v3="${SHIFT_COUNTERS_EXPENSE[i]}"
        local v4="${SHIFT_COUNTERS_EXPENSE_RETURN[i]}"
        echo -e "${counter_titles[i]}:\t$v1\t$v2\t$v3\t$v4"
        JU+=("счетчики_фн.сменные.${counter_keys[i]}" "[\"$v1\",\"$v2\",\"$v3\",\"$v4\"]")
    done

    tabs -8

    JU+=("обмен_с_фн.счетчики_фн.приход" "[$(format_array "${FULL_FN_COUNTERS_INCOME[@]}")]")
    JU+=("обмен_с_фн.счетчики_фн.возврат_прихода" "[$(format_array "${FULL_FN_COUNTERS_INCOME_RETURN[@]}")]")
    JU+=("обмен_с_фн.счетчики_фн.расход" "[$(format_array "${FULL_FN_COUNTERS_EXPENSE[@]}")]")
    JU+=("обмен_с_фн.счетчики_фн.возврат_расхода" "[$(format_array "${FULL_FN_COUNTERS_EXPENSE_RETURN[@]}")]")
    JU+=("обмен_с_фн.счетчики_смены.приход" "[$(format_array "${SHIFT_COUNTERS_INCOME[@]}")]")
    JU+=("обмен_с_фн.счетчики_смены.возврат_прихода" "[$(format_array "${SHIFT_COUNTERS_INCOME_RETURN[@]}")]")
    JU+=("обмен_с_фн.счетчики_смены.расход" "[$(format_array "${SHIFT_COUNTERS_EXPENSE[@]}")]")
    JU+=("обмен_с_фн.счетчики_смены.возврат_расхода" "[$(format_array "${SHIFT_COUNTERS_EXPENSE_RETURN[@]}")]")
}

fetch_and_display_full_kkt_info() {
    echo "Сбор полной информации о ККТ..."
    echo

    fetch_and_display_kkt_date_time
    echo

    fetch_and_display_kkt_status
    echo

    fetch_and_display_all_activated_services
    echo

    fetch_and_display_kkt_service_info
    echo

    fetch_and_display_kkt_info
    echo

    fetch_and_display_fn_exchange_info
    echo

    fetch_and_display_network_settings
    echo

    fetch_and_display_organization_info
    echo

    fetch_and_display_fn_counters
    echo

    fetch_and_display_shift_counters
    echo

    fetch_and_display_register_info
    echo

    fetch_and_display_cash_info
}

fetch_and_display_relevant_kkt_info() {
    echo "Показ релевантной информации..."
    echo

    fetch_and_display_kkt_date_time
    echo

    fetch_and_display_tax_system_and_modes
    echo

    fetch_and_display_nds_state
    echo

    fetch_and_display_all_activated_services
}

fetch_and_display_tp_kkt_info() {
    echo -ne "${FG_BRIGHT_CYAN}"
    fetch_and_display_firmware_version
    echo -ne "${NC}"

    echo -ne "${FG_GREEN}"
    fetch_and_display_factory_number_kkt
    fetch_and_display_registration_number_kkt
    fetch_and_display_registration_number_fn
    fetch_and_display_inn
    echo -ne "${NC}"

    fetch_and_display_battery_voltage

    echo -ne "${FG_BRIGHT_BLUE}"
    fetch_and_display_ofd_exchange_status
    echo -ne "${NC}"

    echo -ne "${FG_YELLOW}"
    fetch_and_display_network_settings
    echo -ne "${NC}"

    echo -ne "${FG_MAGENTA}"
    fetch_and_display_organization_info
    echo -ne "${NC}"

    echo -ne "${FG_CYAN}"
    fetch_and_display_tax_system_and_modes
    echo -ne "${NC}"

    fetch_and_display_fn_resource_end_date

    echo -ne "${FG_BRIGHT_MAGENTA}"
    fetch_and_display_all_activated_services
    echo -ne "${NC}"
}

open_correction_check() {
    local operator_name="$1"
    local cash_amount="$2"
    local electronic_amount="$3"
    local prepayment_amount="$4"
    local postpayment_amount="$5"
    local counterprovided_amount="$6"
    local correction_type="$7"
    local correction_date="$8"
    local correction_doc_number="$9"

    execute_single_command "58" "$operator_name" "$cash_amount" "$electronic_amount" "$prepayment_amount" "$postpayment_amount" "$counterprovided_amount" "$correction_type" "$correction_date" "$correction_doc_number"
}

execute_command() {
    case "$COMMAND_CODE_ARGUMENT" in
    ping)
        if check_device_connection; then
            echo -e "Связь с устройством $TTY_DEVICE установлена."
        else
            exit 3
        fi
        ;;
    all)
        fetch_and_display_full_kkt_info
        ;;
    rel)
        fetch_and_display_relevant_kkt_info
        ;;
    net)
        fetch_and_display_network_settings
        ;;
    sta)
        fetch_and_display_kkt_status
        ;;
    pri)
        fetch_and_display_printer_status
        ;;
    tax)
        fetch_and_display_tax_settings
        ;;
    fn)
        fetch_and_display_fn_status
        ;;
    org)
        fetch_and_display_organization_info
        ;;
    act)
        fetch_and_display_all_activated_services
        ;;
    ser)
        fetch_and_display_kkt_service_info
        ;;
    ofd)
        fetch_and_display_ofd_exchange_status
        ;;
    cou)
        fetch_and_display_shift_counters
        ;;
    csi)
        fetch_and_display_cash_info
        ;;
    rsi)
        fetch_and_display_register_info
        ;;
    tp)
        fetch_and_display_tp_kkt_info
        ;;
    correction)
        open_correction_check "${COMMAND_PARAMS[@]}"
        ;;
    print_service_data)
        execute_single_command "94"
        ;;
    print_last_shift_report)
        execute_single_command "A1"
        ;;
    print_registration_report)
        execute_single_command "A3"
        ;;
    *)
        execute_single_command
        ;;
    esac
}

check_device_connection() {
    echo -n "${ENQ}" >"$COMMAND_PACKET"
    send_command_packet
    timeout 0.3 dd status=none bs=1 count=1 <&"${SOCAT[0]}" 2>/dev/null >"$COMMAND_RESPONSE_FILE"
    if [[ -s "$COMMAND_RESPONSE_FILE" ]]; then
        local byte
        byte="$(xxd -p -u -s -1 "$COMMAND_RESPONSE_FILE" 2>/dev/null)"
        if [[ "$byte" == "06" ]]; then
            return 0
        else
            echo -e "$TTY_DEVICE: Неожиданный ответ: $byte"
            return 1
        fi
    else
        echo -e "$TTY_DEVICE : не удалось установить связь"
        return 1
    fi
}

handle_multiple_devices() {
    local payload_func="$1"

    [[ -z "$should_handle_comproxy" ]] && should_handle_comproxy=1
    ((should_handle_comproxy)) && stop_competing_comproxy

    for device in /dev/ttyACM* /dev/ttyS0 /dev/ttyS1; do
        echo
        if [[ -c "$device" ]]; then
            if [[ "$device" =~ /dev/ttyACM ]] && udevadm info --name="$device" --query=property >"$PROPERTIES_FILE" &&
                { ! grep -q '^ID_VENDOR_ID=0483' "$PROPERTIES_FILE" &>/dev/null ||
                    ! grep -q '^ID_MODEL_ID=5740' "$PROPERTIES_FILE" &>/dev/null; }; then
                echo -e "$device не соответствует требуемым ID_VENDOR_ID=0483 и ID_MODEL_ID=5740."
                continue
            fi

            if is_port_exclusively_used_by_comproxy "$device"; then
                $payload_func
            fi
        else
            echo -e "$device не является символьным устройством."
        fi
    done
}

handle_device_listing() {
    TTY_DEVICE="$device"
    initialize_socat

    if check_device_connection; then
        fetch_and_display_organization_info &>/dev/null
        echo -e "$device : обнаружен вики принт - $ORG_INFO_LINE_1 $ORG_INFO_LINE_2"
        map_device_ports "$device"
    fi

    kill "$SOCAT_PID" &>/dev/null
    wait "$SOCAT_PID" &>/dev/null
    unset TTY_DEVICE
}

execute_command_code() {
    echo -e "$device : выполнение команды '$COMMAND_CODE_ARGUMENT'"
    "$0" --device "$device" --no-pause-comproxy "$COMMAND_CODE_ARGUMENT"
}

execute_all_commands() {
    echo -e "$device : выполнение команды 'all'"
    "$0" --device "$device" --no-pause-comproxy all >/dev/null &
}

map_port_fiscal_register() {
    local device="$1"
    local -i found=0

    mapfile -t serial_ids < <(xmlstarlet sel -t -m "/objects/object[property[@name='device']/value='$device']" -v "@id" -n /linuxcash/cash/conf/drivers/hw::Serial_*.xml)
    for serial_id in "${serial_ids[@]}"; do
        mapfile -t device_ids < <(xmlstarlet sel -t -m "/objects/object[property[@name='serialObj']/ref[@object='$serial_id']]/property[@name='deviceId']" -v "value" -n /linuxcash/cash/conf/drivers/hw::PiritFiscalRegister_*.xml)
        for device_id in "${device_ids[@]}"; do
            found=1
            echo "$2 <-> Артикс ФР${device_id}"
        done
    done
    if ((!found)); then
        echo "$2"
    fi
}

map_port_comproxy() {
    local device="$1"

    if [[ "$device" == "$COMPROXY_PHYSICAL_PORT" ]]; then
        map_port_fiscal_register "$COMPROXY_VIRTUAL_PORT_OUT" "$2 <-> comproxy <-> $COMPROXY_VIRTUAL_PORT <-> $COMPROXY_VIRTUAL_PORT_OUT"
    elif [[ "$device" == "$COMPROXY_PHYSICAL_PORT2" ]]; then
        map_port_fiscal_register "$COMPROXY_VIRTUAL_PORT_OUT2" "$2 <-> comproxy <-> $COMPROXY_VIRTUAL_PORT2 <-> $COMPROXY_VIRTUAL_PORT_OUT2"
    else
        map_port_fiscal_register "$device" "$2"
    fi
}

map_device_ports() {
    local device="$1"

    map_port_comproxy "$device" "$device"
    mapfile -t symlinks < <(udevadm info --query=symlink --root --name="$device" | grep -Po '/dev/\w+(?= |$)')
    for symlink in "${symlinks[@]}"; do
        map_port_comproxy "$symlink" "$device <-> $symlink"
    done
}

main() {
    check_dependencies
    parse_command_line_arguments "$@"

    if [[ "$COMMAND_CODE_ARGUMENT" == "list" ]]; then
        handle_multiple_devices "handle_device_listing"
    elif [[ "$COMMAND_CODE_ARGUMENT" == "stop-com" ]]; then
        stop_competing_comproxy
    elif [[ "$COMMAND_CODE_ARGUMENT" == "cont-com" ]]; then
        continue_competing_comproxy
    elif [[ "$TTY_DEVICE" ]]; then
        if [[ -c "$TTY_DEVICE" ]]; then
            local tty_device_basename="$(basename "$TTY_DEVICE")"
            SOCAT_LOGFILE="$KKT_CLIENT_FOLDER/socat_${tty_device_basename}_last.log"

            if [[ "$TTY_DEVICE" == "/dev/ttyS91" || "$TTY_DEVICE" == "/dev/ttyS101" ]]; then
                should_handle_comproxy=0
            fi
            [[ -z "$should_handle_comproxy" ]] && should_handle_comproxy=1
            ((should_handle_comproxy)) && stop_competing_comproxy

            initialize_socat
            if check_device_connection; then
                map_device_ports "$TTY_DEVICE"
            else
                exit 3
            fi

            local date_time_now="$(date +'%H%M%S_%d%m%y')"

            LOG_OUTPUT_FILE="$KKT_CLIENT_FOLDER/${tty_device_basename}_${date_time_now}.log"
            LOG_LAST_OUTPUT_FILE="$KKT_CLIENT_FOLDER/${tty_device_basename}_last.log"

            JSON_OUTPUT_FILE="$KKT_CLIENT_FOLDER/${tty_device_basename}_${date_time_now}.json"
            JSON_LAST_OUTPUT_FILE="$KKT_CLIENT_FOLDER/${tty_device_basename}_last.json"

            if [[ -z "$COMMAND_CODE_ARGUMENT" ]]; then
                COMMAND_CODE="tp"
            fi
            execute_command
        else
            echo -e "$TTY_DEVICE не существует или не является символьным устройством."
            exit 1
        fi

    elif [[ "$COMMAND_CODE_ARGUMENT" && -z "$TTY_DEVICE" ]]; then
        handle_multiple_devices "execute_command_code"
    elif [[ -z "$COMMAND_CODE_ARGUMENT" && -z "$TTY_DEVICE" ]]; then
        if ((!ignore_modification)); then
            if ! check_file_modification /linuxcash/logs/current/frdriver.log; then
                echo -e "Ошибка: Файл /linuxcash/logs/current/frdriver.log был изменен в последние 15 минут."
                exit 4
            fi
        fi

        handle_multiple_devices "execute_all_commands"

        wait
    fi
}

main "$@"
